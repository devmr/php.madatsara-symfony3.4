<?php

namespace mdts\homeBundle\SearchRepository;

use FOS\ElasticaBundle\Repository;

class EventRepository extends Repository
{
    public function findES($search, $dates = array())
    {
        $query = new \Elastica\Query();

        $query_part = new \Elastica\Query\Bool();

        /*$fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setFieldQuery('name', $search);*/

        if ($search != '') {
            $fieldQuery = new \Elastica\Query\QueryString();
            $fieldQuery->setFields(array('name', 'description'));
            $fieldQuery->setQuery('*'.$search.'*');

            $query_part->addShould(
                //new \Elastica\Query\Term(array('name' => array('value' => $search, 'boost' => 3)))
                $fieldQuery
            );
        }

        //echo $search;
        //echo __FUNCTION__. print_r($dates, true);

        $filters = new \Elastica\Filter\Bool();

        if ($dates['d1s'] != '' && $dates['d2s'] != '') {
            $filters->addMust(
                new \Elastica\Filter\NumericRange('date_unique', array(
                    'gt' => $dates['d1s'],
                    'lt' => $dates['d2e'],
                ))
            );
        } elseif ($dates['d1s'] != '' && $dates['d2s'] == '') {
            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d1s']);
            $date = new \DateTime($d->format('Y-m-d'));
            $date->sub(new \DateInterval('P1D'));
            $d1 = $date->format('Y-m-d\T').'00:00:00Z';

            $filters->addMust(
                new \Elastica\Filter\NumericRange('date_unique', array(
                    'gt' => $dates['d1s'],
                    'lt' => $dates['d1e'],
                ))
            );
        } elseif ($dates['d1s'] == '' && $dates['d2s'] != '') {
            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d2s']);
            $date = new \DateTime($d->format('Y-m-d'));
            $date->sub(new \DateInterval('P1D'));
            $d1 = $date->format('Y-m-d\T').'00:00:00Z';

            $filters->addMust(
                new \Elastica\Filter\NumericRange('date_unique', array(
                    'gt' => $d1,
                    'lt' => $dates['d2e'],
                ))
            );
        }

        $filteredQuery = new \Elastica\Query\Filtered($query_part, $filters);

        $query->setQuery($filteredQuery);

        //print_r($query);

        //$query->setFrom(10);

        //$row = $type->search($query);

        return $this->find($query, 30000);
    }
}
