<?php

namespace mdts\homeBundle\Form;


class Ids
{
    /**
     * @var string
     *
     */
    public $ids;



    /**
     * @return string
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param string $ids
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
    }



}