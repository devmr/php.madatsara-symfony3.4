<?php

namespace mdts\homeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticlesEventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $event = array();
        $artiste = array();

        $entity = $builder->getData();
        $em = $options['er'];

        if ($entity->getId() !== null && $entity->getId() > 0) {
            $EventByArticleid = $em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($entity->getId());
            foreach ($EventByArticleid as $row) {
                $event[] = $row->getId();
            }

            $ArtistesByArticleid = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByArticleId($entity->getId());
            foreach ($ArtistesByArticleid as $row) {
                $artiste[] = $row->getId();
            }
        }

        $builder
            ->add('url')
            ->add('addartistes_inevent', CheckboxType::class, array('required' => false))
            ->add('event', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $event),
            ])
            ->add('artiste', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $artiste),
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\ArticlesEvent', 'er' => false,
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_homebundle_articlesevent';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('er');

    }
}
