<?php

namespace mdts\homeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use mdts\homeBundle\Entity\countriesRepository;
use mdts\homeBundle\Form\EventListener\AddRegionFieldSubscriber;
use mdts\homeBundle\Form\EventListener\AddLocalityFieldSubscriber;
use mdts\homeBundle\Form\EventListener\AddQuartierFieldSubscriber;
use Vich\UploaderBundle\Form\Type\VichImageType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventLieuType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $em = $options['er'];
        

        $builder
            ->addEventSubscriber(new AddRegionFieldSubscriber($em))
            ->addEventSubscriber(new AddLocalityFieldSubscriber($em))
            ->addEventSubscriber(new AddQuartierFieldSubscriber($em))
        ;

        $builder
            ->add('name', TextType::class)
            ->add('slug', TextType::class, array('required' => false))
            ->add('address', TextareaType::class, array('required' => false))
            ->add('gps', TextType::class, array('required' => false))
            ->add('tel', TextType::class, array('required' => false))
            ->add('email', TextType::class, array('required' => false))
            ->add('facebook', TextType::class, array('required' => false))
            ->add('www', TextType::class, array('required' => false))
            //->add('logo', 'text' , array( 'required' => false ))
            ->add('logoFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
            ->add('country', EntityType::class, [
                 'placeholder' => 'Choisir le pays',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\countries',
                'choice_label' => 'name',
                'query_builder' => function (countriesRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
                ->add('telmultiple', CollectionType::class, [
                'entry_type' => telmultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])
            /*->add('region', 'entity' , [
                'placeholder' => 'Choisir la région',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\region',
                'choice_label'       => 'name'
                ,'query_builder' => function (regionRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.country', 'c')
                        ->addSelect('c')
                        ->orderBy('u.name', 'ASC');
                }
                ])
            ->add('locality', 'entity' , [
                    'placeholder' => 'Choisir la ville',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\locality',
                'choice_label'       => 'name'
                ,'query_builder' => function (localityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.region', 'c')
                        ->addSelect('c')
                        ->orderBy('u.name', 'ASC');
                }
                ])
            ->add('quartier', 'entity' , [
            'placeholder' => 'Choisir le quartier',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\quartier',
                'choice_label'       => 'name'
                ]) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\EventLieu', 'er' => false
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_homebundle_eventlieu';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('er');

    }
}
