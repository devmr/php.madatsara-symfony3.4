<?php

namespace mdts\homeBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use mdts\homeBundle\Entity\quartier;
use mdts\homeBundle\Entity\quartierRepository;
use mdts\homeBundle\Entity\locality;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AddQuartierFieldSubscriber implements EventSubscriberInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        );
    }
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $locality = $data->getLocality() ? $data->getLocality() : null;
        $this->addElements($form, $locality);

        /*if (!$data || null === $data->getId()  || gettype( $data->getLocality() ) == 'NULL') {
            $form->add('quartier', 'choice', [
            'placeholder' => 'Choisir le quartier',
                 'required' => false
                ]);
            return;
        }

        $form->add('quartier', 'entity' , [
                'placeholder' => 'Choisir le quartier',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\quartier',
                'choice_label'       => 'name'
                ,'query_builder' => function (quartierRepository $er) use ($data) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.locality', 'c')
                        ->addSelect('c')
                        ->where('c.id = :id')
                        ->setParameter('id', $data->getLocality()->getId())
                        ->orderBy('u.name', 'ASC');
                }
                ]);*/
    }
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!$data || !array_key_exists('locality', $data)) {
            return;
        }


        $locality_id = $data['locality'];
        $locality = $this->em->getRepository('mdtshomeBundle:locality')->find($locality_id);

        $this->addElements($form, $locality);
    }

    public function addElements(FormInterface $form, locality $locality = null)
    {
        $form->add('quartier', EntityType::class, [

                'placeholder' => 'Choisir le quartier',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\quartier',
                'choice_label' => 'name', 'query_builder' => function (quartierRepository $er) use ($locality) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.locality', 'c')
                        ->addSelect('c')
                        ->where('c.id = :id')
                        ->setParameter('id', $locality)
                        ->orderBy('u.name', 'ASC');
                },
                ]);
    }
}
