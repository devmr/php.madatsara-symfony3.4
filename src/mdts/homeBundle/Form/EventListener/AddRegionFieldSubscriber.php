<?php

namespace mdts\homeBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use mdts\homeBundle\Entity\countries;
use mdts\homeBundle\Entity\regionRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AddRegionFieldSubscriber implements EventSubscriberInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        );
    }
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        //Valeur de country dans le form
        $country = $data->getCountry() ? $data->getCountry() : null;
        //echo $data->getCountry()->getId().'--'.$data->getCountry()->getName().' - '.$region->getId().' - '.$region->getName();

        $this->addElements($form, $country);
    }
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!$data || !array_key_exists('country', $data)) {
            return;
        }

        $country_id = $data['country'];
        $country = $this->em->getRepository('mdtshomeBundle:countries')->find($country_id);

        $this->addElements($form, $country);
    }
    public function addElements(FormInterface $form, countries $country = null)
    {
        $form->add('region', EntityType::class, [

                'placeholder' => 'Choisir la région',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\region',
                'choice_label' => 'name', 'query_builder' => function (regionRepository $er) use ($country) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.country', 'c')
                        ->addSelect('c')
                        ->where('c.id = :id')
                        ->setParameter('id', $country)
                        ->orderBy('u.name', 'ASC');
                },
                ]);
    }
}
