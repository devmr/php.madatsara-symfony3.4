<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EventLocal.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventLocalRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(indexes={
 *  @ORM\Index(name="name_idx", columns={"name", "orig_slug"}),  
 *  @ORM\Index(name="name_ftx", columns={"name"}, flags={"fulltext"})  
 * })
 */
class EventLocal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startdate", type="string", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate", type="string", nullable=true)
     */
    private $enddate;

    /**
     * @var string
     *
     * @ORM\Column(name="recurrence", type="string", length=255, nullable=true)
     */
    private $recurrence;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Event")
     */
    private $local;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=true)
     */
    private $hidden;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="event_grouped", type="boolean", nullable=true)
     */
    private $eventgrouped;

    /**
     * @ORM\Column(name="orig_slug", type="string",length=255, nullable=true)
     */
    private $origSlug;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventDate", cascade={"persist"})
     * @ORM\JoinTable(name="local_by_date")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $LocalByDate;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventDate", cascade={"persist"})
     * @ORM\JoinTable(name="local_by_date_home")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $LocalByDateHome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startdate_home", type="string", nullable=true)
     */
    private $startdateHome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enddate_home", type="string", nullable=true)
     */
    private $enddateHome;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EventLocal")
     */
    private $parent;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /*public function __construct() {
        $this->local = new \Doctrine\Common\Collections\ArrayCollection();
    }*/

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EventLocal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startdate.
     *
     * @param \DateTime $startdate
     *
     * @return EventLocal
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate.
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate.
     *
     * @param \DateTime $enddate
     *
     * @return EventLocal
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate.
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set recurrence.
     *
     * @param string $recurrence
     *
     * @return EventLocal
     */
    public function setRecurrence($recurrence)
    {
        $this->recurrence = $recurrence;

        return $this;
    }

    /**
     * Get recurrence.
     *
     * @return string
     */
    public function getRecurrence()
    {
        return $this->recurrence;
    }

    /**
     * Add local.
     *
     * @param \mdts\homeBundle\Entity\Event $local
     *
     * @return EventLocal
     */
    public function addLocal(\mdts\homeBundle\Entity\Event $local)
    {
        $this->local[] = $local;

        return $this;
    }

    /**
     * Remove local.
     *
     * @param \mdts\homeBundle\Entity\Event $local
     */
    public function removeLocal(\mdts\homeBundle\Entity\Event $local)
    {
        $this->local->removeElement($local);
    }

    /**
     * Get local.
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return EventLocal
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return EventLocal
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set hidden.
     *
     * @param bool $hidden
     *
     * @return EventLocal
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden.
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set local.
     *
     * @param \mdts\homeBundle\Entity\Event $local
     *
     * @return EventLocal
     */
    public function setLocal(\mdts\homeBundle\Entity\Event $local = null)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Set eventgrouped.
     *
     * @param bool $eventgrouped
     *
     * @return EventLocal
     */
    public function setEventgrouped($eventgrouped)
    {
        $this->eventgrouped = $eventgrouped;

        return $this;
    }

    /**
     * Get eventgrouped.
     *
     * @return bool
     */
    public function getEventgrouped()
    {
        return $this->eventgrouped;
    }

    /**
     * Set origSlug.
     *
     * @param string $origSlug
     *
     * @return Event
     */
    public function setOrigSlug($origSlug)
    {
        $this->origSlug = $origSlug;

        return $this;
    }

    /**
     * Get origSlug.
     *
     * @return string
     */
    public function getOrigSlug()
    {
        return $this->origSlug;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->LocalByDate = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add localByDate.
     *
     * @param \mdts\homeBundle\Entity\EventDate $localByDate
     *
     * @return EventLocal
     */
    public function addLocalByDate(\mdts\homeBundle\Entity\EventDate $localByDate)
    {
        $this->LocalByDate[] = $localByDate;

        return $this;
    }

    /**
     * Remove localByDate.
     *
     * @param \mdts\homeBundle\Entity\EventDate $localByDate
     */
    public function removeLocalByDate(\mdts\homeBundle\Entity\EventDate $localByDate)
    {
        $this->LocalByDate->removeElement($localByDate);
    }

    /**
     * Get localByDate.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalByDate()
    {
        return $this->LocalByDate;
    }

    /**
     * Add localByDateHome.
     *
     * @param \mdts\homeBundle\Entity\EventDate $localByDateHome
     *
     * @return EventLocal
     */
    public function addLocalByDateHome(\mdts\homeBundle\Entity\EventDate $localByDateHome)
    {
        $this->LocalByDateHome[] = $localByDateHome;

        return $this;
    }

    /**
     * Remove localByDateHome.
     *
     * @param \mdts\homeBundle\Entity\EventDate $localByDateHome
     */
    public function removeLocalByDateHome(\mdts\homeBundle\Entity\EventDate $localByDateHome)
    {
        $this->LocalByDateHome->removeElement($localByDateHome);
    }

    /**
     * Get localByDateHome.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalByDateHome()
    {
        return $this->LocalByDateHome;
    }

    /**
     * Set startdateHome.
     *
     * @param string $startdateHome
     *
     * @return EventLocal
     */
    public function setStartdateHome($startdateHome)
    {
        $this->startdateHome = $startdateHome;

        return $this;
    }

    /**
     * Get startdateHome.
     *
     * @return string
     */
    public function getStartdateHome()
    {
        return $this->startdateHome;
    }

    /**
     * Set enddateHome.
     *
     * @param string $enddateHome
     *
     * @return EventLocal
     */
    public function setEnddateHome($enddateHome)
    {
        $this->enddateHome = $enddateHome;

        return $this;
    }

    /**
     * Get enddateHome.
     *
     * @return string
     */
    public function getEnddateHome()
    {
        return $this->enddateHome;
    }

    /**
     * Set parent
     *
     * @param \mdts\homeBundle\Entity\EventLocal $parent
     *
     * @return EventLocal
     */
    public function setParent(\mdts\homeBundle\Entity\EventLocal $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \mdts\homeBundle\Entity\EventLocal
     */
    public function getParent()
    {
        return $this->parent;
    }
}
