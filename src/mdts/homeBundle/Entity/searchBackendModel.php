<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * searchEventLocal.
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class searchBackendModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    private $query;

    /**
     * @var int
     *
     * @ORM\Column(name="month", type="integer")
     */
    private $month;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var int
     */
    private $country;

    /**
     * @var int
     */
    private $region;

    /**
     * @var int
     */
    private $locality;

    /**
     * @var int
     */
    private $quartier;

    /**
     * @var int
     */
    private $limit;

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set query.
     *
     * @param string $query
     *
     * @return searchEventLocal
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query.
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set month.
     *
     * @param int $month
     *
     * @return searchEventLocal
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month.
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return searchEventLocal
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * Set year.
     *
     * @param int $year
     *
     * @return searchEventLocal
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return searchEventLocal
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getLocality()
    {
        return $this->region;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return searchEventLocal
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get year.
     *
     * @return int
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Set year.
     *
     * @param int $year
     *
     * @return searchEventLocal
     */
    public function setQuartier($quartier)
    {
        $this->quartier = $quartier;

        return $this;
    }
}
