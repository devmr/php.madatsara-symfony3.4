<?php

namespace mdts\homeBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Helper\ProgressBar;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use mdts\homeBundle\Entity\esquerylog;

class MadatsaraCommand extends ContainerAwareCommand
{
    public $em;
    public $cacheManager;
    public $mail;
    public $to;
    public $from;
    private $output;
    private $input;


    protected function configure()
    {
        //OK
        $this
            ->setName('mada:tsara')
            ->setDescription('Mada tsara')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Who do you want to greet?'
            )
            ->addOption(
                '--filter',
                null,
                InputOption::VALUE_OPTIONAL,
                'If set, the task will yell in uppercase letters'
            )
            ->addOption(
                '--d',
                null,
                InputOption::VALUE_OPTIONAL,
                'If set, the index will be deleted'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->output = $output;
        $this->input = $input;

        $default_email = $this->initDefault();
        if ( array_key_exists('to', $default_email)) {
            $this->to = $default_email['to'];
            $this->from = $default_email['from'];
        }
        
        $name = $this->input->getArgument('name');
        $options_filter = $this->input->getOption('filter');
        $text = $name;
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $tr = $tr_o = array();

        $this->cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        $this->mail = \Swift_Message::newInstance();
        switch ($name) {
            case 'sortable':

                $this->_setSortable();
                $text .= 'Fin command '.$name;
                break;

            case 'enablehomeevent':
                $this->setEnableHomeEvent();
                $text = 'Fin command '.$name;
                break;

            case 'imagine':
                $this->setImagine($options_filter);
                $text .= 'Fin command '.$name;
                break;

            case 'getesquerylog':
                $this->getElasticSearchQueryLog();
                $text = 'Fin command '.$name;
                break;
            case 'indexall':
                $this->indexAll();
                $text = 'Fin command '.$name;
                break;

            case 'cancelflyer':
                $this->setCancelFlyer();
                $text = 'Fin command '.$name;
                break;

            default:
                $text = 'Aucune action';
                break;
        }

        $this->output->writeln($text.$options_filter);
    }

    private function execImagineCommand($getFileName, $filters = array('crop_100_100'))
    {
        $command = $this->getApplication()->find('liip:imagine:cache:resolve');
        $arguments = array(
            //'--force' => true
            '--filters' => $filters,
            'paths' => array($getFileName),
        );
        $input = new ArrayInput($arguments);

        return $command->run($input, $this->output);
    }

    protected function getElasticSearchQueryLog()
    {
//        list($query_added, $query_exists, $indexName) = $this->getResultsSearch();


        $msg = '';

        // Mots cles
//        $msg .= $this->getMotsCles($query_added, $query_exists, $indexName);

        // Inscrits a la NL
        $msg .= $this->getAbonnesNewsletter();

        // Membres inscrits
         $msg .= $this->getMembers();

        // Events ajoutés
        $msg .= $this->getEventsAdded();

        // Stats Google Analytics
        $msg .= '' ;

        //Envoyer mail admin
        $this->_mail(
            $this->to,
                '[MADATSARA] - Résumé événements du site',
                $msg,
            $this->from
            );

    }

    protected function setEnableHomeEvent()
    {
        $data = $this->getContainer()->get('madatsara.service')->enableHomeEvent();
        $datalocal = array();
//        $this->getContainer()->get('logger')->info('enableHomeEvent : '.gettype($data));
        $datalocal = $this->getContainer()->get('madatsara.service')->enableHomeEventLocal();
        //Envoyer mail admin
        $this->_mail(
                    $this->to,
                    '[MADATSARA] - Evénements sur la page d\'accueil - début de mois',
                    '<h3>Home event</h3>'.nl2br(implode("\n", $data)).'<hr /><h3>Local event</h3>'.nl2br(implode("\n", $datalocal)),
                    $this->from
                );
    }

    protected function setImagine($options_filter = 'crop_100_100')
    {
        $text = '';
        $data = array();
        $data_error = array();
        $data[] = '<p>Debut du script : '.date('d-m-Y H:i:s').'<br />Caching de : </p><ul>';

        $path_src = $this->getContainer()->get('kernel')->getRootDir().'/../web/assets/flyers/';
        $path_relative = 'assets/flyers/';
        $path_cache = $this->getContainer()->get('kernel')->getRootDir().'/../web/media/cache/'.$options_filter.'/assets/flyers/';

        $filter_array = array($options_filter);

        //$output->writeln(__FUNCTION__);

        $fs = new Filesystem();

        $finder = new Finder();
        $iterator = $finder->files()->in($path_src);

        $i = 0;
        foreach ($iterator as $file) {
            $fname = $file->getFileName();
            $mime_type = \finfo_file(\finfo_open(FILEINFO_MIME_TYPE), $path_src.$fname);

            //Si ce fichier n'existe pas dans '/media/cache/crop_100_100/assets/flyers/'
            if (preg_match('/^image\/.*$/', $mime_type) && !$fs->exists($path_cache.$fname)) {
                $text .= 'Caching de : '.$path_cache.$fname."\n";

                $this->output->writeln($text);

                if (preg_match('/^image\/.*$/', $mime_type)) {
                    $text .= $this->execImagineCommand($path_relative.$fname, $filter_array)."\n";

                    if ($i < 10) {
                        $data[] = '<li>'.$fname.'</li>';
                    }
                    ++$i;
                }
            } elseif (!preg_match('/^image\/.*$/', $mime_type)) {
                $this->output->writeln('Erreur mime '.$path_relative.$fname.' - '.$mime_type);
                $data_error[] = '<li>'.$path_relative.$fname.' - mimetype : <em><small>'.$mime_type.'</small></em></li>';
            }
        }

        if ($i > 10) {
            $data[] = '<li>+ <em>'.($i - 10).'</em> autres fichiers ...</li>';
        }

        $data[] = '</ul><p>Fin du script : '.date('d-m-Y H:i:s').' <strong>'.$i.'</strong> - fichiers traités</p>';

        if (count($data_error) > 0) {
            $data[] = '<hr /><p>Erreur de traitement </p><ul>'.implode(' ', $data_error).'</ul>';
        }

        //Envoi mail
        if ($i > 0 || count($data_error) > 0) {
            $this->_mail(
                $this->to,
                    '[MADATSARA] - Miniature images filtre : '.$options_filter.' - traités : '.$i.' - erreurs : '.count($data_error),
                    implode(' ', $data),
                $this->from
                );
        }

        return $text;
    }

    protected function _mail($to, $subject, $messagehtml, $from)
    {
        $msg = $this->getContainer()->get('templating')->render(
                    'mdtshomeBundle:Email:generique.html.twig',
                    array(
                        'content' => $messagehtml,
                    )
                );
        $this->getContainer()->get('madatsara.service')->sendMail(
                $to,
                $subject,
                $msg,
                $from
                );
    }

    protected function _setSortable()
    {
        $tr = $tr_o = array();

        $data = $this->getContainer()->get('madatsara.service')->setSortableEvent();
        $this->getContainer()->get('logger')->info('Data : '.print_r($data, true));

        if ((isset($data['current']) && count($data['current']) > 0) || (isset($data['other']) && count($data['other']) > 0)) {
            if (count($data['current']) > 0) {
                foreach ($data['current'] as $id) {
                    $entity = $this->em->getRepository('mdtshomeBundle:Event')->find($id);

                    $image = $this->cacheManager->getBrowserPath('assets/flyers/'.$entity->getFlyer(), 'crop_100_100');
                    $image = str_replace('localhost', 'www.madatsara.com', $image);

                    $tr[] = '<tr><td valign="top" style="border-bottom:1px solid #999;padding:10px;"><a href="'.$this->getContainer()->get('router')->getContext()->getScheme().'://'.$this->getContainer()->get('router')->getContext()->getHost().$this->getContainer()->get('router')->getContext()->getBaseUrl().'evenement_'.$entity->getOrigSlug().'.html"><img alt="" src="'.$image.'" border="0" /></a></td><td valign="top" style="border-bottom:1px solid #999;padding:10px;"><a href="'.$this->getContainer()->get('router')->getContext()->getScheme().'://'.$this->getContainer()->get('router')->getContext()->getHost().$this->getContainer()->get('router')->getContext()->getBaseUrl().'evenement_'.$entity->getOrigSlug().'.html">'.$entity->getName().'</a></td></tr>';
                }
            }

            if (count($data['other']) > 0) {
                $i = 0;
                foreach ($data['other'] as $id) {
                    if ($i < 5) {
                        $entity = $this->em->getRepository('mdtshomeBundle:Event')->find($id);
                        $image = $this->cacheManager->getBrowserPath('assets/flyers/'.$entity->getFlyer(), 'crop_100_100');
                        $image = str_replace('localhost', 'www.madatsara.com', $image);
                        $tr_o[] = '<tr><td valign="top" style="border-bottom:1px solid #999;padding:10px;"><a href="'.$this->getContainer()->get('router')->getContext()->getScheme().'://'.$this->getContainer()->get('router')->getContext()->getHost().$this->getContainer()->get('router')->getContext()->getBaseUrl().'evenement_'.$entity->getOrigSlug().'.html"><img alt="" src="'.$image.'" border="0" /></a></td><td valign="top" style="border-bottom:1px solid #999;padding:10px;"><a href="'.$this->getContainer()->get('router')->getContext()->getScheme().'://'.$this->getContainer()->get('router')->getContext()->getHost().$this->getContainer()->get('router')->getContext()->getBaseUrl().'evenement_'.$entity->getOrigSlug().'.html">'.$entity->getName().'</a></td></tr>';
                    }
                    ++$i;
                }
            }

            $content = '';

            if (count($tr) > 0) {
                $content .= '<p>Voici les événements du jour ('.date('d-m-Y').') :</p>';
                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                $content .= implode("\n", $tr);
                $content .= '</table>';
            }

            if (count($tr_o) > 0) {
                $content .= '<p>Voici les 5 autres événements en cours :</p>';
                $content .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
                $content .= implode("\n", $tr_o);
                $content .= '</table>';
            }

            $this->_mail(
                $this->to,
                    '[MADATSARA] - Evènements du jour', //
                    $content,
                $this->from
                );
        }
    }

    /**
     * @return array
     */
    protected function getResultsSearch()
    {
        $data_db = array();
        $query_added = array();
        $query_exists = array();
        // Type
        $dt = new \DateTime();
        $dt->modify('-1 day');
        $date = $dt->format('Ymd');
        $indexName = 'custom_elastic-' . $date;
        $typeName = 'custom_elastic';

        $data = $this->getContainer()->get('madatsara.elasticsearch')->getQueryCustomElastic($indexName, $typeName);
        $entityquerylog = $this->em->getRepository('mdtshomeBundle:esquerylog')->findAll();
        foreach ($entityquerylog as $ql) {
            $data_db[] = $ql->getQuery();
        }
        $batchSize = 20;
        $i = 0;
        foreach ($data as $row) {
            if (!in_array($row, $data_db)) {
                $query_added[] = '- ' . $row;
                $obj = new esquerylog();
                $obj->setQuery($row);
                $this->em->persist($obj);
                $this->em->flush();
            } else {
                $query_exists[] = '- ' . $row;

                // Mettre a jour la table querylog
                $this->getContainer()->get('madatsara.service')->updateQueryLogByQuery($row);
            }
            ++$i;
        }
        return array($query_added, $query_exists, $indexName);
    }

    /**
     * @param $query_added
     * @param $msg
     * @param $query_exists
     * @param $indexName
     * @return string
     */
    protected function getMotsCles()
    {
        $msg = '';
        $indexName = '';
        $query_added = [];
        $query_exists = [];

        $argumentsExiste = false;

        $args = func_get_args();

        if (isset($args[0]) && is_array($args[0])) {
            $query_added = $args[0];
            $argumentsExiste = true;
        }

        if (isset($args[1]) && is_array($args[1])) {
            $query_exists = $args[1];
            $argumentsExiste = true;
        }
        if (isset($args[2]) && is_string($args[2])) {
            $indexName = $args[2];
            $argumentsExiste = true;
        }

        if (!$argumentsExiste)
            return $msg;


        $msg .= '<h3>Nouveaux mots clés ajoutés</h3>' . nl2br(implode("\n", $query_added));
        $msg .= (count($query_exists) > 0 ? '<hr /><h3>Autres mots clés recherchés</h3>' . nl2br(implode("\n", $query_exists)) : '');
        $msg .= '<br /><br />Index : ' . $indexName;
        return $msg;
    }

    /**
     * Inscrits a la NL
     *
     * @return string
     */
    private function getAbonnesNewsletter()
    {
        $msg = '';
        $arrHtml = [];

        // Type
        $dtH = new \DateTime();
        $dtH->modify('-1 day');

        $dtaH = new \DateTime();
        $dtaH->modify('-2 day');

        $dateHier = $dtH->format('Y-m-d');
        $dateAvantHier = $dtaH->format('Y-m-d');

        $nbInscritsHier = $this->em->getRepository('mdtshomeBundle:NewsletterAbonne')->getCountInscritByDate($dateHier);
        if ($nbInscritsHier<=0)
            return $msg;

        $nbInscritsAvantHier = $this->em->getRepository('mdtshomeBundle:NewsletterAbonne')->getCountInscritByDate($dateAvantHier);
        $inscritsHier = $this->em->getRepository('mdtshomeBundle:NewsletterAbonne')->getListsInscritByDate($dateHier);

        $nb = 0;
        foreach ($inscritsHier as $inscrit ) {

            // limiter à 10
            if ($nb<9)
                $arrHtml[] = ' - Email : '.$inscrit->getEmail().' - Pays : '.$inscrit->getCountryName();

            $nb++;
        }

        $points = ($nbInscritsHier-$nbInscritsAvantHier);
        $points = ($points<0?'':'+').$points;

        $msg .= '<hr /><h3>Inscrits Newsletter</h3> - '.$dateHier.' (<strong>'.$nbInscritsHier.'</strong>)<br /><br />';
        $msg .= implode('<br />',$arrHtml);
        $msg .= '<br /><br />Inscrits le '.$dateAvantHier.' ('.$nbInscritsAvantHier.') - Soit :  <strong>'.$points.'</strong> points ';

        return $msg;

    }

    private function getEventsAdded()
    {
        $msg = '';
        $arrHtml = [];

        // Type
        $dtH = new \DateTime();
        $dtH->modify('-1 day');

        $dtaH = new \DateTime();
        $dtaH->modify('-2 day');

        $dateHier = $dtH->format('Y-m-d');
        $dateAvantHier = $dtaH->format('Y-m-d');

        $nbInscritsHier = $this->em->getRepository('mdtshomeBundle:Event')->getCountAddedByDate($dateHier);
        if ($nbInscritsHier<=0)
            return $msg;

        $nbAddedAvantHier = $this->em->getRepository('mdtshomeBundle:Event')->getCountAddedByDate($dateAvantHier);
        $addedHier = $this->em->getRepository('mdtshomeBundle:Event')->getListsAddedByDate($dateHier);

        $nb = 0;

        $scheme = $this->getContainer()->get('router')->getContext()->getScheme();
        $host = $this->getContainer()->get('router')->getContext()->getHost() ;
        $base_url =  $this->getContainer()->get('router')->getContext()->getBaseUrl();
        foreach ($addedHier as $entity ) {
            $url = $scheme.'://'.$host.$base_url.'evenement_'.$entity->getOrigSlug().'.html';
            // limiter à 5
            if ($nb<5)
                $arrHtml[] = ' - <a href="'.$url .'">'.$entity->getName().'</a>' ;

            $nb++;
        }

        $points = ($nbInscritsHier-$nbAddedAvantHier);
        $points = ($points<0?'':'+').$points;

        $msg .= '<hr /><h3>Evenements ajoutés</h3> - '.$dateHier.' (<strong>'.$nbInscritsHier.'</strong>)<br /><br />';
        $msg .= implode('<br />',$arrHtml);
        $msg .= '<br /><br />Ajoutés le '.$dateAvantHier.' ('.$nbAddedAvantHier.') - Soit : <strong>'.$points.'</strong> points';

        return $msg;
    }

    /**
     * members
     *
     * @return string
     */
    private function getMembers()
    {
        $msg = '';
        $arrHtml = [];

        // Type
        $dtH = new \DateTime();
        $dtH->modify('-1 day');

        $dtaH = new \DateTime();
        $dtaH->modify('-2 day');

        $dateHier = $dtH->format('Y-m-d');
        $dateAvantHier = $dtaH->format('Y-m-d');

        $nbInscritsHier = $this->em->getRepository('mdtsUserBundle:User')->getCountInscritByDate($dateHier);

//        $this->output->writeln($nbInscritsHier);

        if ($nbInscritsHier<=0)
            return $msg;


        $nbInscritsAvantHier = $this->em->getRepository('mdtsUserBundle:User')->getCountInscritByDate($dateAvantHier);
        $inscritsHier = $this->em->getRepository('mdtsUserBundle:User')->getListsInscritByDate($dateHier);

        $nb = 0;
        foreach ($inscritsHier as $inscrit ) {

            $provider = ($inscrit->getFacebookId()!=''?'Facebook':'Google');
            // limiter à 10
            if ($nb<9)
                $arrHtml[] = ' - Email : '.$inscrit->getNickname().' - Provider : '.$provider;

            $nb++;
        }

        $points = ($nbInscritsHier-$nbInscritsAvantHier);
        $points = ($points<0?'':'+').$points;

        $msg .= '<hr /><h3>Membres </h3> - '.$dateHier.' (<strong>'.$nbInscritsHier.'</strong>)<br /><br />';
        $msg .= implode('<br />',$arrHtml);
        $msg .= '<br /><br />Connectés/ajoutés le '.$dateAvantHier.' ('.$nbInscritsAvantHier.') - Soit :  <strong>'.$points.'</strong> points ';

        return $msg;

    }

    private function initDefault()
    {
        return $this->getContainer()->get('madatsara.service')->getParameter('default_email');
    }

    private function indexAll()
    {
//        $row = $this->em->getRepository('mdtshomeBundle:Event')->find(13519);

        // creates a new progress bar (50 units)
        $progressBar = new ProgressBar($this->output, 50);

        // starts and displays the progress bar
        $progressBar->start();

        $convert = $this->getContainer()->get('jms_serializer');
        $elasticsearch = $this->getContainer()->get('madatsara.elasticsearch');

        $bDeleteIndex = !empty($this->input->getOption('d'));


        // if delete index
        if ($bDeleteIndex) {
            $elasticsearch->deleteIndex();
            $this->output->writeln('deleteIndex OK');
        }

        // index ALL
        $rows = $this->em->getRepository('mdtshomeBundle:Event')->findAll();
        foreach($rows as $row) {

            // advances the progress bar 1 unit
            $progressBar->advance();

            $this->output->writeln(' * index <comment>' . $row->getName() . '(' . $row->getId() . ')</comment>');

            $json = $convert->serialize($row, 'json');
            if (!$json) {
                return false;
            }

            $data = json_decode($json,true);


            if ($bDeleteIndex || (!$bDeleteIndex && !$elasticsearch->documentExists($row->getId()))) {
                $elasticsearch->indexDocument($row->getId(), $data);
            }

            if (!$bDeleteIndex && $elasticsearch->documentExists($row->getId())) {
                $elasticsearch->updateDocument($row->getId(), $data);
            }


        }

        // ensures that the progress bar is at 100%
        $progressBar->finish();

    }
    private function setCancelFlyer()
    {
        $sql = "SELECT b.id, b.flyer
FROM `event_by_date` a
INNER JOIN event b ON b.id = a.event_id
INNER JOIN event_date c ON c.id = a.event_date_id
INNER JOIN event d ON d.id = a.event_id
WHERE b.flyercancelled IS NULL AND c.date >= ?
GROUP BY a.event_id";

        $sDate = $this->input->getOption('d');

        if ($sDate == '') {
            $this->output->writeln('setCancelFlyer - date empty');
            return false;
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $res = $connection->prepare($sql);
        $res->execute([$sDate]);

        // creates a new progress bar (50 units)
        $progressBar = new ProgressBar($this->output, 50);

        // starts and displays the progress bar
        $progressBar->start();

        while($row = $res->fetch()) {
            $id = $row['id'] ?? 0;
            $flyer = $row['flyer'] ?? '';

//            $this->output->writeln($id . '--' . $flyer);

            if ($flyer != '' && $id > 0) {
                $entity = $this->em->getRepository('mdtshomeBundle:Event')->find($id);

                if (!method_exists($entity, 'getName')) {
                    continue;
                }

                $this->output->writeln($entity->getName());

                $flyerCancelledCreated = $this->getContainer()->get('madatsara.service')->createFlyerCancelled($entity);
                $entity->setFlyercancelled($flyerCancelledCreated);
                $entity->setCancelledAt(true);
                $em->flush();

                $this->output->writeln(' OK ' . $id . ' -- ' . $flyerCancelledCreated);
            }
        }

        // ensures that the progress bar is at 100%
        $progressBar->finish();

    }


}
