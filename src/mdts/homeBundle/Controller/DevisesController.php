<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\Devises;
use mdts\homeBundle\Form\DevisesType;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\checkboxDevisesType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Devises controller.
 */
class DevisesController extends Controller
{
    /**
     * Lists all Devises entities.
     */
    public function indexAction(Request $request)
    {
        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
            ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))

            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_devises_delete_all')->createView();

        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:Devises')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:Devises:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );


    }
    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeallDevises($ids);
        }
        if ($request->isMethod('POST') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->copyallDevises($ids);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_devises'));
    }

    public function duplicateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Devises entity.');
        }
        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->duplicateDevises($entity);

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getInitiales().'» a bien été dupliquée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la copie de l\'Entité «'.$entity->getInitiales().'».');
        }

        return $this->redirect($this->generateUrl('admin_devises'));
    }
    /**
     * Creates a new Devises entity.
     */
    public function createAction(Request $request)
    {
        $entity = new Devises();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateDevises($entity);

            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getInitiales().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getInitiales().'».');
            }

            return $this->redirect($this->generateUrl('admin_devises'));
        }

        return $this->render('mdtshomeBundle:Devises:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Devises entity.
     *
     * @param Devises $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Devises $entity)
    {
        $form = $this->createForm(new DevisesType(), $entity, array(
            'action' => $this->generateUrl('admin_devises_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Devises entity.
     */
    public function newAction()
    {
        $entity = new Devises();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:Devises:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Devises entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Devises entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:Devises:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Devises entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Devises entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:Devises:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Devises entity.
     *
     * @param Devises $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Devises $entity)
    {
        $form = $this->createForm(new DevisesType(), $entity, array(
            'action' => $this->generateUrl('admin_devises_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Devises entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Devises entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateDevises($entity, $id);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getInitiales().'» a bien été mise à jour.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la mise à jour de l\'Entité «'.$entity->getInitiales().'».');
            }

            return $this->redirect($this->generateUrl('admin_devises_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:Devises:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Devises entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeDevises($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeDevises($id);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'Entité .');
        }

        return $this->redirect($this->generateUrl('admin_devises'));
    }

    /**
     * Creates a form to delete a Devises entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_devises_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
