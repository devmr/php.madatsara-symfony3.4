<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\Event;
use mdts\homeBundle\Entity\EventPrice;
use mdts\homeBundle\Form\EventType;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\checkboxEvenementType;
use mdts\homeBundle\Entity\countriesRepository;
use mdts\homeBundle\Entity\regionRepository;
use mdts\homeBundle\Entity\localityRepository;
use mdts\homeBundle\Entity\quartierRepository;
use mdts\homeBundle\Entity\EventVideos;
use mdts\homeBundle\Entity\ArticlesEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;



/**
 * Event controller.
 */
class EventController extends Controller
{
    /**
     * Lists all Event entities.d
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $session->remove('iframe');

        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
            ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))
            ->add('country', EntityType::class, [
                 'placeholder' => 'Choisir le pays',
                 'multiple' => true,
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\countries',
                'choice_label' => 'name',
                'query_builder' => function (countriesRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
            ->add('region', EntityType::class, [
                 'placeholder' => 'Choisir la région',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\region',
                'choice_label' => 'name',
                'query_builder' => function (regionRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
             ->add('locality', EntityType::class, [
                 'placeholder' => 'Choisir la ville',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\locality',
                'choice_label' => 'name',
                'query_builder' => function (localityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
                ->add('quartier', EntityType::class, [
                 'placeholder' => 'Choisir le quartier',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\quartier',
                'choice_label' => 'name',
                'query_builder' => function (quartierRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_event_delete_all')->createView();

        // En cas de submit du form
        if ($filter_form->isSubmitted() && $filter_form->isValid()) {

            //Sauvegarder l'URl en cours en session
            $route = $request->get('_route');
            $url = $request->getUri();
            $session = new Session();
            $session->set('referer', $url);
        }

        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:Event')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:Event:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );

    }


    public function duplicateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }
        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->duplicateEvent($entity);

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'evenement «'.$entity->getName().'» a bien été dupliquée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la copie de l\'evenement «'.$entity->getName().'».');
        }

        return $this->redirect($this->generateUrl('admin_event'));
    }

    /**
     * Creates a new Event entity.
     */
    public function createAction(Request $request)
    {


        $entity = new Event();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $arrForm = $this->geFormEvents($form);

        $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($arrForm,true));

        if ($form->isValid()) {

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEvent($entity, $arrForm);

            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getName().'».');
            }

            $session = new Session();
            $referer = (!empty($session->get('referer')) ? $session->get('referer') : '');

            //Si on clique sur Save and retour
            if ($form->get('save_and_new')->isClicked()) {
                return $this->redirect($this->generateUrl('admin_event_new'));
            }
            //Si on clique sur Save
            if ($form->get('save')->isClicked()) {
                return $this->redirect($this->generateUrl('admin_event_edit', array('id' => $entity->getId())));
            }

            //Si on clique sur Save and duplicate
            if ($form->get('save_and_copy')->isClicked()) {
                //Enregistrer dans BDD
                $id = $this->get('madatsara.service')->duplicateEvent($entity, true);

                return $this->redirect($this->generateUrl('admin_event_edit', array('id' => $id)));
            }

            //Si on clique sur enregistrer et retour et referer existe
            if ($referer != '') {
                return $this->redirect($referer);
            }

            return $this->redirect($this->generateUrl('admin_event'));
            //return $this->redirect($this->generateUrl('admin_event_edit', array('id' => $entity->getId())));
        }

        return $this->render('mdtshomeBundle:Event:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    public function updatemultipleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createEditMultipleForm()->getForm();
        $form->handleRequest($request);

        //Les IDS

        if (count($form->get('id')->getData()) <= 0) {
            return new Response('Aucun ID');
        }

        foreach ($form->get('id')->getData() as $id) {
            $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

            //PLusieurs lieux
            if ($form->get('eventmultilieu')->getData() && $form->get('eventmultilieu')->getData() != '') {
                $id_array = array();
                $tab = explode(',', $form->get('eventmultilieu')->getData());

                if (count($tab) > 0) {
                    foreach ($entity->getEventmultilieu() as $lieu) {
                        $id_array[] = $lieu->getId();
                    }

                    $i = 0;
                    foreach ($tab as $row) {
                        $entitylieu = $em->getRepository('mdtshomeBundle:EventLieu')->find($row);
                        if (count($id_array) > 0 && !in_array($row, $id_array) || count($id_array) == 0) {
                            $entity->addEventmultilieu($entitylieu);
                        }

                        ++$i;
                    }
                }
            }

            //Enregistrer eventtype
            if ($form->get('eventtype')->getData() && $form->get('eventtype')->getData()->getId() > 0) {
                //Suppr Types
                foreach ($entity->getEventtype() as $types) {
                    $entitytypeold = $em->getRepository('mdtshomeBundle:eventtype')->find($types->getId());
                    $entity->removeEventtype($entitytypeold);
                }

                $entitytype = $em->getRepository('mdtshomeBundle:eventtype')->find($form->get('eventtype')->getData()->getId());
                $entity->addEventtype($entitytype);
                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' addEventtype '.$form->get('eventtype')->getData()->getId());
            }

            //Enregistrer eventlocal
            if ($form->get('eventlocal')->getData() && $form->get('eventlocal')->getData() > 0) {
                $id_array = array();
                //Local
                foreach ($entity->getEventlocal() as $types) {
                    $id_array[] = $types->getId();
                }

                $entitylocal = $em->getRepository('mdtshomeBundle:EventLocal')->find($form->get('eventlocal')->getData());
                if (count($id_array) > 0 && !in_array($form->get('eventlocal')->getData(), $id_array) || count($id_array) == 0) {
                    $entity->addEventlocal($entitylocal);
                }
                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' addEventlocal '.$form->get('eventlocal')->getData());
            }

            //Enregistrer artistes
            if ($form->get('artistesdj')->getData() && $form->get('artistesdj')->getData() != '') {
                $id_array = array();
                //Artistes
                foreach ($entity->getEventArtistesDjOrganisateurs() as $types) {
                    $id_array[] = $types->getId();
                }
                //Combien
                $tab = explode(',', $form->get('artistesdj')->getData());
                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' artistes tab : '.print_r($tab,true));
                if (count($tab) > 0) {
                    foreach ($tab as $row) {
                        $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($row);
                        if (count($id_array) > 0 && !in_array($row, $id_array) || count($id_array) == 0) {
                            $entity->addEventArtistesDjOrganisateur($entityartiste);
                        }
                        //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' addEventArtistesDjOrganisateur '.$row);
                    }
                }
            }

            //Enregistrer Event related multiple
            //var_dump($form->get('related_event')->getData());exit;
            if ($form->get('related_event')->getData() && $form->get('related_event')->getData() != '') {
                //Related event
                $id_array = array();
                foreach ($entity->getEventRelated() as $types) {
                    $id_array[] = $types->getId();
                }

                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' related_event data : '.$form->get('related_event')->getData());
                //Combien
                $tab = explode(',', $form->get('related_event')->getData());
                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' related_event data tab '.print_r($tab,true));

                if (count($tab) > 0) {
                    foreach ($tab as $row) {
                        $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($row);
                        //Si ce n'est pas l'event en cours ou new event
                        if (count($id_array) > 0 && !in_array($row, $id_array) || count($id_array) == 0) {
                            $entity->addEventRelated($entityevent);
                            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' addEventRelated '.$row);
                        }
                    }
                }
            }

            //Enregistrer videomultiple
            if (count($form['videomultiple']) > 0) {
                $url_array = array();
                //Videos
                foreach ($entity->getEventvideos() as $types) {
                    $url_array[] = $types->getUrl();
                }
                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' $form[\'videomultiple\'] '.print_r($form['videomultiple'],true));
                $i = 0;
                foreach ($form['videomultiple'] as $row) {
                    $url = $row['url']->getData();
                    //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' - url : '.$url );

                    if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                        if (count($url_array) > 0 && !in_array($url, $url_array) || count($url_array) == 0) {
                            $entityvideo = new EventVideos();
                            $entityvideo->setUrl($url);

                            $entity->addEventvideo($entityvideo);
                            $entityvideo->setEvent($entity);

                            $em->persist($entityvideo);
                        }

                        //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' - save url ' );
                    }
                    ++$i;
                }
            }

            //Enregistrer articlesmultiple
            if (count($form['articlesmultiple']) > 0) {

                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' - $form[\'articlesmultiple\'] - '.print_r($form['articlesmultiple'],true) );
                $url_array = array();
                //Articles
                foreach ($entity->getArticlesEvent() as $types) {
                    $url_array[] = $types->getUrl();
                }

                $i = 0;
                foreach ($form['articlesmultiple'] as $row) {
                    $url = $row['url']->getData();
                    $addartiste = $row['addartiste']->getData();

                    //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' url : '.$url.' - addartiste : '.$addartiste );

                    if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                        if (count($url_array) > 0 && !in_array($url, $url_array) || count($url_array) == 0) {
                            $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->findOneByUrl($url);
                            if (!$entityarticle) {
                                $entityarticle = new ArticlesEvent();
                                $entityarticle->setUrl($url);
                                $em->persist($entityarticle);
                                //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' save url : '.$url );
                            }

                            $entity->addArticlesEvent($entityarticle);
                            if ($addartiste == 1) {
                                //Recuperer les artistes
                                if ($form->get('artistesdj')->getData() && $form->get('artistesdj')->getData() != '') {
                                    //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' Artistesdj : '.$form->get('artistesdj')->getData() );
                                    //Combien
                                    $tab = explode(',', $form->get('artistesdj')->getData());
                                    //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' tab : '.print_r($tab,true) );
                                    if (count($tab) > 0) {
                                        foreach ($tab as $row) {

                                            //Ebtity de l'EventArtistesDjOrganisateurs
                                            $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($row);

                                            //Recuperer les articles de chaque artiste et si existe - supprimer avant d'ajouter
                                            foreach ($entityartiste->getArticlesEvent() as $articles) {
                                                //Supprimer si trouvé
                                                if ($entityarticle->getId() == $articles->getId()) {
                                                    $entityartiste->removeArticlesEvent($entityarticle);
                                                    //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' removeArticlesEvent - articleID :  '.$entityarticle->getId().' - aristeID : '.$row );
                                                }
                                            }
                                            $entityartiste->addArticlesEvent($entityarticle);
                                            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' - addArticlesEvent ');
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ++$i;
                }
            }

            $em->flush();
        }
        $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été enregistrées.');

        return $this->redirect($this->generateUrl('admin_event'));
    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditMultipleForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_event_updatemultiple'))
            ->add('eventtype', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\homeBundle\Entity\EventType',
                'choice_label' => 'name',
                'mapped' => false, 'data' => null,
            ])
            ->add('entreetype', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\homeBundle\Entity\EntreeType',
                'choice_label' => 'name',
            ])
            ->add('eventmultilieu', HiddenType::class, [
                'mapped' => false,
                'default' => '',

            ])
            ->add('artistesdj', HiddenType::class, [
                'mapped' => false,
                'default' => '',

            ])
            ->add('eventlocal', HiddenType::class, [
                'mapped' => false,
                'default' => '',

            ])
            ->add('related_event', HiddenType::class, [
                'mapped' => false,
                'default' => '',

            ])
            ->add('id', HiddenType::class, [
                'mapped' => false,
                'default' => '',

            ])

            ->add('save', SubmitType::class)

            ->add('articlesmultiple', CollectionType::class, [
                'entry_type' => new \mdts\homeBundle\Form\articlesmultipleType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])
            ->add('videomultiple', CollectionType::class, [
                'entry_type' => new \mdts\homeBundle\Form\videomultipleType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])

            ;
    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Event $entity)
    {
        $form = $this->createForm(EventType::class, $entity, array(
            'action' => $this->generateUrl('admin_event_create'),
            'method' => 'POST',
            'er' => false
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Event entity.
     */
    public function newAction(Request $request)
    {
        $session = new Session();
        $session->remove('iframe');
        $getIframe = $request->query->get('iframe');
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session->set('iframe', '1');
        }
        $eventbymember = $request->query->get('eventbymember');
        $entityEventByMember = null;
        $filename = '';
        $pathTmp = $this->get('kernel')->getRootDir().'/../web/tmp/';
        $pathFlyer = $this->get('kernel')->getRootDir().'/../web/assets/flyerseventbyuser/';
        $fs = new Filesystem();

        $entity = new Event();
        $entity->getEventPrice()->add(new EventPrice());

        if (!is_null($eventbymember) &&  $eventbymember>0) {
            // Recuperer les infos du flyer ok
            $em = $this->getDoctrine()->getManager();
            $entityEventByMember = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($eventbymember);
            $filename = $entityEventByMember->getFlyer();

            $entity->setName($entityEventByMember->getName());
            $entity->setDescription($entityEventByMember->getDescription());
            $entity->addEventtype($entityEventByMember->getThematique());
        }

        if (!$fs->exists($pathFlyer.$filename))
            $filename = '';

        if ($filename=='')
            $entityEventByMember = null;

        if ($filename!='')
            $fs->copy($pathFlyer.$filename, $pathTmp.$filename);

        //  ld( $entityEventByMember );



        $form = $this->createCreateForm($entity);

        $tpl = 'mdtshomeBundle:Event:new.html.twig';

        $form = $form->createView();

        return $this->render($tpl, array(
            'entity' => $entity,
            'form' => $form,
            'entityeventbymember' => $entityEventByMember,
        ));
    }

    /**
     * Finds and displays a Event entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:Event:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Event entity.
     */
    public function editAction($id)
    {
        $request = Request::createFromGlobals();
        $getIframe = $request->query->get('iframe');
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session = new Session();
            $session->set('iframe', '1');
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }
        $localprice = array();
        $videoevent = array();
        $flyerevent = array();
        $articlesevent = array();
        $i = 0;
        foreach ($entity->getEventPrice() as $local) {
            $localprice[$i]['price'] = $local->getPrice();
            $localprice[$i]['detailprice'] = $local->getDetailPrice();
            //$localprice[$i]['devise'] = $local->getDevise()->getId();
            //$localprice[$i]['devise_label'] = $local->getDevise()->getInitiales();
            ++$i;
        }

        $entityvideos = $em->getRepository('mdtshomeBundle:EventVideos')->getVideosByEventID($id);

        $i = 0;
        foreach ($entityvideos as $local) {
            $videoevent[$i]['url'] = $local->getUrl();
            ++$i;
        }

        $i = 0;
        foreach ($entity->getEventFlyers() as $local) {
            $flyerevent[$i]['id'] = $local->getId();
            $flyerevent[$i]['image'] = $local->getImage();
            $flyerevent[$i]['ismain'] = $local->getIsmain();

            ++$i;
        }

        $i = 0;
        foreach ($entity->getArticlesEvent() as $local) {
            $articlesevent[$i]['url'] = $local->getUrl();
            ++$i;
        }

        /*print_r($localprice);
        print_r($videoevent);
        print_r($flyerevent);*/

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:Event:edit.html.twig', array(
            'eventprice' => json_encode($localprice),
            'videoevent' => json_encode($videoevent),
            'flyerevent' => json_encode($flyerevent),
            'articlesevent' => json_encode($articlesevent),
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Event $entity)
    {
        $er = $this->getDoctrine()->getManager();

        $form = $this->createForm(EventType::class, $entity, array(
            'action' => $this->generateUrl('admin_event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'er' => $er,
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Event entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $arrForm = $this->geFormEvents($editForm);

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEvent($entity, $arrForm, $id);

            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été enregistré.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de l\'enregistrement de l\'Entité «'.$entity->getName().'».');
            }

            $session = new Session();
            $referer = (!empty($session->get('referer')) ? $session->get('referer') : '');
            //dump($referer);exit;

            //return $this->redirect($referer);

            if ($srv === true) {
                //Si on clique sur Save and retour
                if ($referer != '') {
                    return $this->redirect($referer);
                }
                if ($editForm->get('save_and_back')->isClicked()) {
                    return $this->redirect($this->generateUrl('admin_event'));
                }

                return $this->redirect($this->generateUrl('admin_event_edit', array('id' => $id)));
            } else {
                return $this->redirect($this->generateUrl('admin_event_edit', array('id' => $id)));
            }
        }

        return $this->render('mdtshomeBundle:Event:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Event entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeEvent($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeEvent($id);
        }
        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'événement a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'événement .');
        }

        $session = new Session();
        $referer = (!empty($session->get('referer')) ? $session->get('referer') : '');

        if ($referer != '') {
            return $this->redirect($referer);
        }

        return $this->redirect($this->generateUrl('admin_event'));
    }

    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';

//        print_r($request->getMethod()) ;
//        print_r($datapost) ;
//        print_r($data) ;

        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

//        print_r($ids);exit;
        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeallEvent($ids);
        }
        if ($request->isMethod('POST') && count($ids) > 0) {
            if ($data['action'] != '') {
                return $this->setAction($ids, $data['action'],$request);
            } else {
                $srv = $this->get('madatsara.service')->copyallEvent($ids);
            }
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_event'));
    }

    /**
     * Creates a form to delete a Event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }

    private function setAction($ids, $action,$request)
    {
        if (count($ids) == 0) {
            return false;
        }

        if ($action == 'enable_disable') {
            $srv = $this->get('madatsara.service')->setEnableOrDisableEvent($ids);
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été mises à jour.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la mise à jour des entités.');
            }

            return $this->redirect($this->generateUrl('admin_event'));
        }
        //Edition multiple
        $form = $this->createEditMultipleForm()->getForm();

        $entities = array();
        $em = $this->getDoctrine()->getManager();
        //Pour chaque ID
        foreach ($ids as $id) {
            $entities[] = $em->getRepository('mdtshomeBundle:Event')->find($id);
        }

        $tpl = 'mdtshomeBundle:Event:editmultiple.html.twig';

        return $this->render($tpl, array(
            'entities' => $entities,
            'form' => $form->createView(),
        ));
    }

    private function geFormEvents($form)
    {
        $arrVideos = [];
        $arrPrix = [];
        $arrFlyers = [];
        $arrArticles = [];
        if (count($form['videomultiple']) > 0) {

            foreach ($form['videomultiple'] as $row) {
                $url = $row['url']->getData();
                if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                    array_push($arrVideos,['url'=>$url]);
                }
            }
        }
        if (count($form['prixmultiple']) > 0) {
            foreach ($form['prixmultiple'] as $row) {
                if ($row['price']->getData() != '' && filter_var($row['price']->getData(), FILTER_VALIDATE_INT) && $row['detailprice']->getData() != '' && method_exists($row['devises']->getData(), 'getId') && $row['devises']->getData()->getId() > 0) {
                    $price = $row['price']->getData();
                    $detailprice = $row['detailprice']->getData();
                    $devises = $row['devises']->getData()->getInitiales();
                    array_push($arrPrix,['price'=>$price, 'detailprice'=>$detailprice, 'devises'=>$devises]);
                }
            }
        }
        if (count($form['flyermultiple']) > 0) {
            foreach ($form['flyermultiple'] as $row) {
                if ($row['image']->getData()) {
                    $image = $row['image']->getData();
                    $ismain = $row['ismain']->getData();
                    array_push($arrFlyers,['image'=>$image, 'ismain'=>$ismain]);
                }
            }
        }
        if (count($form['articlesmultiple']) > 0) {
            foreach ($form['articlesmultiple'] as $row) {
                $url = $row['url']->getData();
                $addartiste = $row['addartiste']->getData();
                if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                    array_push($arrArticles,['url'=>$url, 'addartiste'=>$addartiste]);
                }
            }
        }


        $filenameajax = $form->get('filenameajax')->getData();
        $dragandrop = $form->get('dragandrop')->getData();
        $eventmultilieu = $form->get('eventmultilieu')->getData();
        $eventtype = ($form->get('eventtype')->getData() ?$form->get('eventtype')->getData()->getId():0);
        $eventlocal = $form->get('eventlocal')->getData();
        $artistesdj = $form->get('artistesdj')->getData();
        $related_event =  $form->get('related_event')->getData();
        $multiflyer =  $form->get('multiflyer')->getData();
        $oldslug = $form->get('oldslug')->getData();
        $cancelledAt = $form->get('cancelledAt')->getData();
        $eventbymember =  $form->get('eventbymember')->getData();
        $arrForm = [
            'filenameajax' => $filenameajax
            ,'dragandrop' => $dragandrop
            ,'eventmultilieu' => $eventmultilieu
            ,'eventlocal' => $eventlocal
            ,'eventtype' => $eventtype
            ,'artistesdj' => $artistesdj
            ,'related_event' => $related_event
            ,'multiflyer' => $multiflyer
            ,'oldslug' => $oldslug
            ,'cancelledAt' => $cancelledAt
            ,'eventbymember' => $eventbymember
            ,'videomultiple' => $arrVideos
            ,'prixmultiple' => $arrPrix
            ,'flyermultiple' => $arrFlyers
            ,'articlesmultiple' => $arrArticles
            ,'api' => 2 // Web
        ];
        return $arrForm;
    }
    public function date_range($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
        $todate = \DateTime::createFromFormat('Y-m-d', $todate);

        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }
}
