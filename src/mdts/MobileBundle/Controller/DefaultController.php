<?php

namespace mdts\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();
        /**
         * Dates.
         */
        $monday_current_week = date('Y-m-d', strtotime('monday this week'));
        $sunday_current_week = date('Y-m-d', strtotime('sunday this week'));

        /**
         * Toutes les thematiques.
         */
        $all_thematiques = $repository->getRepository('mdtshomeBundle:EventType')->findAll();
        /**
         * Event de la semaine.
         */
        $week_events = $repository->getRepository('mdtshomeBundle:Event')->getCurrentWeekEvent(8);

        return $this->render('mdtsMobileBundle:Default:index.html.twig', ['d1' => $monday_current_week, 'd2' => $sunday_current_week, 'all_thematiques' => $all_thematiques, 'week_events' => $week_events]);
    }
}
