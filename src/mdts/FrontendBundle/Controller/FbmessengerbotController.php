<?php

namespace mdts\FrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use mdts\homeBundle\Entity\EventLieu;
use mdts\FrontendBundle\Entity\ChatbotUser;
use mdts\FrontendBundle\Entity\ChatbotSession;
use mdts\FrontendBundle\Entity\ChatbotHears;
use Symfony\Component\Security\Core\User\UserInterface;
use mdts\FrontendBundle\Services\OnboardingConversation;


class FbmessengerbotController extends Controller
{
    protected $accessToken = 'EAAMjP1vlPTEBAPKWuZCshyqgWbND8OX44ZA2JnfZCHF9eA2Ucf91gc685GNE8R8yotvZBd94J23ymOuqSu0DQunshvPPuCuKZCczAKJHz1TZBcKR1YQ6rBePNdr2KwjfJmLqh8GtTQilthAKBNLLJva3jHqhNZCbeCtZC7SZBJ7g4IwZDZD';
    protected $verifyToken = 'madatsara';
    protected $appSecret = 'e6ab8c29a8661a9b0dc413a3c9f7c8ca';
    protected $welcomeMsg = 'Bonjour, je suis Madabot ! Si tu cherches un événement, tu es au bon endroit ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgYesterDay = 'Bonjour, je suis Madabot ! Content de te retrouver à nouveau depuis hier ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgDays = 'Bonjour, je suis Madabot ! Content de te retrouver à nouveau depuis ces {nb} jours ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgLastMonth = 'Bonjour, je suis Madabot ! Content de te retrouver à nouveau depuis le mois dernier ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgMonths = 'Bonjour, je suis Madabot ! Content de te retrouver à nouveau depuis ces {nb} mois ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgLastYear = 'Bonjour, je suis Madabot ! Bienvenue !! Content de te retrouver à nouveau depuis l\'année dernière ! Comment je peux t\'aider aujourd\'hui ?';
    protected $welcomeMsgYears = 'Bonjour, je suis Madabot ! Bienvenue !! Content de te retrouver à nouveau depuis ces {nb} ans ! Comment je peux t\'aider aujourd\'hui ?';
    protected $lblChooseRubrique = 'Choisis une rubrique dans cette liste.';
    protected $lblChooseRubriqueOtherLists = '... ou dans cette liste.';
    protected $clickbtnMessageMenu = 'ou directement sur un des boutons dans le menu.';
    protected $sBtnSearchEvent = 'événement de la semaine';
    protected $sBtnSearchEventMonth = 'événement du mois';
    protected $sBtnSearchEventLieu = 'événement par ville';
    protected $sMerciLike = 'merci (y)';
    protected $sFallbackMessage= 'Désolé {firstname}, je n\'ai pas encore compris ton message "{strMessage}".';
    protected $sMsgFiltre= 'Ah ok. Comment tu veux que je les affiche ?';
    protected $sBy= 'Par';
    protected $arrLike = ['369239263222822', '369239383222810', '369239343222814'];
    protected $aStringHears = [];
    protected $sPaysRegionVille = 'Pays/région/ville';
    protected $sArtiste = 'Artiste';
    protected $sTous = 'ou tous';
    protected $em = null;


    /** 
    * Ajouter bouton démarrer
    curl -X POST -H "Content-Type: application/json" -d '{
    "setting_type":"call_to_actions",
    "thread_state":"new_thread",
    "call_to_actions":[
     {
      "payload":"USER_DEFINED_PAYLOAD"
     }
    ]
    }' "https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAAMjP1vlPTEBAPKWuZCshyqgWbND8OX44ZA2JnfZCHF9eA2Ucf91gc685GNE8R8yotvZBd94J23ymOuqSu0DQunshvPPuCuKZCczAKJHz1TZBcKR1YQ6rBePNdr2KwjfJmLqh8GtTQilthAKBNLLJva3jHqhNZCbeCtZC7SZBJ7g4IwZDZD"
    */


    protected function getStringHears ()
    {
        $aStringHears = [];
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtsFrontendBundle:ChatbotHears')->findAll();
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($rows,1));
        foreach ($rows as $row) {
            $sCmd = method_exists($row, 'getCmd') ? $row->getCmd() : '';
            if ($sCmd !== '' && !in_array($sCmd, ['fallback'])) {
                array_push($aStringHears, $sCmd);
            }
        }
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($aStringHears,1));
        return $aStringHears;
    }
    public function indexAction()
    {

        $hubVerifyToken = null;
        $challenge = null;

        if(isset($_REQUEST['hub_challenge'])) {
            $challenge = $_REQUEST['hub_challenge'];
            $hubVerifyToken = $_REQUEST['hub_verify_token'];
        }


        if ($hubVerifyToken === $this->verifyToken) {
            return new Response($challenge);
        }
        $config = [
            'facebook' => [
                'token' => $this->accessToken,
                'app_secret' => $this->appSecret,
                'verification'=>$this->verifyToken,
            ]
        ];
        // Load the driver(s) you want to use
        DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookDriver::class);
        // DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookAudioDriver::class);
        // DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookFileDriver::class);
        DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookImageDriver::class);
        DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookLocationDriver::class);
        // DriverManager::loadDriver(\BotMan\Drivers\Facebook\FacebookVideoDriver::class);

        // Create an instance
        $botman = BotManFactory::create($config);

        // Quelle commande ecoute le bot?
        $this->setHearsStringCmd($botman);

        // Ecouter les commandes specifiques aux rubriques
        $this->setHearsEventtypeCmd($botman);
        
        // Si aucun message compris
        $botman->fallback(function($bot) {  
            $this->sendFallback($bot); 
        });

        // A la reception d'une image
        $botman->receivesImages(function(BotMan $bot, $images) {
            $this->sendImages($bot, $images);  
        });

 
        // Start listening
        $botman->listen();
        return new Response('Hello yourself.'.__FILE__);
    } 

    /**
     *
     * A la reception d'une image
     *
     */

    protected function sendImages (BotMan $bot, $images)
    {
        foreach ($images as $image) {
                
            $arrPayload = $image->getPayload(); // The original payload 
            $iStickerid = array_key_exists('sticker_id', $arrPayload) ? $arrPayload['sticker_id'] : '';
            $sUrl = array_key_exists('url', $arrPayload) ? $arrPayload['url'] : '';

            $this->get('mts.facebookbot')->savebotImages($bot, $iStickerid, $sUrl);

            // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($arrPayload,1));

            // Est-ce un bouton like ?
            $bStickerIsLike = $this->stickerIsLike($iStickerid);
            // Afficher merci si bouton like
            if ($bStickerIsLike) {
                // Faire patienter 1 secondes
                $bot->typesAndWaits(1);
                $bot->reply($this->sMerciLike);
            }
        }
    }

    /**
     *
     * A l'ecoute d'une commande
     *
     */
    protected function setHearsStringCmd ($botman)
    {
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($this->aStringHears, 1));
        $this->aStringHears = $this->getStringHears();

        foreach ($this->aStringHears as $sHears) {

//             $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.$sHears);

            // Commande ecoutee
             $botman->hears($sHears, function ($bot) use ($sHears)  { 
                // Remove "bot:"
                $sHears = str_replace('bot:', '', $sHears);
                // Clean
                $sHears = trim($sHears);
//                 $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.$sHears);

                // La methode existe dans la classe
                if (method_exists($this, $sHears)) {
                    // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__);
                    call_user_func_array([$this, $sHears], [$bot]);
                    return false;
                }



                // La methode n'existe pas dans la classe
                if (!method_exists($this, $sHears)) {

                    switch ($sHears) {

                        // Au clic sur Demarrer
                        case 'USER_DEFINED_PAYLOAD':
                            $this->sendWelcome($bot); 
                            break;

                        // Si la personne a tapé Hi|Hello|cc|slt|bjr|bsr|bonjour|bonsoir|akory    
                        case '.*(cc|coucou|slt|salut|bjr|bonjour|bsr|bonsoir).*': 
                            $this->sendInvite($bot);
                            break; 
                    }
                }
            });
        }
    }

    /**
    * Est-ce un bouton like facebook?
    *
    * @param String $stickerId - Identifiant du sticker
    *
    * @return Boolean
    **/
    protected function stickerIsLike($stickerId) 
    {
        if ($stickerId === '' || !is_array($this->arrLike)) {
            return false;
        } 

        // au lieu d'utiliser > if (in_array($stickerId, arrLike));
        $arrLike = array_flip($this->arrLike);
        $isLikeSticker = isset($arrLike[$stickerId]);

        return $isLikeSticker;
    } 

    /**
    * Afficher message de bienvenue
    *
    **/
    protected function sendWelcome(BotMan $bot)
    {
        $em = $this->getDoctrine()->getManager();

        $iNbYear = 0;
        $iNbMonth = 0;
        $iNbDay = 0;
        $iNbHour = 0;
        $iNbMinute = 0;

        // Enregistrer dans chatbot_session
        $strHears = 'USER_DEFINED_PAYLOAD';
        $chatbotsession = $this->get('mts.facebookbot')->saveCmd($strHears, $bot);
        $iSessionId = method_exists($chatbotsession, 'getId') ? $chatbotsession->getId() : 0;

        // L'identifiant facebook de l'utilisateur
        $user = $bot->getUser();
        $iIdfb = $user->getId();
        $chatbotuser = $this->get('mts.facebookbot')->saveChatbotUser($iIdfb);
        $iChatbotuserId = $chatbotuser->getId();

        $chatbothears = $em->getRepository('mdtsFrontendBundle:ChatbotHears')->findOneByCmd($strHears);
        $iRowHearsdid = method_exists($chatbothears, 'getId') ? $chatbothears->getId() : 0;

        // Dernier message enregistré du même type
        $rows = $em->getRepository('mdtsFrontendBundle:ChatbotSession')->getLastrowHears([/*'hears_id'=>$iRowHearsdid, */'user_id'=>$iChatbotuserId, 'session_id'=>$iSessionId]);

        $sDateRow = method_exists($rows, 'getCreatedAt') ? $rows->getCreatedAt()->format('Y-m-d H:i:s') : '';

        if ($sDateRow !== '') {
            $oDate = new \DateTime($sDateRow);
            $oNow = new \DateTime();
            // Difference entre maintenant et date
            $iDate = $oDate->diff($oNow);

            $iNbYear = property_exists($iDate, 'y') ? $iDate->y : 0;
            $iNbMonth = property_exists($iDate, 'm') ? $iDate->m : 0;
            $iNbDay = property_exists($iDate, 'd') ? $iDate->d : 0;
            $iNbHour = property_exists($iDate, 'h') ? $iDate->h : 0;
            $iNbMinute = property_exists($iDate, 'i') ? $iDate->i : 0;

//            $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r(['date'=>$sDateRow, 'year'=>$iDate->y, 'month'=>$iDate->m, 'day'=>$iDate->d, 'hour'=>$iDate->h, 'minute'=>$iDate->i], 1));
        }

        // Change msg by interval year, month, day, hour
        $sWelcomeMsg = $this->welcomeMsg;

        // Ne pas afficher Bonjour si connexion dans la même minute ou heure
        if ($iNbMinute > 1 || $iNbHour > 1) {
            $sWelcomeMsg = '';
        }

        // more than 1 day
        if ($iNbDay === 1) {
            $sWelcomeMsg = $this->welcomeMsgYesterDay;
        }
        if ($iNbDay > 1) {
            $sWelcomeMsg = str_replace('{nb}',$iNbDay,$this->welcomeMsgDays);
        }

        // more than 1 month
        if ($iNbMonth === 1) {
            $sWelcomeMsg = $this->welcomeMsgLastMonth;
        }
        if ($iNbMonth > 1) {
            $sWelcomeMsg = str_replace('{nb}',$iNbMonth,$this->welcomeMsgMonths);
        }
        // more than 1 year
        if ($iNbYear === 1) {
            $sWelcomeMsg = $this->welcomeMsgLastYear;
        }
        if ($iNbYear > 1) {
            $sWelcomeMsg = str_replace('{nb}',$iNbYear,$this->welcomeMsgYears);
        }


        // Faire patienter 2 secondes
        $bot->typesAndWaits(2); 

        // Afficher message de bienvenue
        if ($sWelcomeMsg !== '') {
            $bot->reply($sWelcomeMsg);
        }

        // Bouton d'action
        $this->showRubriques($bot);

            
    }

    /**
    * Afficher message de bienvenue
    *
    **/
    protected function sendInvite(BotMan $bot)
    {

        $this->get('mts.facebookbot')->saveCmd($strHears, $bot);

        // Faire patienter 2 secondes
        $bot->typesAndWaits(2); 

        // Bouton d'action
        $this->showRubriques($bot);

            
    }

    protected function showRubriques (BotMan $bot)
    {


        $aIds = [];
        $em = $this->getDoctrine()->getManager();

        // Rubriques ayant le plus d'events
        $rows = $em->getRepository('mdtshomeBundle:Event')->getEventsGroupedByType();

        // Limite 3 imposé par Facebook
        $rows = $this->array_divide($rows, 3);
//        $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__.' '.print_r($rows, 1));

        $iN = 0;
        $iNbIterations = count($rows);
        foreach ($rows as $aRow) {
            $aBtnBot = [];
            // Creer liste des rubriques sous forme de bouton
            foreach ($aRow as $r ) {

                $iId = array_key_exists('id', $r) ? $r['id'] : 0;
                $row = $em->getRepository('mdtshomeBundle:EventType')->find($iId);
                $sName = method_exists($row, 'getName') ? $row->getName() : '';
                $sSlug = method_exists($row, 'getSlug') ? $row->getSlug() : '';

                $oBtn = ElementButton::create($sName)->type('postback')->payload('bot:rubrique:' . $sSlug);
                array_push($aBtnBot, $oBtn);
            }

            if ($iN < $iNbIterations - 1) {
                // Faire patienter 1 secondes
                $bot->typesAndWaits(1);
            }

            $sLblChooseRubrique = $this->lblChooseRubrique;
            // Ne pas afficher le même message pour les iterations
            if ($iN > 0) {
                $sLblChooseRubrique = $this->lblChooseRubriqueOtherLists;
            }
            // Proposer des boutons d'actions
            $bot->reply(
            // Label de l'action
                ButtonTemplate::create($sLblChooseRubrique)
                    // Liste des rubriques
                    ->addButtons($aBtnBot)

            );
            
            $iN++;
        }
 

    }

    /**
     *
     * Si message incompris par le bot
     *
     */ 
    protected function sendFallback (BotMan $bot)
    {
        $strPayloadType = $this->getPayloadType($bot);




        $strHears = 'fallback';
        $this->get('mts.facebookbot')->saveCmd($strHears, $bot);

        if ($strPayloadType === 'quick_reply') {
            return $this->setQuickReply($bot);
        }

        if ($strPayloadType === 'location') {
            return $this->setLocalization($bot);
        }


        // a typing indicator and sleep for 2 seconds
        $user = $bot->getUser();
        // Access first name
        $firstname = $user->getFirstName();
//        $strMessage = print_r($arrPayload, 1); // TODO comment at last
        $strMessage = $strPayloadType;

        $msg = $this->sFallbackMessage;
        $msg = str_replace('{firstname}',$firstname,$msg);
        $msg = str_replace('{strMessage}',$strMessage,$msg);
        $bot->types();
        $bot->reply($msg); 
    }


    /**
    * Evenement de la semaine
    *
    **/
    protected function eventWeek (BotMan $bot)
    {
        // Faire patienter 2 secondes
        $bot->typesAndWaits(2); 

        // Afficher texte
        $bot->reply($this->sMsgFiltre);

        // Proposer des boutons d'actions
        $bot->reply(
            // Label de l'action
            ButtonTemplate::create($this->sBy)

            // Evenement de la semaine
            ->addButton(
                // Label du bouton
                ElementButton::create($this->sPaysRegionVille)->type('postback')->payload('bot:eventWeekPaysRegionVille')
            )

            // Evenement du mois
            ->addButton(
                // Label du bouton
                ElementButton::create($this->sArtiste)->type('postback')->payload('bot:eventWeekArtiste')
            )

            // Evenement dans un lieu
            ->addButton(
                // Label du bouton
                ElementButton::create($this->sTous)->type('postback')->payload('bot:eventWeekAll')
            )
             
        );
    }

    /**
     *
     * Tous les evenements de la semaine
     *
     */
    protected function eventWeekAll (Botman $oBot)
    {
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__);
        // Afficher texte
        $offset = 0;
        $limit = 10; // limit impose par fb

        $day = date('w') - 1;
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]); 

        $aData = $this->getEventResults($rows, $limit, $offset);
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  print_r($rows, 1));

        $sScheme = $this->get('router')->getContext()->getScheme();
        $sHost = $this->get('router')->getContext()->getHost();
        $sBaseUrl = $this->get('router')->getContext()->getBaseUrl();

        $aTplBot = [];
        foreach ($aData as $aRowTpl) {

            $sName = array_key_exists('name', $aRowTpl) ? $aRowTpl['name'] : '';
            $sFlyer = array_key_exists('flyer', $aRowTpl) ? $aRowTpl['flyer'] : '';
            $sSlug = array_key_exists('slug', $aRowTpl) ? $aRowTpl['slug'] : '';
            $oDateUnique = array_key_exists('date_unique', $aRowTpl) ? $aRowTpl['date_unique'] : '';
            $oHeureDebut = array_key_exists('heure_debut', $aRowTpl) ? $aRowTpl['heure_debut'] : '';
            $oDateDebut = array_key_exists('date_debut', $aRowTpl) ? $aRowTpl['date_debut'] : '';
            $oDateFin = array_key_exists('date_fin', $aRowTpl) ? $aRowTpl['date_fin'] : '';
            
            $sDateUnique = !is_null($oDateUnique) ? $oDateUnique->format('d/m/Y') : '';
            $sHeureDebut = !is_null($oHeureDebut) ? $oHeureDebut->format('H:i') : '';
            $sDateDebut = !is_null($oDateDebut) ? $oDateDebut->format('d/m/y') : '';
            $sDateFin = !is_null($oDateFin) ? $oDateFin->format('d/m/y') : '';

            $sSubtitle = '';
            // Si date Unique
            if ($sDateUnique != '') {
                $sSubtitle = 'Le ' . $sDateUnique . ' à ' . $sHeureDebut;
            }

            // Si entre 2 dates
            if ($sDateUnique === '') {
                $sSubtitle = 'Du ' . $sDateDebut . ' au ' . $sDateFin;
            }


            // Si flyer non vide
            if ($sFlyer !== '') {
                $sFlyer = $sScheme .'://' . $sHost . $sBaseUrl .  '/media/cache/resize/assets/flyers/' . $sFlyer;
            }

            $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  $sFlyer );
            $oTmpBot = Element::create($sName)
                    ->subtitle($sSubtitle)
                    ->image($sFlyer)
                    ->addButton(
                        ElementButton::create('+ d\'infos')->payload('tellmemore')->type('postback')
                    )
                    ->addButton(
                        ElementButton::create('Détail sur le site')->url($sScheme .'://' . $sHost . $sBaseUrl . '/evenement_' . $sSlug . '.html')
                    );
            array_push($aTplBot, $oTmpBot);
             
           
        }

        $oBot->reply(GenericTemplate::create()
            ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
            ->addElements($aTplBot)
        ); 
    }

    protected function getEventResults($rows, $limit, $offset, $typerelation = false, $idrelation = 0)
    {
        $em = $this->getDoctrine()->getManager();
        
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventInID($rows, $limit, true, $offset);
        $total = count($rows);
        // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  $total);
        $data = [];
        foreach ($rows  as $row) {
            // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  $row->getName());

            // Type event
            $aEventType = $this->getEventtypeResults($row->getEventType());

            $iId = method_exists($row, 'getId') ? $row->getId() : 0;
            $sName = method_exists($row, 'getName') ? $row->getName() : '';
            $sFlyer = method_exists($row, 'getFlyer') ? $row->getFlyer() : '';
            $sSlug = method_exists($row, 'getSlug') ? $row->getSlug() : '';
            $iPosition = method_exists($row, 'getPosition') ? $row->getPosition() : 0;
            $aDateUnique = method_exists($row, 'getDateUnique') ? $row->getDateUnique() : [];
            $aHeureFin = method_exists($row, 'getHeureFin') ? $row->getHeureFin() : [];
            $aHeureDebut = method_exists($row, 'getHeureDebut') ? $row->getHeureDebut() : [];
            $aDateDebut = method_exists($row, 'getDateDebut') ? $row->getDateDebut() : [];
            $aDateFin = method_exists($row, 'getDateFin') ? $row->getDateFin() : [];

            $aData = [
                    'id' => $iId, 
                    'name' => $sName, 
                    'flyer' => $sFlyer, 
                    'slug' => $sSlug, 
                    'position' => $iPosition, 
                    'date_unique' => $aDateUnique, 
                    'heure_fin' => $aHeureFin, 
                    'heure_debut' => $aHeureDebut, 
                    'date_debut' => $aDateDebut, 
                    'date_fin' => $aDateFin, 
                    'eventtype' => $aEventType, 
                    'eventlieu' => '', 
                    '_event_artistes_dj_organisateurs' =>  [] 
            ];
            array_push($data, $aData); 
        }
        //$this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  print_r($data, 1));
        return $data;
    }

    /**
     *
     * Event types
     *
     */
    protected function getEventtypeResults ($aData)
    {
        $aResults = [];
        $aDataResults = [];

        foreach ($aData  as $row) {
            // $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ .  $row->getName());

            $iId = method_exists($row, 'getId') ? $row->getId() : 0;
            $sName = method_exists($row, 'getName') ? $row->getName() : '';
            $sLogo = method_exists($row, 'getLogo') ? $row->getLogo() : '';
            $sSlug = method_exists($row, 'getSlug') ? $row->getSlug() : '';

            $aDataResults = [
                    'id' => $iId, 
                    'name' => $sName, 
                    'flyer' => $sLogo, 
                    'slug' => $sSlug 
            ];
            array_push($aResults, $aDataResults); 
        }
        return $aResults;
    }

    protected function array_divide($array, $segmentCount) {
        $dataCount = count($array);
        if ($dataCount == 0) return false;
        $segmentLimit = ceil($dataCount / $segmentCount);
        $outputArray = array();
        while($dataCount > $segmentLimit) {
            $outputArray[] = array_splice($array,0,$segmentLimit);
            $dataCount = count($array);
        }
        if($dataCount > 0) $outputArray[] = $array;
        return $outputArray;
    }

    private function sendRubrique($bot, $sSlug)
    {
        // Sauvegarder la rubrique selectionnée
        /*$bot->userStorage()->save([
            'sSlug' => $sSlug
        ]);*/

        // Demander geolocalisation ou clic pays
        $bot->startConversation(new OnboardingConversation($sSlug));
    }

    private function setHearsEventtypeCmd($botman)
    {
        $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__);
        // Pour toutes les rubriques
        $em = $this->getDoctrine()->getManager();

        $rows = $em->getRepository('mdtshomeBundle:EventType')->findAll();
        foreach ($rows as $row) {
            $sSlug = method_exists($row, 'getSlug') ? $row->getSlug() : '';
            if ($sSlug !== '') {
                $sHearsSlug = 'bot:rubrique:' . $sSlug;
                $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ . ' - ' . $sHearsSlug);
                $botman->hears($sHearsSlug, function ($bot) use ($sSlug) {
                    $this->get('logger')->info( __FUNCTION__.' - line '.__LINE__ . ' - ' . $sHearsSlug . ' - ' . $sSlug );
                    $this->sendRubrique($bot, $sSlug);
                });
            }
        }
    }


    /**
     *
     * Events courants
     *
     */
    public function currenteventAction ()
    {

        $em = $this->getDoctrine()->getManager();
        $rows = $this->getStringHears();

        $sData = print_r($rows, 1);
        return new Response($sData);
    }

    public function OLDcurrenteventAction ()
    {
        $nextpage = 0;
        $limit = 20;

        $day = date('w') - 1;
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]);


        $aData = $this->getEventResults($rows, $limit, $nextpage);


        $aTmpData = [];
        $aTmp = [];

        foreach ($aData as $aRowTpl) {

            $sName = array_key_exists('name', $aRowTpl) ? $aRowTpl['name'] : '';
            $sFlyer = array_key_exists('flyer', $aRowTpl) ? $aRowTpl['flyer'] : '';
            $sSlug = array_key_exists('slug', $aRowTpl) ? $aRowTpl['slug'] : '';

            $oDateUnique = array_key_exists('date_unique', $aRowTpl) ? $aRowTpl['date_unique'] : '';
            $oHeureDebut = array_key_exists('heure_debut', $aRowTpl) ? $aRowTpl['heure_debut'] : '';
            $oDateDebut = array_key_exists('date_debut', $aRowTpl) ? $aRowTpl['date_debut'] : '';
            $oDateFin = array_key_exists('date_fin', $aRowTpl) ? $aRowTpl['date_fin'] : '';

            $oDateUnique = !is_null($oDateUnique) ? $oDateUnique->format('d/m/Y') : '';
            $oHeureDebut = !is_null($oHeureDebut) ? $oHeureDebut->format('H:i') : '';
            $oDateDebut = !is_null($oDateDebut) ? $oDateDebut->format('d/m/y') : '';
            $oDateFin = !is_null($oDateFin) ? $oDateFin->format('d/m/y') : '';

            $aTmp = ['name'=>$sName, 'flyer'=>$sFlyer, 'slug'=>$sSlug, 'dateUnique'=>$oDateUnique, 'HeureDebut'=> $oHeureDebut, 'DateDebut'=> $oDateDebut, 'DateFin'=>$oDateFin];

            array_push($aTmpData, $aTmp);
        }

        $aData = $aTmpData;
        $sData = print_r($aData, 1);
        return new Response($sData);
    }

    private function getPayload($bot)
    {
        $arrPayload = $bot->getMessage()->getPayload();
        $arrMessage = array_key_exists('message', $arrPayload) ? $arrPayload['message'] : [];

        $strText = array_key_exists('text', $arrMessage) ? $arrMessage['text'] : '';

        $arrAttachmentsMessage = array_key_exists('attachments', $arrMessage) ? $arrMessage['attachments'] : [];
        $arr0AttachmentsMessage = array_key_exists(0, $arrAttachmentsMessage) ? $arrAttachmentsMessage[0] : [];
        $strType0AttachmentsMessage = array_key_exists('type', $arr0AttachmentsMessage) ? $arr0AttachmentsMessage['type'] : '';
        $arrPayload0AttachmentsMessage = array_key_exists('payload', $arr0AttachmentsMessage) ? $arr0AttachmentsMessage['payload'] : [];
        $arrCoordPayload0AttachmentsMessage = array_key_exists('coordinates', $arrPayload0AttachmentsMessage) ? $arrPayload0AttachmentsMessage['coordinates'] : [];
        $strLat = array_key_exists('lat', $arrCoordPayload0AttachmentsMessage) ? $arrCoordPayload0AttachmentsMessage['lat'] : '';
        $strLong = array_key_exists('long', $arrCoordPayload0AttachmentsMessage) ? $arrCoordPayload0AttachmentsMessage['long'] : '';

        $arrQuickreplyMessage = array_key_exists('quick_reply', $arrMessage) ? $arrMessage['quick_reply'] : [];
        $strPayloadQuickreplyMessage = array_key_exists('payload', $arrQuickreplyMessage) ? $arrQuickreplyMessage['payload'] : '';

        $arrPostback = array_key_exists('postback', $arrPayload) ? $arrPayload['postback'] : [];
        $strPayload = array_key_exists('payload', $arrPostback) ? $arrPostback['payload'] : '';

        $aData = [
            'text' => $strText,
            'latitude' => $strLat,
            'longitude' => $strLong,
            'payload' => $strPayload,
            'type' => $strType0AttachmentsMessage,
            'payload' => $strPayloadQuickreplyMessage
        ];
        return $aData;

    }

    private function getPayloadType($bot)
    {
        $arrPayload = $this->getPayload($bot);

        if ($arrPayload['latitude'] !== '' && $arrPayload['longitude'] !== '') {
            return 'location';
        }

        if ($arrPayload['payload'] !== '') {
            return 'quick_reply';
        }

        return 'text';
    }

    private function setQuickReply($bot)
    {
//        $user = $bot->userStorage()->all();
        $msg = __FUNCTION__ ;
        $bot->reply($msg);
    }

    private function setLocalization($bot)
    {
//        $user = $bot->userStorage()->all();
        $msg = __FUNCTION__ ;
        $bot->reply($msg);
    }

}
