<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ChatbotSession
 *
 * @ORM\Table(name="chatbot_session")
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Repository\ChatbotSessionRepository")
 */
class ChatbotSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\ChatbotUser")
     */
    private $chatbotUser;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\ChatbotHears")
     */
    private $hears;

    /**
     * @var string
     *
     * @ORM\Column(name="hears_text", type="string", nullable=true)
     */
    private $hearstext;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chatbotUser
     *
     * @param mdts\FrontendBundle\Entity\ChatbotUser $chatbotUser
     *
     * @return ChatbotSession
     */
    public function setChatbotUser($chatbotUser)
    {
        $this->chatbotUser = $chatbotUser;

        return $this;
    }

    /**
     * Get chatbotUser
     *
     * @return mdts\FrontendBundle\Entity\ChatbotUser
     */
    public function getChatbotUser()
    {
        return $this->chatbotUser;
    }

    /**
     * Set hears
     *
     * @param string $hears
     *
     * @return ChatbotSession
     */
    public function setHears($hears)
    {
        $this->hears = $hears;

        return $this;
    }

    /**
     * Get hears
     *
     * @return string
     */
    public function getHears()
    {
        return $this->hears;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EventByMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EventByMember
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set hearstext
     *
     * @param string $hearstext
     *
     * @return ChatbotSession
     */
    public function setHearstext($hearstext)
    {
        $this->hearstext = $hearstext;

        return $this;
    }

    /**
     * Get hearstext
     *
     * @return string
     */
    public function getHearstext()
    {
        return $this->hearstext;
    }
}
