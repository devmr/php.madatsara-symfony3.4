<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailByInfoType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Entity\EmailByInfoTypeRepository")
 */
class EmailByInfoType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType")
     */
    private $authorinfotype;

    /**
     * @var string
     *
     * @ORM\Column(name="objetmail", type="string", length=255, nullable=true)
     */
    private $objetmail;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return EmailByInfoType
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set authorinfotype
     *
     * @param \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype
     *
     * @return EmailByInfoType
     */
    public function setAuthorinfotype(\mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype = null)
    {
        $this->authorinfotype = $authorinfotype;

        return $this;
    }

    /**
     * Get authorinfotype
     *
     * @return \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType
     */
    public function getAuthorinfotype()
    {
        return $this->authorinfotype;
    }



    /**
     * Set objetmail
     *
     * @param string $objetmail
     *
     * @return EmailByInfoType
     */
    public function setObjetmail($objetmail)
    {
        $this->objetmail = $objetmail;

        return $this;
    }

    /**
     * Get objetmail
     *
     * @return string
     */
    public function getObjetmail()
    {
        return $this->objetmail;
    }
}
