<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventByMember.
 *
 * @ORM\Table()
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Entity\EventByMemberRepository")
 */
class EventByMember
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "eventbm.name.not_blank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\NotBlank(message = "eventbm.description.not_blank")
     *
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EventType")
     * @Assert\NotBlank(message = "eventbm.thematique.not_blank")
     */
    private $thematique;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="flyereventbymember", fileNameProperty="flyer")
     * @Assert\File( mimeTypes = {"image/jpeg","image/pjpeg","image/png"}, mimeTypesMessage = "Le type de l'image est incorrect ({{ type }})." )
     *
     * @var File
     */
    private $flyerFile;

    /**
     * @var string
     *
     * @ORM\Column(name="flyer", type="string", length=255)
     */
    private $flyer;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\UserBundle\Entity\User")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Event")
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\Column(name="infos", type="text", nullable=true)
     */
    private $infos;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMemberStatus")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType")
     */
    private $authorinfotype;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType")
     */
    private $emailauthorinfotype;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EventByMember
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return EventByMember
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set flyer.
     *
     * @param string $flyer
     *
     * @return EventByMember
     */
    public function setFlyer($flyer)
    {
        $this->flyer = $flyer;

        return $this;
    }

    /**
     * Get flyer.
     *
     * @return string
     */
    public function getFlyer()
    {
        return $this->flyer;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setflyerFile(File $image = null)
    {
        $this->flyerFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getflyerFile()
    {
        return $this->flyerFile;
    }

    /**
     * Set thematique.
     *
     * @param \mdts\homeBundle\Entity\EventType $thematique
     *
     * @return EventByMember
     */
    public function setThematique(\mdts\homeBundle\Entity\EventType $thematique = null)
    {
        $this->thematique = $thematique;

        return $this;
    }

    /**
     * Get thematique.
     *
     * @return \mdts\homeBundle\Entity\EventType
     */
    public function getThematique()
    {
        return $this->thematique;
    }

    /**
     * Set author.
     *
     * @param \mdts\UserBundle\Entity\User $author
     *
     * @return EventByMember
     */
    public function setAuthor(\mdts\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author.
     *
     * @return \mdts\UserBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set infos.
     *
     * @param string $infos
     *
     * @return EventByMember
     */
    public function setInfos($infos)
    {
        $this->infos = $infos;

        return $this;
    }

    /**
     * Get infos.
     *
     * @return string
     */
    public function getInfos()
    {
        return $this->infos;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EventByMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EventByMember
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set event.
     *
     * @param \mdts\homeBundle\Entity\Event $event
     *
     * @return EventByMember
     */
    public function setEvent(\mdts\homeBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set status.
     *
     * @param \mdts\FrontendBundle\Entity\EventByMemberStatus $status
     *
     * @return EventByMember
     */
    public function setStatus(\mdts\FrontendBundle\Entity\EventByMemberStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \mdts\FrontendBundle\Entity\EventByMemberStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set authorinfotype
     *
     * @param \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype
     *
     * @return EventByMember
     */
    public function setAuthorinfotype(\mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype = null)
    {
        $this->authorinfotype = $authorinfotype;

        return $this;
    }

    /**
     * Get authorinfotype
     *
     * @return \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType
     */
    public function getAuthorinfotype()
    {
        return $this->authorinfotype;
    }

    /**
     * Set emailauthorinfotype
     *
     * @param \mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType $emailauthorinfotype
     *
     * @return EventByMember
     */
    public function setEmailauthorinfotype(\mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType $emailauthorinfotype = null)
    {
        $this->emailauthorinfotype = $emailauthorinfotype;

        return $this;
    }

    /**
     * Get emailauthorinfotype
     *
     * @return \mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType
     */
    public function getEmailauthorinfotype()
    {
        return $this->emailauthorinfotype;
    }
}
