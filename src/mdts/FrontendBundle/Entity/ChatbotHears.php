<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChatbotHears
 *
 * @ORM\Table(name="chatbot_hears")
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Repository\ChatbotHearsRepository")
 */
class ChatbotHears
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cmd", type="string", nullable=true)
     */
    private $cmd;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cmd
     *
     * @param string $cmd
     *
     * @return ChatbotHears
     */
    public function setCmd($cmd)
    {
        $this->cmd = $cmd;

        return $this;
    }

    /**
     * Get cmd
     *
     * @return string
     */
    public function getCmd()
    {
        return $this->cmd;
    }
}

