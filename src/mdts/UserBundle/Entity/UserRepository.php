<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 28/11/2017
 * Time: 21:53
 */

namespace mdts\UserBundle\Entity;

use \Doctrine\ORM\EntityRepository;

class UserRepository extends  EntityRepository
{
    public function getCountInscritByDate($date)
    {
        $query = $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->where('DATE(a.lastLogin) = :date')
            ->setParameter('date', $date )
            ->getQuery();

        return $query->getSingleScalarResult();
    }

    public function getListsInscritByDate($date)
    {
        $query = $this->createQueryBuilder('a');

        $query->where('DATE(a.lastLogin) = :date')
            ->setParameter('date', $date );


        return $query->getQuery()->getResult();
    }

}