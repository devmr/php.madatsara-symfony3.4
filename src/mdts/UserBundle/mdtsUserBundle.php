<?php

namespace mdts\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class mdtsUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
