<?php

namespace mdts\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\EntreeType;
use mdts\homeBundle\Form\EntreeTypeType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BackendController extends Controller
{
    public function ngindexAction()
    {
        return $this->render('mdtsBackendBundle:Angular:index.html.twig');
    }
    public function dispatchAction(Request $request)
    {
        $url = $request->query->get('url');
        //Parser l'URL
        $tab = parse_url($url);
        $type = '';

        //Quel est le type d'URL
        if (isset($tab['host'])) {
            //Si type video : youtube, dailymotion, facebook video vimeo... -> ajout video dans event
            if ($this->get('madatsara.service')->isUrlVideoType($url)) {
                $type = 'video';

                return $this->redirectToRoute('admin_event_new', array('iframe' => 1), 301);
            }
            //Si madatsara.com -> modif event
            elseif (preg_match('/madatsara\.com/i', $tab['host']) && preg_match('/\/evenement_.*\.html/i', $tab['path'])) {
                $type = 'madatsara';

                //Replace /evenement_ et .html dans path
                $slug = str_replace('/evenement_', '', $tab['path']);
                $slug = str_replace('.html', '', $slug);

                //Chercher l'ID correspondant à slug dans la BDD
                $repository = $this->getDoctrine()->getManager();
                $res = $repository->getRepository('mdtshomeBundle:Event')->findOneBy(array('slug' => $slug));
                if ($res->getId() > 0) {
                    return $this->redirectToRoute('admin_event_edit', array('iframe' => 1, 'id' => $res->getId()), 301);
                }
            }
            //Si fbcdn.net -> créer event
            elseif (preg_match('/fbcdn\./i', $tab['host'])) {
                $type = 'event fb';

                return $this->redirectToRoute('admin_event_new', array('iframe' => 1), 301);
            }
            //Si madatsara.com -> mais non event
            elseif (preg_match('/madatsara\.com/i', $tab['host']) && !preg_match('/\/evenement_.*\.html/i', $tab['path'])) {
                return $this->redirectToRoute('admin_event_new', array('iframe' => 1), 301);
            }
            //Si autre > presse > ajout presse à un event ou artiste
            else {
                $type = 'presse';

                return $this->redirectToRoute('admin_articlesevent_new', array('iframe' => 1, 'url' => $url), 301);
            }
        }

        return new Response('<html><body>URL inconnu - Dispatch non effectué "'.$type.'"</body></html>');
    }
    public function indexAction()
    {
        return $this->render('mdtsBackendBundle:Default:index.html.twig');
    }
    public function createEntreetypeAction(Request $request)
    {
        $model = new EntreeType();
        $form = $this->get('form.factory')->create(new EntreeTypeType(), $model);
        //Validation du form
        if ($form->handleRequest($request)->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createEntreeType($model);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$model->getName().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$model->getName().'».');
            }
            // Rediriger vers elle meme
            return $this->redirect($this->generateUrl('mdts_backend_entreetype_create'));
        }

        return $this->render('mdtsBackendBundle:EntreeType:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    public function listeEntreetypeAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();
        $all = $repository->getRepository('mdtshomeBundle:EntreeType')->findAll();

        return $this->render('mdtsBackendBundle:EntreeType:index.html.twig', array('all' => $all));
    }
    public function createEventAction()
    {
        return $this->render('mdtsBackendBundle:Event:create.html.twig');
    }

    public function listeEventAction()
    {
        return $this->render('mdtsBackendBundle:Event:liste.html.twig');
    }
}
