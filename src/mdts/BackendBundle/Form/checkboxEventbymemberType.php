<?php

namespace mdts\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use mdts\FrontendBundle\Entity\EventByMemberRepository;

class checkboxEventbymemberType extends AbstractType
{
    private $page;

    public function __construct($page)
    {
        $this->page = $page;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fooid', 'entity', array(
            'required' => false,
            'class' => 'mdtsFrontendBundle:Eventbymember',
            'property' => 'id',
            'property_path' => '[id]', // in square brackets!
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (EventByMemberRepository $er) {
                return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'DESC')
                        ->setMaxResults(100)
                        ->setFirstResult(($this->page > 1 ? (($this->page - 1) * 100) : 0));
            },
        ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null, 'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdts_frontendbundle_all';
    }
}
