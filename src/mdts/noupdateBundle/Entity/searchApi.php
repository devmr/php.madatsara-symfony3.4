<?php

namespace mdts\noupdateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * searchApi.
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class searchApi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    private $query;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set query.
     *
     * @param string $query
     *
     * @return searchApi
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query.
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }
}
