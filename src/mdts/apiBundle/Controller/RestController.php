<?php

namespace mdts\apiBundle\Controller;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\FileParam;
use mdts\homeBundle\Entity\countries;
use mdts\homeBundle\Entity\quartier;
use mdts\homeBundle\Entity\locality;
use mdts\homeBundle\Entity\region;
use mdts\homeBundle\Entity\EntreeType;
use mdts\homeBundle\Entity\EventLieu;
use mdts\homeBundle\Entity\EventLocal;
use mdts\homeBundle\Entity\Event;
use mdts\homeBundle\Entity\EventArtistesDjOrganisateurs;
use mdts\homeBundle\Entity\esquerylog;
use mdts\homeBundle\Form\EventType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use mdts\FrontendBundle\Entity\registrationid;
use GeoIp2\Database\Reader;
use HTMLPurifier;
use mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType;
use mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType;
use mdts\homeBundle\Entity\EventType as EntityEventtype;
use Symfony\Component\HttpFoundation\Request as Sfrequest;

//use Ladybug\Dumper;

class RestController extends FOSRestController
{
    public function getAction()
    {
        //return $this->render('mdtsapiBundle:Default:index.html.twig', array('name' => $name));
        return array('Bonjour' => 'Madatsara user');
    }

    public function getQueryAction()
    {
        $data = [];
        $i = 0;
        $em = $this->getDoctrine()->getManager();
        $entityquerylog = $em->getRepository('mdtshomeBundle:esquerylog')->findByHidden(0);
        foreach ($entityquerylog as $ql) {
            $data[$i]['keyword'] = $ql->getQuery();
            ++$i;
        }

        return ['results' => $data];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAndranaAction(ParamFetcher $paramFetcher)
    {
        $nextpage = 0;
        $limit = 20;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates(['2016-08-09', '2016-08-14']);

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    public function getArtistesResults($rows, $limit, $offset, $typerelation = false, $idrelation = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $total = count($rows);
        $rows = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getAllEventInID($rows, $limit, true, $offset);

        $data = $this->getDatas($rows);
        $k = array_keys($data);

        $endkey = end($k) + 1;
        if ($offset > 0) {
            $endkey += $limit * $offset;
        }

        return array('allids' => '', 'end_key' => $endkey, 'endresults' => end($k) + 1 == $total ? true : false, 'total' => $total, 'nextpage' => ($offset > 0 ? $limit + $offset : $offset), 'limit' => $limit, 'results' => array_values($data));
    }

    public function getEventResults($rows, $limit, $offset, $typerelation = false, $idrelation = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $total = count($rows);
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventInID($rows, $limit, true, $offset);

        $data = $this->getDatas($rows);
        $k = array_keys($data);

        $endkey = end($k) + 1;
        if ($offset > 0) {
            $endkey += $limit * $offset;
        }

        return array('allids' => '', 'end_key' => $endkey, 'endresults' => end($k) + 1 == $total ? true : false, 'total' => $total, 'nextpage' => ($offset > 0 ? $limit + $offset : $offset), 'limit' => $limit, 'results' => array_values($data));
    }

    private function getDatas($rows)
    {
        $data = array();
        foreach ($rows  as $row) {
            $data[] = [
                'id' => $row->getId(), 'name' => $row->getName(), 'flyer' => $row->getFlyer(), 'slug' => $row->getSlug(), 'position' => $row->getPosition(), 'entreetype' => $row->getEntreetype(), 'date_unique' => $row->getDateUnique(), 'heure_fin' => $row->getHeureFin(), 'heure_debut' => $row->getHeureDebut(), 'date_debut' => $row->getDateDebut(), 'date_fin' => $row->getDateFin(), 'eventtype' => $row->getEventType(), 'eventlieu' => (!is_null($row->getEventlieu()) ? $this->getMainLieuxInfo($row->getEventlieu()) : null), '_event_artistes_dj_organisateurs' => ($row->getEventArtistesDjOrganisateurs() ? $this->getMainArtistsInfo($row->getEventArtistesDjOrganisateurs()) : []),
                //,'typerelation' => (true===$typerelation && $idrelation>0?$em->getRepository('mdtshomeBundle:Event')->getRelationType( $row->getId(), $idrelation ):null)
            ];
        }

        return $data;
    }

    public function getMainLieuxInfo(EventLieu $eventlieu)
    {
        if ($eventlieu->getTelsLieu() instanceof \mdts\homeBundle\Entity\TelsLieu) {
            $eventlieu->removeTelsLieu($eventlieu->getTelsLieu());
        }
        if (!is_null($eventlieu->getCountry())) {
            $eventlieu->setCountry(null);
        }
        if (!is_null($eventlieu->getRegion())) {
            $eventlieu->setRegion(null);
        }
        if (!is_null($eventlieu->getQuartier())) {
            $eventlieu->setQuartier(null);
        }
        if (!is_null($eventlieu->getLocality())) {
            $eventlieu->setLocality(null);
        }

        return $eventlieu;
    }

    public function getMainArtistsInfo($eventArtistesDjOrganisateurs)
    {
        foreach ($eventArtistesDjOrganisateurs as $artists) {
            $data[] = [
                'id' => $artists->getId(), 'vztid' => $artists->getVztid(), 'name' => $artists->getName(), 'slug' => $artists->getSlug(), 'type' => $artists->getType(),
            ];
        }

        return count($eventArtistesDjOrganisateurs) > 0 ? $data : null;
    }

    /**
     * @QueryParam(name="query", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getCountriesAction(ParamFetcher $paramFetcher)
    {
        // print_r($query);
        $em = $this->getDoctrine()->getManager();

        $query = array();
        if ($paramFetcher->get('query') != '') {
            $query['query'] = $paramFetcher->get('query');
        }

        $row = $em->getRepository('mdtshomeBundle:countries')->findAllQuery($query);

        return $row;
    }

    /**
     * @QueryParam(name="country_id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getRegionsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $query = array();
        if ($paramFetcher->get('country_id') > 0) {
            $query['country_id'] = $paramFetcher->get('country_id');
        }

        $row = $em->getRepository('mdtshomeBundle:region')->findAllQuery($query);

        return $row;
    }

    /**
     * @QueryParam(name="region_id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getLocalitiesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('region_id') > 0) {
            $query['region_id'] = $paramFetcher->get('region_id');
        }

        $row = $em->getRepository('mdtshomeBundle:locality')->findAllQuery($query);

        return $row;
    }

    /**
     * @param ParamFetcher $paramFetcher
     */
    public function getThematiquesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('mdtshomeBundle:EventType')->findAll();

        return array('results' => $row);
    }

    /**
     * @param ParamFetcher $paramFetcher
     */
    public function getEventflyersAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('mdtshomeBundle:EventFlyers')->findAll();

        return array('results' => $row);
    }

    /**
     * @param ParamFetcher $paramFetcher
     */
    public function getEventvideosAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('mdtshomeBundle:EventVideos')->findAll();

        return array('results' => $row);
    }

    /**
     * @QueryParam(name="ville_id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getQuartiersAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('ville_id') > 0) {
            $query['ville_id'] = $paramFetcher->get('ville_id');
        }

        $row = $em->getRepository('mdtshomeBundle:quartier')->findAllQuery($query);

        return $row;
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="nolieu", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getLieuxAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        $queryC = array();
        $queryQ = array();
        $queryR = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
            $queryC['query'] = $paramFetcher->get('name');
            $queryQ['query'] = $paramFetcher->get('name');
            $queryR['query'] = $paramFetcher->get('name');
        }
        if ($paramFetcher->get('id') > 0) {
            return $em->getRepository('mdtshomeBundle:EventLieu')->findInId(explode(',', $paramFetcher->get('id')));
        }
        if ($paramFetcher->get('nolieu') == 1) {

        }

        $row = $em->getRepository('mdtshomeBundle:EventLieu')->findAllQuery($query,0,0,true);
        $rowC = $em->getRepository('mdtshomeBundle:countries')->findAllQuery($queryC);
        $rowR = $em->getRepository('mdtshomeBundle:region')->findByQuery($queryR);
        $rowQ = $em->getRepository('mdtshomeBundle:quartier')->findByQuery($queryQ);
        $rowL = $em->getRepository('mdtshomeBundle:locality')->findByQuery($queryQ);

        $arrLieu = $row->getResult();
        $arrCountry = $rowC;
        $arrQuartier = $rowQ;
        $arrRegion = $rowR;
        $arrLocality = $rowL;

        if ($paramFetcher->get('nolieu') == 1) {
            return array_merge( $arrCountry, $arrRegion, $arrLocality, $arrQuartier);
        }

        return array_merge(['lieu' => $arrLieu], ['country' => $rowC], ['region' => $rowR],['commune' => $rowL], ['quartier' => $rowQ] );

    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
        }

        $rows = $em->getRepository('mdtshomeBundle:Event')->findAllQuery($query,0,0,true);

        if ($paramFetcher->get('id') != '') {
            $rows = $em->getRepository('mdtshomeBundle:Event')->findByIds(explode(',', $paramFetcher->get('id')));
        }

        $cacheManager = $this->container->get('liip_imagine.cache.manager');

        $data = array();
        foreach ($rows->getResult() as $row) {
            if ($paramFetcher->get('name') != '') {
                $row['flyer'] = $cacheManager->getBrowserPath('assets/flyers/'.$row['flyer'], 'my_thumb_70');
            }
            $data[] = $row;
        }

        return $data;
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getArticledetailAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $paramFetcher->get('id');
        if (!$id) {
            throw $this->createNotFoundException('Data not found.');
        }

        $row = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($id);
        $event = array();
        $i = 0;
        foreach ($em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($id) as $events) {
            $event[$i] = $events;
            ++$i;
        }
        $artiste = array();
        $i = 0;
        foreach ($em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByArticleId($id) as $events) {
            $artiste[$i] = $events;
            ++$i;
        }

        return ['articleID' => intval($id), 'event_count' => count($event), 'artiste_count' => count($artiste),  'events' => $event, 'artiste' => $artiste];
    }
    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventdetailAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $row = $em->getRepository('mdtshomeBundle:Event')->find($paramFetcher->get('id'));

        return $row;
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getArtistedetailAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $row = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($paramFetcher->get('id'));

        $groupartistesByArtisteId = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getGroupartistesByArtisteId($paramFetcher->get('id'));
        $parentartisteid = [];
        foreach ($groupartistesByArtisteId as $data) {
            $parentartisteid[] = $data;
        }

        // return $row; 
        

        $rows = [
            'id' => $row->getId(),
            'name' => $row->getName(),
            'position' => $row->getPosition(),
            'slug' => $row->getSlug(),
            'orig_slug' => $row->getOrigSlug(),
            'vztid' => $row->getVztid(),
            'type' => $row->getType(),
            'photo' => $row->getPhoto(),
            'enfant_artiste' => $row->getParentArtiste(),
            'parent_artiste' => $parentartisteid,
        ]; 
        
         
        return $rows;
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getCurrenteventAction(ParamFetcher $paramFetcher)
    {
        $nextpage = 0;
        $limit = 20;

        $day = date('w') - 1;
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]);

        $ids = explode(',', file_get_contents(dirname(__FILE__).'/ids.txt'));
        //return $ids;
        if (count($ids) == 2) {
            //$rows = range($ids[0], $ids[1]);
            //return $rows;
        }

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="home", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getCurrenteventmonthAction(ParamFetcher $paramFetcher, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $home = 0;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('home') != '') {
            $home = $paramFetcher->get('home');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $dates = $em->getRepository('mdtshomeBundle:Event')->getDatesArrayNextEvent();

        //return [$dates['date_from'],$dates['date_to']];

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$dates['date_from'], $dates['date_to']]);

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="startdate", requirements="^\d{2}-\d{2}-\d{4}$",  description="queru search")
     * @QueryParam(name="enddate", requirements="^\d{2}-\d{2}-\d{4}$",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllideventbydatesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $startdate = '';
        $enddate = '';

        if ($paramFetcher->get('startdate') != '') {
            $startdate = $paramFetcher->get('startdate');
            //Convertir le format des dates
            $date = \DateTime::createFromFormat('j-m-Y', $startdate);
            $startdate = $date->format('Y-m-d');
        }
        if ($paramFetcher->get('enddate') != '') {
            $enddate = $paramFetcher->get('enddate');
            //Convertir le format des dates
            $date = \DateTime::createFromFormat('j-m-Y', $enddate);
            $enddate = $date->format('Y-m-d');
        }

        if ($startdate == '' && $enddate != '') {
            $startdate = $enddate;
        } elseif ($startdate != '' && $enddate == '') {
            $enddate = $startdate;
        } elseif ($startdate == '' && $enddate == '') {
            $enddate = $startdate = date('Y-m-d');
        }

        if ($startdate > $enddate) {
            $tmp_startdate = $startdate;
            $tmp_enddate = $enddate;

            //intervertir
            $startdate = $tmp_enddate;
            $enddate = $tmp_startdate;
        }

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$startdate, $enddate]);

        return  ['ids' => implode(',', $rows)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="startdate", requirements=".*",  description="queru search")
     * @QueryParam(name="enddate", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbydatesAction(ParamFetcher $paramFetcher, Request $request)
    {
        $regexpdate = '/^\d{2}-\d{2}-\d{4}$/';
        $defaultFormat = 'j-m-Y';

        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $startdate = '';
        $enddate = '';
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('startdate') != '') {
            $startdate = $paramFetcher->get('startdate');
        }
        if ($paramFetcher->get('enddate') != '') {
            $enddate = $paramFetcher->get('enddate');
        }

        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($startdate != '' && !preg_match($regexpdate, $startdate)) {
            $startdate = $this->convertStringToDate($startdate, $defaultFormat);
        }
        if ($enddate != '' && !preg_match($regexpdate, $enddate)) {
            $enddate = $this->convertStringToDate($enddate, $defaultFormat);
        }

        if ($startdate == '' && $enddate != '') {
            $startdate = $enddate;
        } elseif ($startdate != '' && $enddate == '') {
            $enddate = $startdate;
        } elseif ($startdate == '' && $enddate == '') {
            $enddate = $startdate = date('j-m-Y');
        }

        //Convertir le format des dates
        $date = \DateTime::createFromFormat('j-m-Y', $startdate);
        $startdate = $date->format('Y-m-d');

        $date = \DateTime::createFromFormat('j-m-Y', $enddate);
        $enddate = $date->format('Y-m-d');

        if ($startdate > $enddate) {
            $tmp_startdate = $startdate;
            $tmp_enddate = $enddate;

            //intervertir
            $startdate = $tmp_enddate;
            $enddate = $tmp_startdate;
        }

        //return [$startdate,$enddate ];

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$startdate, $enddate]);

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="slug", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbytypeslugAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $slug = '';
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('slug') != '') {
            $slug = $paramFetcher->get('slug');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromType($slug);

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="slug", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbylieuslugAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $slug = '';
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('slug') != '') {
            $slug = $paramFetcher->get('slug');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromPlace($slug);

        return $this->getEventResults($rows, $limit, $nextpage);
    }
    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbyartisteidsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $id = '';
        $nextpage = 0;
        $ids_array = array();

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $ids_array = explode(',', $id);
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findByIds($ids_array);
        if (!$entity) {
            throw new HttpException(404, 'User not found');
        }

        $event_id = array();

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByArtisteId($ids_array);
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        //return $event_id;

        return $this->getEventResults($event_id, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbylieuxidsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $id = '';
        $nextpage = 0;
        $ids_array = array();

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $ids_array = explode(',', $id);
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->findByIds($ids_array);
        if (!$entity) {
            throw new HttpException(404, 'User not found');
        }

        $event_id = array();

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByLieuxId($ids_array);
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        //return $event_id;

        return $this->getEventResults($event_id, $limit, $nextpage);
    }
    /**
     * @QueryParam(name="slug", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbyartisteslugAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $slug = '';
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('slug') != '') {
            $slug = $paramFetcher->get('slug');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findBySlug($slug);
        if (!$entity || $entity[0]->getId() == 1) {
            throw new HttpException(404, 'User not found');
        }

        $event_id = array();

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByArtisteId($entity[0]->getId());
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        //return $event_id;

        return $this->getEventResults($event_id, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="query", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getSearcheventAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $query = '';
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }

        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }
        if ($paramFetcher->get('query') != '') {
            $query = $paramFetcher->get('query');
        }

        $rows = $this->get('madatsara.service')->searchEventByQuery($query);

        return $this->getEventResults($rows, $limit, $nextpage);
    }

    /**
     * @QueryParam(name="query", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidsearcheventAction(ParamFetcher $paramFetcher)
    {
        $query = '';

        if ($paramFetcher->get('query') != '') {
            $query = $paramFetcher->get('query');
        }

        $rows = $this->get('madatsara.service')->searchEventByQuery($query);

        return ['ids' => implode(',', $rows)];
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getLocalAction(ParamFetcher $paramFetcher)
    {
        $id = $paramFetcher->get('id');
        $arrId = explode(',',$id);
        $firstId = ctype_digit($arrId[0]);

//        dump($firstId);

        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
        }

        $row = $em->getRepository('mdtshomeBundle:EventLocal')->findAllQuery($query,0,0,true);

        if (count($arrId)>0 && $firstId>0) {
            $row = $em->getRepository('mdtshomeBundle:EventLocal')->findByIds(explode(',', $paramFetcher->get('id')));
        }


        return $row->getResult();
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidlocalAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
        }

        if ($paramFetcher->get('id') > 0) {
            return $em->getRepository('mdtshomeBundle:EventLocal')->find($paramFetcher->get('id'));
        }

        //return $query;
        //$row  = $em->getRepository('mdtshomeBundle:EventLieu')->findAllQuery($query);
        $row = $em->getRepository('mdtshomeBundle:EventLocal')->findAllQuery($query,0,0,true);

        foreach ($row->getResult() as $row) {
            $event_id[] = $row->getId();
        }

        return ['ids' => implode(',', $event_id)];
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getArtistesdjAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
        }

        if (count($query) > 0) {
            $row = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findAllQuery($query,0,0,true);
        }


        if ($paramFetcher->get('id') != '') {
            $row = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findByIds(explode(',', $paramFetcher->get('id')));
        }

        if (count($row) <= 0) {
            return [];
        }

        return $this->getArtistsResults($row->getResult());
    }

    /**
     * @QueryParam(name="withphoto", requirements=".*",  description="queru search")
     * @QueryParam(name="os", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getArtistescurrentweekAction(ParamFetcher $paramFetcher)
    {
        $withphoto = '';
        $os = '';
        if ($paramFetcher->get('withphoto') != '') {
            $withphoto = $paramFetcher->get('withphoto');
        }
        if ($paramFetcher->get('os') != '') {
            $os = $paramFetcher->get('os');
        }

        $em = $this->getDoctrine()->getManager();
        $artiste = $artisteIDs = array();
        $i = 0;

        $day = date('w') - 1;
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

        $em = $this->getDoctrine()->getManager();
        $eventIDS = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]);

        foreach ($eventIDS as $eventID) {
            $artiste[] = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByEventId($eventID);
        }

        foreach ($artiste as $artiste_array) {
            if (count($artiste_array) > 0) {
                foreach ($artiste_array as $artisteID) {
                    $artisteIDs[] = $artisteID;
                }
            }
        }

        if (count($artisteIDs) <= 0) {
            return [];
        }

        $row = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findByIds($artisteIDs, $withphoto);

        return $this->getArtistsResults($row->getResult(), $os);
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getLieuxcurrentweekAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $lieux = $lieuxIDs = array();
        $i = 0;

        $day = date('w') - 1;
        $date_from = date('Y-m-d');
        $date_to = date('Y-m-d', strtotime('+'.(6 - $day).' days'));

        $em = $this->getDoctrine()->getManager();
        $eventIDS = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]);

        //$em->getRepository('mdtshomeBundle:EventLieu')->getLieuxByEventId(4893) ;exit;

        foreach ($eventIDS as $eventID) {
            $lieux [] = $em->getRepository('mdtshomeBundle:EventLieu')->getLieuxByEventId($eventID);
        }

        foreach ($lieux as $artiste_array) {
            if (count($artiste_array) > 0) {
                foreach ($artiste_array as $artisteID) {
                    $lieuxIDs[] = $artisteID;
                }
            }
        }

        if (count($lieuxIDs) <= 0) {
            return [];
        }

        return $em->getRepository('mdtshomeBundle:EventLieu')->findInId($lieuxIDs);
    }

    /**
     * @QueryParam(name="registration_id", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getRegistrationidAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('registration_id') != '') {
            $query['registration_id'] = $paramFetcher->get('registration_id');
        }

        $row = $em->getRepository('mdtsFrontendBundle:registrationid')->findAllQuery($query);

        if ($paramFetcher->get('id') != '') {
            $row = $em->getRepository('mdtsFrontendBundle:registrationid')->findByIds(explode(',', $paramFetcher->get('id')));
        }

        return array('results' => $row->getResult());
    }

    /**
     * @QueryParam(name="image", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return array
     */
    public function getTesseractAction(ParamFetcher $paramFetcher)
    {
        $fs = new Filesystem();
        $path = $this->get('kernel')->getRootDir().'/../web/tmp/';

        if ($paramFetcher->get('image') != '' && $fs->exists($path.$paramFetcher->get('image'))) {
            $filename_path = $path.$paramFetcher->get('image');

            $cmd = 'tesseract '.$filename_path.' '.$path.'out -l fra';
            //echo $cmd;

            $configuration = new \Ssh\Configuration($this->getParameter('ssh_host'));
            $authentication = new \Ssh\Authentication\Password($this->getParameter('ssh_username'), $this->getParameter('ssh_password'));
            $session = new \Ssh\Session($configuration, $authentication);
            $exec = $session->getExec();
            $exec->run($cmd);

            return array('success' => 'OK', 'outcontent' => file_get_contents($path.'out.txt'));
        }

        return array('error' => 'Erreur sauvegarde pays.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return array
     */
    public function postSavecountriesAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '') {
            $entity = new countries();
            $entity->setName($paramFetcher->get('name'));
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateCountry($entity);

            return array('success' => 'Pays '.$entity->getName().' sauvegardé.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde pays.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     * @RequestParam(name="country_id", requirements="[0-9]+",  description="country_id", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveregionAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '' && $paramFetcher->get('country_id') > 0) {
            $em = $this->getDoctrine()->getManager();
            $entitycountry = $em->getRepository('mdtshomeBundle:countries')->find($paramFetcher->get('country_id'));

            $entity = new region();
            $entity->setName($paramFetcher->get('name'));
            $entity->setCountry($entitycountry);
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateRegion($entity);

            return array('success' => 'Region '.$entity->getName().' sauvegardée.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde region.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     * @RequestParam(name="region_id", requirements="[0-9]+",  description="country_id", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSavelocalityAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '' && $paramFetcher->get('region_id') > 0) {
            $em = $this->getDoctrine()->getManager();
            $entitylocality = $em->getRepository('mdtshomeBundle:region')->find($paramFetcher->get('region_id'));

            $entity = new locality();
            $entity->setName($paramFetcher->get('name'));
            $entity->setRegion($entitylocality);
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateLocality($entity);

            return array('success' => 'Ville '.$entity->getName().' sauvegardée.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde ville.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     * @RequestParam(name="ville_id", requirements="[0-9]+",  description="country_id", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSavequartierAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '' && $paramFetcher->get('ville_id') > 0) {
            $em = $this->getDoctrine()->getManager();
            $entityville = $em->getRepository('mdtshomeBundle:locality')->find($paramFetcher->get('ville_id'));

            $entity = new quartier();
            $entity->setName($paramFetcher->get('name'));
            $entity->setLocality($entityville);
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateQuartier($entity);

            return array('success' => 'Quartier '.$entity->getName().' sauvegardé.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde quartier.');
    }

    /**
     * @RequestParam(name="country", requirements=".*",  description="name")
     * @RequestParam(name="region", requirements=".*",  description="name")
     * @RequestParam(name="locality", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveallAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $entitycountry = $em->getRepository('mdtshomeBundle:countries')->findByName($paramFetcher->get('country'));
            //echo $entitycountry[0]->getName();exit();
            if (!$entitycountry) {
                $entitycountry = new countries();
                $entitycountry->setName($paramFetcher->get('country'));
                $em->persist($entitycountry);
                $em->flush();
            } else {
                $entitycountry = $entitycountry[0];
            }

        $entityregion = $em->getRepository('mdtshomeBundle:region')->findByNameAndCountries($paramFetcher->get('region'), $entitycountry->getId());
        if (!$entityregion) {
            $entityregion = new region();
            $entityregion->setName($paramFetcher->get('region'));
            $entityregion->setCountry($entitycountry);
            $em->persist($entityregion);
            $em->flush();
        } else {
            $entityregion = $entityregion[0];
        }

        $entitylocality = $em->getRepository('mdtshomeBundle:locality')->findByNameAndRegion($paramFetcher->get('locality'), $entityregion->getId());
        if (!$entitylocality) {
            $entitylocality = new locality();
            $entitylocality->setName($paramFetcher->get('locality'));
            $entitylocality->setRegion($entityregion);
            $em->persist($entitylocality);
            $em->flush();
        } else {
            $entitylocality = $entitylocality[0];
        }

        return array(
                'success' => 'Ok sauvegardé', 'country_id' => $entitycountry->getId(), 'country_name' => $entitycountry->getName(), 'region_id' => $entityregion->getId(), 'region_name' => $entityregion->getName(), 'locality_id' => $entitylocality->getId(), 'locality_name' => $entitylocality->getName(),
            );

        return array('error' => 'Erreur sauvegarde quartier.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveentreetypeAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '') {
            $entity = new EntreeType();
            $entity->setName($paramFetcher->get('name'));
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEntreetype($entity);

            return array('success' => 'Entrée '.$entity->getName().' sauvegardée.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde Entrée.');
    }

    /**
     * @RequestParam(name="data", map=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSavelieuAction(ParamFetcher $paramFetcher)
    {
        $id = 0;
        $em = $this->getDoctrine()->getManager();

        $data = $paramFetcher->all();
        $data = $data['data'];

        // ID si besoin
        if (array_key_exists('id', $data) && $data['id']>0) {
            $id = $data['id'];
        }

        // Create entity  
        if ($id === 0) {
            $entity = new EventLieu();
        }
        if ($id > 0) {
            $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);
        }

        $name = array_key_exists('name', $data) ? $data['name'] : '';
        $tel = array_key_exists('tel', $data) ? $data['tel'] : '';
        $email = array_key_exists('email', $data) ? $data['email'] : '';
        $facebook = array_key_exists('facebook', $data) ? $data['facebook'] : '';
        $www = array_key_exists('www', $data) ? $data['www'] : '';
        $gps = array_key_exists('gps', $data) ? $data['gps'] : '';
        $adresse = array_key_exists('adresse', $data) ? $data['adresse'] : '';
        $pays = array_key_exists('pays', $data) && $data['pays'] > 0 ? $data['pays'] : 0;
        $region = array_key_exists('region', $data) && $data['region'] > 0 ? $data['region'] : 0;
        $ville = array_key_exists('ville', $data) && $data['ville'] > 0 ? $data['ville'] : 0;
        $quartier = array_key_exists('quartier', $data) && $data['quartier'] > 0 ? $data['quartier'] : 0;

        $iNbFields = 0;

        if ($name !== '') {
            $entity->setName($name);
            $iNbFields++;
        }
        if ($tel !== '') {
            $entity->setTel($tel);
            $iNbFields++;
        }
        if ($email !== '') {
            $entity->setEmail($email);
            $iNbFields++;
        }
        if ($facebook !== '') {
            $entity->setFacebook($facebook);
            $iNbFields++;
        }
        if ($www !== '') {
            $entity->setWww($www);
            $iNbFields++;
        }
        if ($logo !== '') {
            $entity->setLogo($logo);
            $iNbFields++;
        }
        if ($gps !== '') {
            $entity->setGps($gps);
            $iNbFields++;
        }
        if ($adresse !== '') {
            $entity->setAdress($adresse);
            $iNbFields++;
        }
        // Pays
        if ($pays > 0) {
            $_id = $pays;
            $_entity = $em->getRepository('mdtshomeBundle:countries')->find($_id);
            $entity->setCountry($_entity);
        }
        if ($pays === 0) {
            $entity->setCountry(null);
        }
        // Region
        if ($region > 0) {
            $_id = $region;
            $_entity = $em->getRepository('mdtshomeBundle:region')->find($_id);
            $entity->setRegion($_entity);
        }
        if ($region === 0) {
            $entity->setRegion(null);
        }
        // Ville
        if ($ville > 0) {
            $_id = $ville;
            $_entity = $em->getRepository('mdtshomeBundle:locality')->find($_id);
            $entity->setLocality($_entity);
        }
        if ($ville === 0) {
            $entity->setLocality(null);
        }
        // Quartier
        if ($quartier > 0) {
            $_id = $quartier;
            $_entity = $em->getRepository('mdtshomeBundle:quartier')->find($_id);
            $entity->setQuartier($_entity);
        }
        if ($quartier === 0) {
            $entity->setQuartier(null);
        }

        if ($entity && $iNbFields > 0) {
            //Enregistrer dans BDD
           $srv = $this->get('madatsara.service')->createOrUpdateEventLieu($entity,$id);
           $id = $entity->getId();
           return array('success' => 'Lieu '.$name.' sauvegardé.', 'id' => $id, 'name' => $name);
        }

        return array('error' => 'Erreur');
        
        
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSavelocalAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '') {
            $entity = new EventLocal();
            $entity->setName($paramFetcher->get('name'));
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventLocal($entity);

            return array('success' => 'Fête '.$entity->getName().' sauvegardé.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde fête.');
    }

    /**
     * @RequestParam(name="data", map=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveartistsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        
        $arrForm = [];
        $id = null;

        $data = $paramFetcher->all();
        $data = $data['data'];

        // ID si besoin
        if (array_key_exists('id', $data) && $data['id']>0)
            $id = $data['id'];

        // Create entity  
        if (is_null($id))
            $entity = new EventArtistesDjOrganisateurs();
        else
            $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);

        if (array_key_exists('name', $data))
            $entity->setName($data['name']); 

        if (array_key_exists('type', $data))
            $entity->setType($data['type']);

        // Form
        if (array_key_exists('api', $data))
            $arrForm['api'] = $data['api'];

        if (array_key_exists('filenameajax', $data))
            $arrForm['filenameajax'] = $data['filenameajax'];

        if (array_key_exists('dragandrop', $data))
            $arrForm['dragandrop'] = $data['dragandrop'];

        if (array_key_exists('event', $data))
            $arrForm['event'] = $data['event'];

        if (array_key_exists('parentartisteid', $data))
            $arrForm['parentartisteid'] = $data['parentartisteid'];

        if (array_key_exists('enfantartisteid', $data))
            $arrForm['enfantartisteid'] = $data['enfantartisteid'];     

 
             
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventArtistesDjOrganisateurs($entity, $arrForm, $id);
            if ($srv) {
                return array('success' => 'Artiste ou DJ '.$entity->getName().' sauvegardé.', 'id' => $entity->getId(), 'name' => $entity->getName());
            }
            
        

        return array('error' => 'Erreur sauvegarde Artiste ou DJ.');
    }

    /**
     * @FileParam(name="flyer", image=true, default="noPicture")
     * @RequestParam(name="oldfilename", requirements=".*",  description="name")
     * @RequestParam(name="flyerurl", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postUploadflyerAction(ParamFetcher $paramFetcher)
    {
        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($paramFetcher->all(), true));
        $repertoire = $this->container->getParameter('kernel.root_dir').'/../web/tmp/';
        $str_suppr = '';

        if ($paramFetcher->get('flyer') != 'noPicture') {
            $file = $paramFetcher->get('flyer');
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($repertoire, $fileName);

            if ($paramFetcher->get('oldfilename') != '') {
                $fs = new Filesystem();
                $path = $this->get('kernel')->getRootDir().'/../web/tmp/';
                if ($fs->exists($path.$paramFetcher->get('oldfilename'))) {
                    $fs->remove($path.$paramFetcher->get('oldfilename'));
                    $str_suppr = ' et ancien fichier '.$paramFetcher->get('oldfilename').' supprimé';
                }
            }

            return array('success' => 'Fichier enregistré sous le nom <strong>'.$fileName.'</strong>.'.$str_suppr, 'filename' => $fileName);
        } elseif ($paramFetcher->get('flyerurl') != '') {
            $data = $paramFetcher->get('flyerurl');
            $data = str_replace('data:image/png;base64,', '', $data);
            $data = str_replace('data:image/jpeg;base64,', '', $data);

            $fileName = md5(uniqid()).'.jpg';
            $fileName_txt = md5(uniqid()).'.txt';

            $fs = new Filesystem();
            $fs->dumpFile($repertoire.$fileName, base64_decode($data));
            $fs->dumpFile($repertoire.$fileName_txt,  ($data));

            if ($paramFetcher->get('oldfilename') != '') {
                $path = $this->get('kernel')->getRootDir().'/../web/tmp/';
                if ($fs->exists($path.$paramFetcher->get('oldfilename'))) {
                    $fs->remove($path.$paramFetcher->get('oldfilename'));
                    $str_suppr = ' et ancien fichier '.$paramFetcher->get('oldfilename').' supprimé';
                }
            }

            return array('success' => 'Fichier enregistré sous le nom <strong>'.$fileName.'</strong>.'.$str_suppr, 'filename' => $fileName);
        }

        return array('error' => 'Erreur sauvegarde Fichier.');
    }



    /**
     * @RequestParam(name="registration_id", requirements=".*",  description="name")
     * @RequestParam(name="android_modelname", requirements=".*",  description="name")
     * @RequestParam(name="android_version", requirements=".*",  description="name")
     * @RequestParam(name="device_id", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveregistrationidAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('registration_id') != '') {
            $registrationid = $paramFetcher->get('registration_id');
            $android_modelname = $paramFetcher->get('android_modelname');
            $android_version = $paramFetcher->get('android_version');
            $device_id = $paramFetcher->get('device_id');
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateRegistrationid($registrationid, $android_modelname, $android_version, $device_id);

            //Envoyer mail admin
            $infos = array(
                'model' => $android_modelname,
                'Android version' => $android_version,
                'IP' => $this->get('request_stack')->getCurrentRequest()->getClientIp(),
            );

            $path_geoipmmdb_exists = $this->get('madatsara.service')->hasParameter('path_geoipmmdb');
            if ($path_geoipmmdb_exists) {
                $path_geoipmmdb = $this->get('madatsara.service')->getPathParam('path_geoipmmdb');
                $reader = new Reader($path_geoipmmdb);
                $record = $reader->city($this->get('request_stack')->getCurrentRequest()->getClientIp());

                $infos = array_merge($infos, array(
                    'Country isoCode' => $record->country->isoCode,
                    'Country' => $record->country->names['fr'],
                    'City' => $record->city->names['fr'],
                    'TimeZone' => $record->location->timeZone,
                ));
            }

            $subject = '[MADATSARA App] - Nouveau terminal connecté';
            if (false === $srv) {
                $subject = '[MADATSARA App] - terminal connecté existant';
            }

            //$ladybug = new  \Ladybug\Dumper();
            $to = '';
            $from = '';
            $default_email = $this->get('madatsara.service')->getParameter('default_email');
            if ( array_key_exists('to', $default_email)) {
                $to = $default_email['to'];
                $from = $default_email['from'];
            }
            $mail = \Swift_Message::newInstance();
            $mail
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject)
                ->setBody($this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'mdtshomeBundle:Email:generique.html.twig',
                    array(

                        'content' => $this->get('madatsara.service')->array2table($infos),
                    )
                ))
                ->setContentType('text/html');

            $this->get('mailer')->send($mail);

            return array('success' => 'Registration ID '.$registrationid.' sauvegardé.', 'registrationid' => $registrationid);
        }

        return array('error' => 'Erreur sauvegarde Registration ID.');
    }

    /**
     * @RequestParam(name="email", requirements=".*",  description="name")
     * @RequestParam(name="emailRS", requirements=".*",  description="name")
     * @RequestParam(name="nick_name", requirements=".*",  description="name")
     * @RequestParam(name="profile_picture", requirements=".*",  description="name")
     * @RequestParam(name="last_name", requirements=".*",  description="name")
     * @RequestParam(name="real_name", requirements=".*",  description="name")
     * @RequestParam(name="middle_name", requirements=".*",  description="name")
     * @RequestParam(name="expire_in", requirements=".*",  description="name")
     * @RequestParam(name="service", requirements=".*",  description="name")
     * @RequestParam(name="token", requirements=".*",  description="name")
     * @RequestParam(name="device_id", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveuserAction(ParamFetcher $paramFetcher)
    {
        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($paramFetcher->all(), true));

        $data = array();
        if ($paramFetcher->get('email') != '') {
            $data['email'] = $paramFetcher->get('email');
        }
        if ($paramFetcher->get('nick_name') != '') {
            $data['nick_name'] = $paramFetcher->get('nick_name');
        }
        if ($paramFetcher->get('profile_picture') != '') {
            $data['profile_picture'] = $paramFetcher->get('profile_picture');
        }
        if ($paramFetcher->get('last_name') != '') {
            $data['last_name'] = $paramFetcher->get('last_name');
        }
        if ($paramFetcher->get('real_name') != '') {
            $data['real_name'] = $paramFetcher->get('real_name');
        }
        if ($paramFetcher->get('middle_name') != '') {
            $data['middle_name'] = $paramFetcher->get('middle_name');
        }
        if ($paramFetcher->get('expire_in') != '') {
            $data['expire_in'] = $paramFetcher->get('expire_in');
        }
        if ($paramFetcher->get('service') != '') {
            $data['service'] = $paramFetcher->get('service');
        }
        if ($paramFetcher->get('token') != '') {
            $data['token'] = $paramFetcher->get('token');
        }
        if ($paramFetcher->get('emailRS') != '') {
            $data['emailRS'] = $paramFetcher->get('emailRS');
        }
        if ($paramFetcher->get('device_id') != '') {
            $data['device_id'] = $paramFetcher->get('device_id');
        }

        if (count($data) > 0) {
            $srv = $this->get('madatsara.service')->createOrUpdateUser($data);

            return array('success' => 'Utilisateur sauvegardé.');
        }

        return array('error' => 'Erreur sauvegarde utilisateur.');
    }

    /**
     * @QueryParam(name="id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getDeleteflyerAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('id') > 0) {
            $srv = $this->get('madatsara.service')->deleteFlyer($paramFetcher->get('id'));

            return array('success' => 'Flyer supprimé.');
        }

        return array('error' => 'Erreur suppression flyer.');
    }

    /**
     * @QueryParam(name="id", requirements="[0-9]+",  description="queru search")
     * @QueryParam(name="eventid", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getSetflyerAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('id') > 0 && $paramFetcher->get('eventid') > 0) {
            $srv = $this->get('madatsara.service')->setFlyer($paramFetcher->get('id'), $paramFetcher->get('eventid'));

            return array('success' => 'Flyer modifié avec succès.', 'id' => $srv);
        }

        return array('error' => 'Erreur modification flyer.');
    }

    /**
     * @QueryParam(name="name", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getRelatedeventAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();
        if ($paramFetcher->get('name') != '') {
            $query['query'] = $paramFetcher->get('name');
        }

        $row = $em->getRepository('mdtshomeBundle:Event')->findAllQuery($query,0,0,true);

        if ($paramFetcher->get('id') != '') {
            $row = $em->getRepository('mdtshomeBundle:Event')->findByIds(explode(',', $paramFetcher->get('id')));
        }

        return $row->getResult();
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllrelatedeventAction(ParamFetcher $paramFetcher)
    {
        $row = array();

        $em = $this->getDoctrine()->getManager();

        $i = 0;

        $limit = 5;
        $nextpage = 0;

        $data = array();
        $allids = array();
        $total = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            //$row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEvent($id);

            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByID($id);
            //Supprimer les doublons
            $row = array_unique(array_values($row));
            $allids = $row;
            //Exclure ID en cours
            if (($key = array_search($id, $row)) !== false) {
                unset($row[$key]);
            }
            $rowAll = $row;
            if (count($rowAll) > 0) {
                $i += 1;
            }

            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByLocalID($id);
            //Supprimer les doublons
            $row = array_unique(array_values($row));
            //Exclure ID en cours
            if (($key = array_search($id, $row)) !== false) {
                unset($row[$key]);
            }
            //Exclure les ids deja existants
            if (count($allids) > 0) {
                foreach ($allids as $ids) {
                    if (($key = array_search($ids, $row)) !== false) {
                        unset($row[$key]);
                    }
                }
            }
            $allids = array_merge($allids, $row);
            $rowAllLocal = $row;
            if (count($rowAllLocal) > 0) {
                $i += 1;
            }

            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByArtistsID($id);
            //Supprimer les doublons
            $row = array_unique(array_values($row));
            //Exclure ID en cours
            if (($key = array_search($id, $row)) !== false) {
                unset($row[$key]);
            }
            //Exclure les ids deja existants
            if (count($allids) > 0) {
                foreach ($allids as $ids) {
                    if (($key = array_search($ids, $row)) !== false) {
                        unset($row[$key]);
                    }
                }
            }
            $allids = array_merge($allids, $row);
            $rowAllArtists = $row;
            if (count($rowAllArtists) > 0) {
                $i += 1;
            }

            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByPlacesID($id);
            //Supprimer les doublons
            $row = array_unique(array_values($row));
            //Exclure ID en cours
            if (($key = array_search($id, $row)) !== false) {
                unset($row[$key]);
            }
            //Exclure les ids deja existants
            if (count($allids) > 0) {
                foreach ($allids as $ids) {
                    if (($key = array_search($ids, $row)) !== false) {
                        unset($row[$key]);
                    }
                }
            }
            $rowAllPlaces = $row;
            if (count($rowAllPlaces) > 0) {
                $i += 1;
            }

            switch ($i) {

                case 1:
                    $limit = 20;
                    break;
                case 2:
                    $limit = 10;
                    break;
                case 3:
                    $limit = 7;
                    break;
                case 4:
                    $limit = 5;
                    break;
            }

            $limitAll = $limit;
            $limitLocal = $limit;
            $limitArtists = $limit;
            $limitPlaces = $limit;

            if ($i == 2) {
                if (count($rowAll) > 10) { //ex:16
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 10) {
                        //ex: 4
                        $limitAll += (10 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 10) {
                        //ex: 4
                        $limitAll += (10 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 10) {
                        //ex: 4
                        $limitAll += (10 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllLocal) > 10) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 10) {
                        //ex: 4
                        $limitLocal += (10 - count($rowAll));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 10) {
                        //ex: 4
                        $limitLocal += (10 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 10) {
                        //ex: 4
                        $limitLocal += (10 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllArtists) > 10) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 10) {
                        //ex: 4
                        $limitArtists += (10 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 10) {
                        //ex: 4
                        $limitArtists += (10 - count($rowAllLocal));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 10) {
                        //ex: 4
                        $limitArtists += (10 - count($rowAllPlaces));
                    }
                }

                if (count($limitPlaces) > 10) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 10) {
                        //ex: 4
                        $limitPlaces += (10 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 10) {
                        //ex: 4
                        $limitPlaces += (10 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 10) {
                        //ex: 4
                        $limitPlaces += (10 - count($rowAllArtists));
                    }
                }
            }
            if ($i == 3) {
                if (count($rowAll) > 7) { //ex:16
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 7) {
                        //ex: 4
                        $limitAll += (7 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 7) {
                        //ex: 4
                        $limitAll += (7 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 7) {
                        //ex: 4
                        $limitAll += (7 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllLocal) > 7) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 7) {
                        //ex: 4
                        $limitLocal += (7 - count($rowAll));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 7) {
                        //ex: 4
                        $limitLocal += (7 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 7) {
                        //ex: 4
                        $limitLocal += (7 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllArtists) > 7) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 7) {
                        //ex: 4
                        $limitArtists += (7 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 7) {
                        //ex: 4
                        $limitArtists += (7 - count($rowAllLocal));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 7) {
                        //ex: 4
                        $limitArtists += (7 - count($rowAllPlaces));
                    }
                }

                if (count($limitPlaces) > 7) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 7) {
                        //ex: 4
                        $limitPlaces += (7 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 7) {
                        //ex: 4
                        $limitPlaces += (7 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 7) {
                        //ex: 4
                        $limitPlaces += (7 - count($rowAllArtists));
                    }
                }
            }
            if ($i == 4) {
                if (count($rowAll) > 5) { //ex:16
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 5) {
                        //ex: 4
                        $limitAll += (5 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 5) {
                        //ex: 4
                        $limitAll += (5 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 5) {
                        //ex: 4
                        $limitAll += (5 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllLocal) > 5) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 5) {
                        //ex: 4
                        $limitLocal += (5 - count($rowAll));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 5) {
                        //ex: 4
                        $limitLocal += (5 - count($rowAllArtists));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 5) {
                        //ex: 4
                        $limitLocal += (5 - count($rowAllPlaces));
                    }
                }

                if (count($rowAllArtists) > 5) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 5) {
                        //ex: 4
                        $limitArtists += (5 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 5) {
                        //ex: 4
                        $limitArtists += (5 - count($rowAllLocal));
                    }
                    if (count($rowAllPlaces) > 0 && count($rowAllPlaces) < 5) {
                        //ex: 4
                        $limitArtists += (5 - count($rowAllPlaces));
                    }
                }

                if (count($limitPlaces) > 5) { //ex:16
                    if (count($rowAll) > 0 && count($rowAll) < 5) {
                        //ex: 4
                        $limitPlaces += (5 - count($rowAll));
                    }
                    if (count($rowAllLocal) > 0 && count($rowAllLocal) < 5) {
                        //ex: 4
                        $limitPlaces += (5 - count($rowAllLocal));
                    }
                    if (count($rowAllArtists) > 0 && count($rowAllArtists) < 5) {
                        //ex: 4
                        $limitPlaces += (5 - count($rowAllArtists));
                    }
                }
            }

            //return ['nb:'.count($row), implode(', ',$row) ];
            $data['all'] = $this->getEventResults($rowAll, $limitAll, $nextpage, true, $id);
            $data['local'] = $this->getEventResults($rowAllLocal, $limitLocal, $nextpage, true, $id);
            $data['artists'] = $this->getEventResults($rowAllArtists, $limitArtists, $nextpage, true, $id);
            $data['places'] = $this->getEventResults($rowAllPlaces, $limitPlaces, $nextpage, true, $id);

            //return [$i, $data ];
            return  $data;
        }

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        //return ['nb:'.count($row), implode(', ',$row) ];
        return $this->getEventResults($row, $limit, $nextpage, true, $id);
    }
    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidrelatedeventAction(ParamFetcher $paramFetcher)
    {
        $row = array();

        $em = $this->getDoctrine()->getManager();

        $limit = 5;
        $nextpage = 0;

        $data = array();
        $allids = array();
        $total = 0;

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEvent($id);
        }

        //Supprimer les doublons
        $row = array_unique(array_values($row));

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        return ['ids' => implode(',', $row)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllrelatedeventByeventsAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();
        $limit = 5;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByID($id);
        }

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        //return ['nb:'.count($row), implode(', ',$row) ];
        return $this->getEventResults($row, $limit, $nextpage, true, $id);
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidrelatedeventByeventsAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByID($id);
        }

        return ['ids' => implode(',', $row)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllrelatedeventBylocalAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();
        $limit = 5;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByLocalID($id);
        }

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        //return ['nb:'.count($row), implode(', ',$row) ];
        return $this->getEventResults($row, $limit, $nextpage, true, $id);
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidrelatedeventBylocalAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByLocalID($id);
        }

        return ['ids' => implode(',', $row)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllrelatedeventByartistsAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();
        $limit = 5;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByArtistsID($id);
        }

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        //return ['nb:'.count($row), implode(', ',$row) ];
        return $this->getEventResults($row, $limit, $nextpage, true, $id);
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidrelatedeventByartistsAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByArtistsID($id);
        }

        return ['ids' => implode(',', $row)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllrelatedeventBylieuxAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();
        $limit = 5;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByPlacesID($id);
        }

        //Exclure ID en cours
        if (($key = array_search($id, $row)) !== false) {
            unset($row[$key]);
        }

        //return ['nb:'.count($row), implode(', ',$row) ];
        return $this->getEventResults($row, $limit, $nextpage, true, $id);
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidrelatedeventBylieuxAction(ParamFetcher $paramFetcher)
    {
        $row = array();
        $em = $this->getDoctrine()->getManager();

        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $row = $em->getRepository('mdtshomeBundle:Event')->getAllRelatedEventByPlacesID($id);
        }

        return ['ids' => implode(',', $row)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllidarticlesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:ArticlesEvent')->getLastArticles(0, true);
        $allids = array();
        foreach ($rows as $article) {
            $allids[] = $article->getId();
        }

        return  ['ids' => implode(',', $allids)];
    }
    public function getAllideventsMontheventsAction(ParamFetcher $paramFetcher)
    {
        $date_tmp = new \DateTime('now');

        $date_tmp->modify('last day of this month');
        $d2 = $date_tmp->format('Y-m-d');

        $date1 = new \DateTime('now');

        $date2 = new \DateTime($d2);
        $diff = $date2->diff($date1)->format('%a');

        if ($diff <= 4) {
            $date_tmp = new \DateTime('now');

            $date_tmp1 = new \DateTime('now');
            $date_tmp1->modify('last day of next month');
        } elseif ($diff <= 4 && $diff > 1) {
            $date_tmp = new \DateTime('now');
            //$date_tmp->modify('first day of next month');
            //$date_tmp1 = new \DateTime('now');
            $date_tmp1 = new \DateTime('now');
            $date_tmp->modify('last day of next month');

            if ($date_tmp > $date_tmp1) {
                $date_tmp1->modify('last day of next month');
            }
        } else {
            $date_tmp = new \DateTime('now');

            $newDate = new \DateTime();

            $date_tmp->modify(($newDate->format('w') === '0') ? 'now' : 'monday next week');
            $date_tmp1 = new \DateTime('now');

            $date_tmp1->modify('last day of this month');

            if ($date_tmp > $date_tmp1) {
                $date_tmp1->modify('last day of next month');
            }
        }
        //var_dump( $date_tmp>$date_tmp1);
        $date_from = $date_tmp->format('Y-m-d');
        $date_to = $date_tmp1->format('Y-m-d');
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromDates([$date_from, $date_to]);

        return  ['ids' => implode(',', $rows)];
    }
    public function getAllideventsWeekeventsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getCurrentWeekEvent(20, true);

        $allids = array();
        foreach ($rows->getQuery()->getArrayResult() as $row) {
            $allids[] = $row['id'];
        }

        return  ['ids' => implode(',', $allids)];
    }
    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="archive", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllideventslieuAction(ParamFetcher $paramFetcher)
    {
        $archive = intval($paramFetcher->get('archive'));
        $em = $this->getDoctrine()->getManager();

        return ['ids' => implode(',', $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromPlace(intval($paramFetcher->get('id')), $archive))];
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="archive", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllideventstypesAction(ParamFetcher $paramFetcher)
    {
        $archive = intval($paramFetcher->get('archive'));
        $em = $this->getDoctrine()->getManager();

        return ['ids' => implode(',', $em->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromType(intval($paramFetcher->get('id')), $archive))];
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="archive", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllideventsartistsAction(ParamFetcher $paramFetcher)
    {
        $archive = intval($paramFetcher->get('archive'));
        $em = $this->getDoctrine()->getManager();
        $event_id = array();

        $id = $paramFetcher->get('id');
        $ids_array = explode(',', $id);

        $argfunc = (count($ids_array) > 0 ? $ids_array : intval($id));

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByArtisteId($argfunc, $archive);
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        return ['ids' => implode(',', $event_id)];
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="archive", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getAllideventslocalAction(ParamFetcher $paramFetcher)
    {
        $archive = intval($paramFetcher->get('archive'));
        $em = $this->getDoctrine()->getManager();
        $event_id = array();

        $id = $paramFetcher->get('id');
        $ids_array = explode(',', $id);

        $argfunc = (count($ids_array) > 0 ? $ids_array : intval($id));

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByLocalId($argfunc, $archive);
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        return ['ids' => implode(',', $event_id)];
    }

    /**
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getArticlesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $limit = 5;
        $nextpage = 0;

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $event_arr = array();

        $rows = $em->getRepository('mdtshomeBundle:ArticlesEvent')->getLastArticles($limit, true, $nextpage);
        $total = intval($em->getRepository('mdtshomeBundle:ArticlesEvent')->getCountArticles());
        //return $lastarticlesrecents;
        $data = $article_ = array();
        $allids = array();
        $i = 0;

        //return $limit*$paramFetcher->get('offset');

        $position = ($nextpage > 0 ? ($limit * $paramFetcher->get('offset')) : 1);
        foreach ($rows as $article) {
            //echo $article->getTitle()."<br>";
            //$event_arr[] = [ 'article' => $article , 'events' => $em->getRepository('mdtshomeBundle:Event')->getEventByArticleId( $article->getId() ) ];
            $events = $em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($article->getId(), true)->getQuery()->getArrayResult();
            $dataevents = array();
            foreach ($events as $event) {
                $eventid = $event['id'];
                $eventname = $event['name'];
                $eventslug = $event['slug'];
                $eventflyer = $event['flyer'];
                $eventposition = $event['position'];
                $eventdateunique = $event['dateUnique'];
                $eventdatedebut = $event['dateDebut'];
                $eventdatefin = $event['dateFin'];
                $eventheuredebut = $event['heureDebut'];
                $eventheurefin = $event['heureFin'];
                $event['date_unique'] = $event['dateUnique'];
                $event['date_debut'] = $event['dateDebut'];
                $event['date_fin'] = $event['dateFin'];
                $event['heure_debut'] = $event['heureDebut'];
                $event['heure_fin'] = $event['heureFin'];
                unset($event['dateUnique'], $event['dateDebut'], $event['dateFin'], $event['heureDebut'], $event['heureFin']);
                $dataevents[] = $event;
            }
            //$article['event'] = $em->getRepository('mdtshomeBundle:Event')->getEventByArticleId( $article->getId() );
            //$data[] =  array_merge( array('id' => $article->getId().'_'.$eventid  , 'article'=>$article), array( 'events' => $dataevents  ) ) ;

            $article_[$i] = array(
                'id' => $article->getId(), 'url' => $article->getUrl(), 'title' => $article->getTitle(), 'author' => $article->getAuthor(), 'content' => $this->get('madatsara.service')->html_substr($article->getContentPurify(), 0, 400, false, '...'), 'created_at' => $article->getCreatedAt(), 'addartistes_inevent' => $article->getAddartistesInevent(), 'event_id' => $eventid, 'event_name' => $eventname, 'event_slug' => $eventslug, 'event_flyer' => $eventflyer, 'event_position' => $eventposition, 'event_datedebut' => $eventdatedebut, 'event_datefin' => $eventdatefin, 'event_dateunique' => $eventdateunique, 'event_heuredebut' => $eventheuredebut, 'event_heurefin' => $eventheurefin, 'position' => $position,
            );

            $allids[] = $article->getId();

            //$data[] =  $article_  ;
            unset($dataevents);
            ++$i;
            ++$position;
        }

        $data = $article_;
        //$data = array_slice( $article_, ($nextpage>0?$limit*$nextpage:$nextpage), $limit, true );

        $k = array_keys($data);

        $endkey = end($k) + 1;
        if ($nextpage > 0) {
            $endkey += $limit * $nextpage;
        }

        return array('allids' => '', 'end_key' => $endkey, 'endresults' => end($k) + 1 == $total ? true : false, 'total' => $total, 'nextpage' => ($nextpage > 0 ? $limit + $nextpage : $nextpage), 'limit' => $limit, 'results' => array_values($data));
    }

    /**
     * @QueryParam(name="id", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventbylocalidsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $query = array();

        $limit = 4;
        $id = '';
        $nextpage = 0;
        $ids_array = array();

        if ($paramFetcher->get('limit') != '') {
            $limit = $paramFetcher->get('limit');
        }
        if ($paramFetcher->get('id') != '') {
            $id = $paramFetcher->get('id');
            $ids_array = explode(',', $id);
        }
        if ($paramFetcher->get('offset') != '') {
            $nextpage = $paramFetcher->get('offset');
        }

        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->findByIds($ids_array);
        if (!$entity) {
            throw new HttpException(404, 'User not found');
        }

        $event_id = array();

        $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByLocalId($ids_array);
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        //return $event_id;

        return $this->getEventResults($event_id, $limit, $nextpage);
    }

    public function purifier($html)
    {
        //require 'HTMLPurifier.auto.php';
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'em,strong,i,b,p,h1,h2,h3,h4,h5,h6,font[style|size],ol,ul,li,br,div');
        $config->set('AutoFormat.RemoveEmpty',  true);
        $config->set('Cache.SerializerPath', $this->get('kernel')->getRootDir().'/../web/tmp/'); // TODO: remove this later!
        $filter = new HTMLPurifier($config);
        $html = $filter->purify($html);

        return $html;
    }

    /**
     * Convertir une date text en format date.
     *
     * @param string $txtDate - today|tomorrow|yesterday
     * @param string $format  - format d'une date valide
     *
     * @return string - date formattée
     */
    public function convertStringToDate($txtDate, $format)
    {
        switch ($txtDate) {
            case 'today':
            default:
                return date($format, mktime(0, 0, 0));
                break;
            case 'yesterday':
                return date($format, mktime(0, 0, 0, date('n'), date('j') - 1, date('Y')));
                break;
            case 'tomorrow':
                return date($format, mktime(0, 0, 0, date('n'), date('j') + 1, date('Y')));
                break;
        }
    }

    public function getArtistsResults($row, $os = '')
    {
        $row_ = array();
        $i = -1;

        foreach ($row as $data) {
            $data['viewholder'] = 'items';
            $data['photo_png'] = str_replace('.jpg', '.png', $data['photo']);
            $row_[] = $data;

            ++$i;
        }

        if ('android' === $os) {
            array_unshift($row_, array('id' => 9999999, 'viewholder' => 'text', 'name' => 'Artistes dans les événements de la semaine'));
        }

        return array('allids' => '', 'end_key' => $i, 'endresults' => true, 'total' => $i, 'nextpage' => 0, 'limit' => $i, 'results' => $row_);
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveauthorinfotypeAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '') {
            $entity = new EventByMemberAuthorinfoType();
            $entity->setName($paramFetcher->get('name'));
            $entity->setEnable(true);
            $entity->setDontsendEMail(false);
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventByMemberAuthorinfoType($entity);

            return array('success' => 'Entrée '.$entity->getName().' sauvegardée.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde Entrée.');
    }

    /**
     * @RequestParam(name="name", requirements=".*",  description="name")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveemailauthorinfotypeAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('name') != '') {
            $entity = new EventByMemberEmailAuthorinfoType();
            $entity->setName($paramFetcher->get('name'));
            $entity->setEnable(true); 
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventByMemberEmailAuthorinfoType($entity);

            return array('success' => 'Entrée '.$entity->getName().' sauvegardée.', 'id' => $entity->getId(), 'name' => $entity->getName());
        }

        return array('error' => 'Erreur sauvegarde Entrée.');
    }

    /**
     * @RequestParam(name="data", map=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSavethematiqueAction(ParamFetcher $paramFetcher)
    {
        $id = 0;
        $name = '';

        $data = $paramFetcher->all();
        $data = $data['data'];
        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($paramFetcher->all(), true));

        $em = $this->getDoctrine()->getManager();

        // ID si besoin
        if (array_key_exists('id', $data) && $data['id']>0) {
            $id = $data['id'];
        }

        // En cas d'Ajout
        if ($id === 0) {
            $entity = new EntityEventtype();
        }

        // En cas d'edition
        if ($id > 0) {
            $entity = $em->getRepository('mdtshomeBundle:EventType')->find($id);
        }
 
        if (array_key_exists('name', $data)) {
            $name = $data['name'];
            $entity->setName($name);
        }

        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->createOrUpdateEventType($entity, $id);

        // last insert id
        if ($id === 0) {
            $id = $entity->getId();
        }

        if ($srv) {
            return array('success' => 'Entrée '.$name.' sauvegardée.', 'id' => $id, 'name' => $name);
        }

        return array('error' => 'Erreur sauvegarde Entrée.');
    }

    /**
     * @param ParamFetcher $paramFetcher
     */
    public function getEntreetypesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository('mdtshomeBundle:EntreeType')->findAll();

        return array('results' => $row);
    }

    /**
     * @RequestParam(name="data", map=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveeventAction(ParamFetcher $paramFetcher)
    {
        $arrForm = [];
        $id = null;
        $em = $this->getDoctrine()->getManager();

        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($paramFetcher->all(), true));

        $data = $paramFetcher->all();
        $data = $data['data'];

        // ID si besoin
        if (array_key_exists('id', $data) && $data['id']>0)
            $id = $data['id'];

        // Create entity Event
        if (is_null($id))
            $entity = new Event();
        else
            $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);



        if (array_key_exists('name', $data))
            $entity->setName($data['name']);

        if (array_key_exists('prixenclair', $data))
            $entity->setPrixenclair($data['prixenclair']);

        if (array_key_exists('prixenclair', $data))
            $entity->setPrixenclair($data['prixenclair']);

        if (array_key_exists('hidden', $data))
            $entity->setHidden($data['hidden']==1);

        if (array_key_exists('cancelledat', $data))
            $entity->setCancelledAt($data['cancelledat']==1);//

        if (array_key_exists('dateclair', $data))
            $entity->setDateclair($data['dateclair']);

        if (array_key_exists('optdateclair', $data))
            $entity->setOptdateclair($data['optdateclair']);

        if (array_key_exists('entreetype', $data)) {
            $entityEntreeType = $em->getRepository('mdtshomeBundle:EntreeType')->find($data['entreetype']);
            $entity->setEntreetype($entityEntreeType);
        }

        if ($id>0) {
            // $this->get('logger')->info(__FUNCTION__.' - Slug : '.$entity->getSlug() );
            $arrForm['oldslug'] = $entity->getSlug();
        }

        // Form
        if (array_key_exists('api', $data))
            $arrForm['api'] = $data['api'];

        if (array_key_exists('filenameajax', $data))
            $arrForm['filenameajax'] = $data['filenameajax'];

        if (array_key_exists('dragandrop', $data))
            $arrForm['dragandrop'] = $data['dragandrop'];

        if (array_key_exists('eventmultilieu', $data))
            $arrForm['eventmultilieu'] = $data['eventmultilieu'];

        if (array_key_exists('eventlocal', $data))
            $arrForm['eventlocal'] = $data['eventlocal'];


        if (array_key_exists('eventtype', $data))
            $arrForm['eventtype'] = $data['eventtype'];

        if (array_key_exists('artistesdj', $data))
            $arrForm['artistesdj'] = $data['artistesdj'];

        if (array_key_exists('related_event', $data))
            $arrForm['related_event'] = $data['related_event'];

        if (array_key_exists('multiflyer', $data))
            $arrForm['multiflyer'] = $data['multiflyer'];

        if (array_key_exists('oldslug', $data))
            $arrForm['oldslug'] = $data['oldslug'];

        if (array_key_exists('eventbymember', $data))
            $arrForm['eventbymember'] = $data['eventbymember'];

        if (array_key_exists('cancelledat', $data))
            $arrForm['cancelledat'] = $data['cancelledat'];

        if (array_key_exists('videomultiple', $data))
            $arrForm['videomultiple'] = $data['videomultiple'];

        if (array_key_exists('prixmultiple', $data))
            $arrForm['prixmultiple'] = $data['prixmultiple'];

        if (array_key_exists('flyermultiple', $data))
            $arrForm['flyermultiple'] = $data['flyermultiple'];

        if (array_key_exists('articlesmultiple', $data))
            $arrForm['articlesmultiple'] = $data['articlesmultiple'];



        $srv = $this->get('madatsara.service')->createOrUpdateEvent($entity, $arrForm, $id);

        if ($srv) {
            return ['success' => 'Entité sauvegardée correctement.', 'id' => $entity->getId()];
        }

        return ['error' => 'Erreur sauvegarde entité.'];
    }

    /**
     * @QueryParam(name="filename", requirements=".*",  description="queru search")
     * @QueryParam(name="eventid", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher.
     */
    public function getAddtmpflyertoeventAction(ParamFetcher $paramFetcher)
    {
        if ($paramFetcher->get('filename') !='' && $paramFetcher->get('eventid') > 0) {

            $arr = $this->get('madatsara.service')->addFlyerToEventFlyers($paramFetcher->get('eventid'), '', $paramFetcher->get('filename'));

            return array('success' => 'Flyer ajouté correctement.', 'id' => $arr['id'], 'name' => $arr['filename'] );
        }

        return array('error' => 'Erreur ajout flyer.');
    }

    public function postSavenotifAction(Request $request)
    {

        $notifWelcome = false;
        $parametersArr = [];
        $browser = [];

        $ip = $this->get('madatsara.service')->getIpAddress();
        $path_geoipmmdb_exists = $this->get('madatsara.service')->hasParameter('path_geoipmmdb');
        $path_geoipmmdb = $this->get('madatsara.service')->getPathParam('path_geoipmmdb');
        $path_udgerdb_exists = $this->get('madatsara.service')->hasParameter('path_udgerdb');
        $path_udgerdb = $this->get('madatsara.service')->getPathParam('path_udgerdb');

        $welcomeNotification = $this->get('translator')->trans('onesignal.welcomeNotification.title');
        $welcomeNotificationMsg = $this->get('translator')->trans('onesignal.message.action.subscribed');

        $data = array(
            'ip' => $ip
        );

        if ($path_geoipmmdb_exists) {
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($ip);
            $data = array_merge($data,  [
                'geoinfo' => [
                'country_isoCode' => $record->country->isoCode,
                'country_confidence' => $record->country->confidence,
                'country_names' => $record->country->names['fr'],
                'city_confidence' => $record->city->confidence,
                'city_geonameId' => $record->city->geonameId,
                'city_names' => $record->city->names['fr'],
                'postal_confidence' => $record->postal->confidence,
                'postal_code' => $record->postal->code,
                'location_averageIncome' => $record->location->averageIncome,
                'location_accuracyRadius' => $record->location->accuracyRadius,
                'location_latitude' => $record->location->latitude,
                'location_longitude' => $record->location->longitude,
                'location_populationDensity' => $record->location->populationDensity,
                'location_postalCode' => $record->location->postalCode,
                'location_timeZone' => $record->location->timeZone,
            ]]);
        }

        if ($path_udgerdb_exists) {

            $factory = new \Udger\ParserFactory($path_udgerdb);
            $parser = $factory->getParser();
            try {
                $ua = $this->get('madatsara.service')->getHeaders('User-Agent');
                $ip = $this->get('madatsara.service')->getIpAddress();
                $parser->setUA($ua);
                $parser->setIP($ip);
                $browser = $parser->parse();
            } catch (Exception $ex) {

            }

        }

        if (count($browser)>0) {
            $data = array_merge($data,  $browser );
        }

        if ($content = $request->getContent()) {
            $parametersArr = json_decode($content, true);
        }
//        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($parametersArr, 1));

        $notifTitle = array_key_exists('heading', $parametersArr) ? $parametersArr['heading'] : '';
        $notifContent = array_key_exists('content', $parametersArr) ? $parametersArr['content'] : '';
        $notifEvent = array_key_exists('event', $parametersArr) ? $parametersArr['event'] : '';

        if ($welcomeNotification==$notifTitle && $welcomeNotificationMsg==$notifContent)
            $notifWelcome = true;

        if ($notifEvent!='notification.displayed' && $notifWelcome)
            $notifWelcome = false;

        $to = '';
        $from = '';
        $default_email = $this->get('madatsara.service')->getParameter('default_email');
        if ( array_key_exists('to', $default_email)) {
            $to = $default_email['to'];
            $from = $default_email['from'];
        }

        // Les emails sont OK?
        $notifWelcome = filter_var($to,FILTER_VALIDATE_EMAIL) && filter_var($from,FILTER_VALIDATE_EMAIL);

        // Envoyer un email
        if ($notifWelcome) {

            $subject = '[MADATSARA push] - Nouveau device enregistré';

            $msg = $this->renderView(
                'mdtshomeBundle:Email:generique.html.twig',
                array(
                    'content' => $this->get('madatsara.service')->array2table($data),
                )
            );
            $this->get('madatsara.service')->sendMail($to, $subject, $msg, $from);

        }

        return $data;
    }

    /**
     * Derniers evenements ajoutées
     *  
     * 
     * @QueryParam(name="offset", requirements=".*",  description="queru search")
     * @QueryParam(name="limit", requirements=".*",  description="queru search")
     * @QueryParam(name="hidden", requirements=".*",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getEventlastaddedAction(ParamFetcher $paramFetcher)
    {
        $offset = 0;
        $limit = 20;
        $hidden = 0;

        // Limit
        if ($paramFetcher->get('limit') > 0) {
            $limit = $paramFetcher->get('limit');
        }

        // offset
        if ($paramFetcher->get('offset') > 0) {
            $offset = $paramFetcher->get('offset');
        }

        // Doit on afficher aussi les champs caches
        if ($paramFetcher->get('hidden') > 0) {
            $hidden = $paramFetcher->get('hidden');
        }

        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('mdtshomeBundle:Event')->getAllEventsLastadded($hidden);

        return $this->getEventResults($rows, $limit, $offset);
    }

    /**
     * @QueryParam(name="id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getDeletethematiqueAction(ParamFetcher $paramFetcher)
    {
        // Id Passé en parametre
        $id = $paramFetcher->get('id');

        if ($id > 0) {
            $srv = $this->get('madatsara.service')->removeEventType($id);

            return array('success' => 'Item supprimé.');
        }

        return array('error' => 'Erreur suppression Item.');
    }

    /**
     * Enregistrement d'un acces
     * 
     * @RequestParam(name="data", map=true)
     *
     * @param ParamFetcher $paramFetcher
     */
    public function postSaveaccessAction(ParamFetcher $paramFetcher)
    {
        $id = 0;
        $name = '';

        $data = $paramFetcher->all();
        $data = $data['data'];
        $this->get('logger')->info(__FUNCTION__.' - paramFetcher->all : '.print_r($paramFetcher->all(), true));

        $em = $this->getDoctrine()->getManager();

        // ID si besoin
        if (array_key_exists('id', $data) && $data['id']>0) {
            $id = $data['id'];
        }

        // En cas d'Ajout
        if ($id === 0) {
            $entity = new EntreeType();
        }

        // En cas d'edition
        if ($id > 0) {
            $entity = $em->getRepository('mdtshomeBundle:EntreeType')->find($id);
        }
 
        if (array_key_exists('name', $data)) {
            $name = $data['name'];
            $entity->setName($name);
        }

        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->createOrUpdateEntreetype($entity, $id);

        // last insert id
        if ($id === 0) {
            $id = $entity->getId();
        }

        if ($srv) {
            return array('success' => 'Entrée '.$name.' sauvegardée.', 'id' => $id, 'name' => $name);
        }

        return array('error' => 'Erreur sauvegarde Entrée.');
    }

    /**
     * Enregistrement d'un acces
     * 
     * @QueryParam(name="id", requirements="[0-9]+",  description="queru search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getDeleteaccessAction(ParamFetcher $paramFetcher)
    {
        // Id Passé en parametre
        $id = $paramFetcher->get('id');

        if ($id > 0) {
            $srv = $this->get('madatsara.service')->removeEntreetype($id);

            return array('success' => 'Item supprimé.');
        }

        return array('error' => 'Erreur suppression Item.');
    }
    /**
     * Suppression d'un lieu
     * 
     * @QueryParam(name="id", requirements="[0-9]+",  description="query search")
     *
     * @param ParamFetcher $paramFetcher
     */
    public function getDeletelieuAction(ParamFetcher $paramFetcher)
    {
        // Id Passé en parametre
        $id = $paramFetcher->get('id');

        if ($id > 0) {
            $srv = $this->get('madatsara.service')->removeEventLieu($id);

            return array('success' => 'Item supprimé.');
        }

        return array('error' => 'Erreur suppression Item.');
    }
}
