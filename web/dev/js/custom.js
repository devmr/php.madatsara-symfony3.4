;var test = '';
function showPopupWindow(b,e,a){
    var d=(window.screen.width-e)/2,c=(window.screen.height-a)/2;    
    window.open(
            b,
            "SharingWindow",
            "status=1,height="+a+",width="+e+",resizable=0,screenX="+d+",screenY="+c
    );
}
function showGmap(id) {
    //Get Info
    var lat = $(id).find('meta[itemprop=latitude]').attr('content');
    var lng = $(id).find('meta[itemprop=longitude]').attr('content');
    var title = $(id).find('span[itemprop=name]').text();
    var text = $(id).find('span[itemprop=name]').html();
    var streetAddress = $(id).find('span[itemprop=streetAddress]').html();

    if ( typeof streetAddress != 'undefined' ) {
        streetAddress = '<br>'+streetAddress;
    }

    var arrLatLng = [lat, lng];
    var iZoomDefault = 16;

    // Remove "#" between id
    id = id.substr(1);
    var mymap = L.map(id).setView(arrLatLng, iZoomDefault);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: '',
        id: 'mapbox.streets'
    }).addTo(mymap);

    L.marker(arrLatLng)
        .addTo(mymap)
        .bindPopup('<p><strong>'+text+'</strong>'+(typeof streetAddress!='undefined'?streetAddress:'')+'</p>')
        .openPopup();
}

function isDateInferieureA(cette_date, date_compare)
{
    if ( cette_date == '' || cette_date == '__/__/____' ) {
        console.log('Ligne 51');
        return false;
    }
    
    if ( date_compare == '' || date_compare == '__/__/____' ) {
        console.log('Ligne 56');
        return false;
    }
    
    var d1 = moment(cette_date, "DD/MM/YYYY", true);
    var d2 = moment(date_compare, "DD/MM/YYYY", true);
    
    if ( !d1.isValid() ) {
        console.log('Ligne 64');
        return false;
    }
    
    if ( !d2.isValid() ) {
        console.log('Ligne 69');
        return false;
    }
    
    if ( d1.isAfter(date_compare) ) {
        console.log('Ligne 74 '+date_compare);
        return false;
    }
    
    return true;
    
}

function isDateSuperieureA(cette_date, date_compare)
{
    if ( cette_date == '' || cette_date == '__/__/____' ) {
        console.log('Ligne 88');
        return false;
    }
    
    if ( date_compare == '' || date_compare == '__/__/____' ) {
        console.log('Ligne 93');
        return false;
    }
    
    var d1 = moment(cette_date, "DD/MM/YYYY", true);
    var d2 = moment(date_compare, "DD/MM/YYYY", true);
    
    if ( !d1.isValid() ) {
        console.log('Ligne 101');
        return false;
    }
    
    if ( !d2.isValid() ) {
        console.log('Ligne 106');
        return false;
    }
    
    if ( d1.isBefore(date_compare) ) {
        console.log('Ligne 111 '+date_compare);
        return false;
    }
    
    return true;
    
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

$( document ).ready(function() {
    $("img.lazy").lazyload({
        effect : "fadeIn"
    });
    
    $(document).on('click','.dropdown-toggle-share a',function(){
        var url = "";
        var txt = $(this).closest('.caption').find('.truncate a').text()+" via @madatsara";
        var urla = "http://"+location.hostname+$(this).closest('.caption').find('.truncate a').attr('href');
        if ( $(this).hasClass('btn-twitter') ) {
            url = "http://www.twitter.com/share?text="+txt+"&url="+encodeURIComponent( urla );
        }
        if ( $(this).hasClass('btn-facebook') ) {
            url = "http://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent( urla );
        }
        if ( $(this).hasClass('btn-google') ) {
            url = "http://plus.google.com/share?url="+encodeURIComponent( urla );
        }
        
        if ( url!='')
            showPopupWindow(url,550,300);
        return false;
    });

    if ( $('.datetimepicker').length ) {
        $('.datetimepicker').datetimepicker({
                    locale: 'fr',
                    format: 'DD/MM/YYYY'
                });
        $(document).on('blur','.datetimepicker :text', function(){
                    console.log( $(this).attr('id'));
                    
                    valider = false;
                    
                    if ( $(this).val() != '' && $(this).val() != '__/__/____' ) {
                        switch( $(this).attr('id') ) {
                            case 'mdts_homebundle_search_date_from' :
                                    if($('#mdts_homebundle_search_date_to').length>0 && $('#mdts_homebundle_search_date_to').val() !='' && $('#mdts_homebundle_search_date_to').val()!='__/__/____' ) {
                                        console.log( 'L 182: #'+$('#mdts_homebundle_search_date_to').val()+'#' );
                                        valider = isDateInferieureA( $(this).val(), $('#mdts_homebundle_search_date_to').val() );
                                    } else
                                        valider = true;
                                break;
                            case 'mdts_homebundle_search_date_to' :
                                    if( $('#mdts_homebundle_search_date_from').val() !='' && $('#mdts_homebundle_search_date_from').val()!='__/__/____' )
                                        valider = isDateSuperieureA( $(this).val(), $('#mdts_homebundle_search_date_from').val() );
                                    else
                                        valider = true;
                                break;
                        }
                    }
                    console.log( 'valider: '+valider );
                    if( valider ) {
                        $(this).closest('form').trigger('submit');
                    }
                    
                });
         
    }
    $('[data-toggle="tooltip"]').tooltip();
    
    if ( $('form[name=mdts_homebundle_contact]').length && !$('.alert-dismissible').length ) {
        var tabfield = new Array();
        $('form[name=mdts_homebundle_contact] :text, form[name=mdts_homebundle_contact] input[type=email], form[name=mdts_homebundle_contact] textarea').each(function(){
            tabfield.push( $(this).prop('id') );
            
            if ( $(this).prop('id') != 'mdts_homebundle_contact_captcha' && sessionStorage.getItem($(this).prop('id'))!=null ) {
                $(this).val( sessionStorage.getItem($(this).prop('id')) );
            }
            
        });
        
        if ( $('#mdts_homebundle_contact_subscribe').length && sessionStorage.getItem('mdts_homebundle_contact_subscribe') == 'true' ) {
            $('#mdts_homebundle_contact_subscribe').attr('checked', 'checked');
        }
        
        //console.log( 'tabfield '+tabfield );
    }
    
    if ( $('.alert-dismissible').length ) {
        sessionStorage.clear();
    }
     
    $(document).on('click','img[data-href]',function(){
           bootbox.dialog({
              title: "",
              message: '<img class="center-block img-responsive"  src="'+$(this).data('href')+'" />',
              onEscape: true
            });
           return false;
    });
    
    $(document).on('blur','form[name=mdts_homebundle_contact] :text, form[name=mdts_homebundle_contact] input[type=email], form[name=mdts_homebundle_contact] textarea',function(){
           console.log( 'formval: '+$(this).val() );
           console.log( 'sessionval: '+sessionStorage.getItem($(this).prop('id')) );
           sessionStorage.setItem($(this).prop('id'),$(this).val());
    });
    
    $(document).on('click','#mdts_homebundle_contact_subscribe',function(){
        sessionStorage.setItem($(this).prop('id'),$(this).attr('checked'));
    });

	//Googlmap
    if( $('#googlemap').length   ){
        showGmap('#googlemap');
    }

    if( $('.googlemap').length   ){
        $('.googlemap').each( function(){
                //alert( $(this).prop('id') );
                showGmap( '#'+$(this).prop('id') );
        });
    }
    
    console.log('typeof lory: '+typeof lory );
    if ('function' === typeof lory && document.querySelectorAll('.js_slider').length > 0) {
        lory(document.querySelector('.js_slider'), {
           rewind: true
        });
    }
    
    if ( document.querySelectorAll('.container-more').length>0) {
         
        [].forEach.call(document.querySelectorAll('.btnmore'), function(el) {
          el.addEventListener('click', function(evt) {
                
                var containerMore = el.closest('.container-more');
                containerMore.querySelector('.blocmore').classList.remove('hidden');
                el.classList.add('hidden');
                document.querySelector('.btnminus').classList.remove('hidden');
                evt.preventDefault();
          });
        });
        
        [].forEach.call(document.querySelectorAll('.btnminus'), function(el) {
          el.addEventListener('click', function(evt) {
                
                var containerMore = el.closest('.container-more');
                containerMore.querySelector('.blocmore').classList.add('hidden');
                el.classList.add('hidden');
                document.querySelector('.btnmore').classList.remove('hidden');
                
                evt.preventDefault();
          });
        });
        
        
    }
    
});


(function() {
    console.log( typeof detail_article );
    function addEvent(obj, evt, fn) {
        if (obj.addEventListener) {
            obj.addEventListener(evt, fn, false);
        }
        else if (obj.attachEvent) {
            obj.attachEvent("on" + evt, fn);
        }
    }



    function animerdiv()
    {
        //$("#divtoBlink").removeClass('hidden');
        document.getElementById("divtoBlink").classList.remove('hidden');
        var startTime = new Date().getTime();
        var myVar = setInterval(function(){
            //$(".containermodal_inner_content").toggleClass("backgroundRed");
            document.querySelector(".containermodal_inner_content").classList.toggle('backgroundRed');

            //Stoper apres 2 sec
            if(new Date().getTime() - startTime > 1000){
                //$(".containermodal_inner_content").addClass('reset');
                document.querySelector(".containermodal_inner_content").classList.add('reset');
                clearInterval(myVar);

                //Donner le focus
                document.getElementById('mdts_homebundle_newsletterabonne_email').focus();
                return;
            }

        },100);
    }


    addEvent(document, "keyup", function (e) {
        e = e || window.event;
        // use e.keyCode
        if(document.querySelectorAll('.containermodal_inner_content').length > 0 && 27 ==  e.keyCode ) {
            //Escape
            var isClickInside = document.querySelector('.containermodal_inner_content').contains(event.target);
            if (!isClickInside && !document.getElementById('divtoBlink').classList.contains("hidden")) {
                console.log('You clicked outside');
                //$("#divtoBlink").addClass('hidden');
                createCookie('mts_box_newsletter','1' );
                document.getElementById("divtoBlink").classList.add('hidden');
            }
        }
    });

    addEvent( document , 'click', function(e) {
        if ( document.querySelectorAll('.containermodal_inner_content').length > 0 ) {
        var isClickInside = document.querySelector('.containermodal_inner_content').contains(event.target);
        if (!isClickInside && !document.getElementById('divtoBlink').classList.contains("hidden")) {
            console.log('You clicked outside');
            //$("#divtoBlink").addClass('hidden');
            createCookie('mts_box_newsletter','1' );
            document.getElementById("divtoBlink").classList.add('hidden');
        }
        }

    } );



    addEvent(document, "mouseout", function(e) {
        e = e ? e : window.event;
        var from = e.relatedTarget || e.toElement;
        if (!from || from.nodeName == "HTML") {
            // stop your drag event here
            // for now we can just use an alert
            console.log("left window");

            if ( null == readCookie('mts_box_newsletter'))
                animerdiv();
            else
                console.log('Cookie pr&eacute;sent - ne rien faire : '+readCookie('div_anim') );
        } else {
            console.log('On window');
        }

    });

    addEvent(document,"DOMContentLoaded",function(e) {
        if ( document.querySelectorAll('.bigclose').length > 0 ) {
        addEvent( document.querySelector('.bigclose'), 'click', function(e) {
            //$("#divtoBlink").addClass('hidden');
            document.getElementById("divtoBlink").classList.add('hidden');
            createCookie('mts_box_newsletter','1',365);
        });
        }
    });

    //Afficher le div au bout de 10 sec d'inactivité
    if ( document.querySelectorAll('.bigclose').length > 0 ) {
    setTimeout(function(){
        if ( null == readCookie('mts_box_newsletter'))
            animerdiv();
    }, 3000 );

    addEvent( document.getElementById('mdts_homebundle_newsletterabonne_save') , 'click', function(e) {
        if ( null == readCookie('mts_box_newsletter'))
            createCookie('mts_box_newsletter','1',365);
    });
    }




})();

/**
 * Implement infinite scrolling
 * - Inspired by: http://ravikiranj.net/drupal/201106/code/javascript/how-implement-infinite-scrolling-using-native-javascript-and-yui3
 */

(function() {
    var isIE = /msie/gi.test(navigator.userAgent); // http://pipwerks.com/2011/05/18/sniffing-internet-explorer-via-javascript/

    this.infiniteScroll = function(options) {
        var defaults = {
            callback: function() {},
            distance: 50
        }
        // Populate defaults
        for (var key in defaults) {
            if(typeof options[key] == 'undefined') options[key] = defaults[key];
        }

        var scroller = {
            options: options,
            updateInitiated: false
        }

        window.onscroll = function(event) {
            handleScroll(scroller, event);
        }
        // For touch devices, try to detect scrolling by touching
        document.ontouchmove = function(event) {
            handleScroll(scroller, event);
        }
    }

    function getScrollPos() {
        // Handle scroll position in case of IE differently
        if (isIE) {
            return document.documentElement.scrollTop;
        } else {
            return window.pageYOffset;
        }
    }

    var prevScrollPos = getScrollPos();

    // Respond to scroll events
    function handleScroll(scroller, event) {
        if (scroller.updateInitiated) {
            return;
        }
        var scrollPos = getScrollPos();
        if (scrollPos == prevScrollPos) {
            return; // nothing to do
        }

        // Find the pageHeight and clientHeight(the no. of pixels to scroll to make the scrollbar reach max pos)
        var pageHeight = document.documentElement.scrollHeight;
        var clientHeight = document.documentElement.clientHeight;

        // Check if scroll bar position is just 50px above the max, if yes, initiate an update
        if (pageHeight - (scrollPos + clientHeight) < scroller.options.distance) {
            //scroller.updateInitiated = true;

            scroller.options.callback(function() {
                scroller.updateInitiated = false;
            });
        }

        prevScrollPos = scrollPos;
    }
}());

// setup infinite scroll
var ajax = false;
infiniteScroll({
    distance: 700,
    callback: function(done) {
        // 1. fetch data from the server
        // 2. insert it into the document
        // 3. call done when we are done
        //console.log('infiniteScroll here');
        //Pagination exists ?
        //console.log('Pagination length : '+document.querySelectorAll('ul.pagination').length );
        //console.log('ajax : '+ajax);
        if (!ajax &&  0 < document.querySelectorAll('ul.pagination').length ) {
            [].map.call(document.querySelectorAll('ul.pagination'), function(el) {
                el.classList.add('hidden');
            });
            if ( !document.getElementById('loading_paginate') ) {
                var loading_paginate = document.createElement('div');
                loading_paginate.id = 'loading_paginate';
                loading_paginate.className = 'center-block';

                //Ajouter 2 blocs
                var clearfix_div = document.createElement('div');
                clearfix_div.className = 'clearfix center-block';
                clearfix_div.setAttribute('style','width:25%;');

                var pull_left1 = document.createElement('div');
                pull_left1.className = 'pull-left';

                //Spinner
                var spinner_div = document.createElement('div');
                spinner_div.className = 'spinner';
                //3 div dans spinner
                var bounce1 = document.createElement('div');
                bounce1.className = 'bounce1';
                var bounce2 = document.createElement('div');
                bounce2.className = 'bounce2';
                var bounce3 = document.createElement('div');
                bounce3.className = 'bounce3';

                spinner_div.appendChild(bounce1);
                spinner_div.appendChild(bounce2);
                spinner_div.appendChild(bounce3);

                pull_left1.appendChild(spinner_div);

                var pull_left2 = document.createElement('div');
                pull_left2.className = 'pull-left';
                pull_left2.setAttribute('style','font-size:1.3em;');
                pull_left2.textContent = 'Patientez...';

                clearfix_div.appendChild(pull_left1);
                clearfix_div.appendChild(pull_left2);

                loading_paginate.appendChild(clearfix_div);

                //document.querySelector('div.pagination').closest('div').appendChild(loading_paginate);
                document.getElementById('loader_paginate').appendChild(loading_paginate);
            }
            //Sur quelle page on est ?
            var active_page_dom = document.querySelector('ul.pagination').querySelector('.active');
            //console.log('active page : #'+ active_page_dom.textContent.trim()+'#' );
            var current_page = eval( active_page_dom.textContent.trim() );
            var next_page = current_page+1;
            //console.log('typeof next page : #'+typeof next_page+'#' );
            //console.log('next page : #'+next_page+'#' );
            var tabpagination = [];
            //Parcourir l'ensemble des li
            [].forEach.call( document.querySelector('ul.pagination').querySelectorAll('li') , function(el) {
                //console.log( el.innerHTML );
                tabpagination.push( el.textContent.trim() );
            });

            console.log( tabpagination );

            var index_en_cours;
            var i = 0;
            for(var i = 0; i < tabpagination.length; i++ ) {
                if( tabpagination[i] == active_page_dom.textContent.trim() )
                    index_en_cours = i;

            }

            //console.log( 'index_en_cours: ' +index_en_cours +' - nextindex value : '+ eval(index_en_cours+1) +' - length : '+tabpagination.length+' - LAST : '+( eval(index_en_cours+2)>=tabpagination.length?' yes ' : '' ) );
            console.log( 'index_en_cours: ' +index_en_cours +' - nextindex value : '+ eval(index_en_cours+1) +' - length : '+tabpagination.length+' - LAST : '+( eval(index_en_cours+2)>=tabpagination.length?' yes ' : '' ) );
            //Si on n'est pas sur la page de fin
            if (eval(index_en_cours+2)<tabpagination.length ) {

                var urlnext = document.querySelector('ul.pagination').querySelectorAll('li')[eval(index_en_cours+1)].querySelector('a').getAttribute('href');
                console.log('urlnext : '+urlnext);
                ajax = true;

                var r = new XMLHttpRequest();
                r.open("GET", urlnext, true);
                r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                r.onreadystatechange = function () {
                    console.log('readyState&status : '+r.readyState+' - '+r.status);
                    if (r.readyState != 4 || r.status != 200) return;

                    var parser = new DOMParser();
                    var xmlDoc = parser.parseFromString(r.responseText,"text/html");
                    //var row = xmlDoc.querySelector('.data-liste') ;
                    var row = xmlDoc.querySelector('#contentAjax').innerHTML ;
                   //if ( xmlDoc.querySelector('.data-liste').querySelector('.row') )
                    //    row = xmlDoc.querySelector('.data-liste').querySelector('.row');

                    var row_pagination = xmlDoc.querySelector('div.pagination') ;


                    //Ajouter ce bloc à la fin de row
                    //if ( document.querySelector('.data-liste').querySelector('.row') )
                    //    document.querySelector('.data-liste').querySelector('.row').appendChild(row);
                    //else
                        document.querySelector('.data-liste').innerHTML += row;
                    document.querySelector('div.pagination').innerHTML = row_pagination.innerHTML;

                    $("img.lazy").lazyload({
                        effect : "fadeIn"
                    });

                    ajax = false;

                };
                r.send();

            } else {
                //On est sur la fin
                document.getElementById('loading_paginate').classList.add('hidden');
            }
            done();

        }

    }
});
