<?php
//
error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');

Header('Content-Type: text/html; charset=utf-8');
$dns = 'mysql:host=localhost;charset=utf8;dbname=mts_w4';
$user = 'madatsara';
$mdp = '!4xntmj60!';
try {
    $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}
function getAllPlacesIDOfEvent($eventId)
{
    global $connection;

    $res = $connection->prepare("SELECT event_lieu_id FROM `event_event_lieu` as foreignid WHERE `event_id` = :event_id");
    $res->bindParam(':event_id', $eventId);
    $res->execute();
    $rows = $res->fetchAll(PDO::FETCH_ASSOC);

    $row = [];
    foreach ($rows as $row_) {
        array_push($row, $row_['foreignid']);
    }
    return $row;
}
function getAllArtistsIDOfEvent($eventId)
{
    global $connection;

    $res = $connection->prepare("SELECT event_artistes_dj_organisateurs_id as foreignid FROM `event_event_artistes_dj_organisateurs` WHERE `event_id` = :event_id");
    $res->bindParam(':event_id', $eventId);
    $res->execute();
    $rows = $res->fetchAll(PDO::FETCH_ASSOC);

    $row = [];
    foreach ($rows as $row_) {
        array_push($row, $row_['foreignid']);
    }
    return $row;
}
function addAbonneLieux($lieuId, $abonneId)
{
    global $connection;

    $log = [];

    $lieuId = filter_var($lieuId,FILTER_VALIDATE_INT);
    $abonneId = filter_var($abonneId,FILTER_VALIDATE_INT);

    if (!$lieuId || !$abonneId) {
        $log[] = 'lieuId: '.$lieuId.' ou abonneId: '.$abonneId.' non entier' ;
        return implode("\n", $log );
    }


    if (rowExists($abonneId,$lieuId,'abonne_by_eventlieu')) {
        $log[] = 'Relation '.$abonneId.' - '.$lieuId.' existe dans abonne_by_eventlieu' ;
        $log[] = attachPaysRegionLocalityQuartierToAbonne($lieuId, $abonneId);
        return implode("\n", $log );
    }

    $res = $connection->prepare("INSERT INTO `abonne_by_eventlieu` SET `abonne_id` = :abonne_id, `foreign_id` = :foreign_id");
    $res->bindParam(':abonne_id', $abonneId);
    $res->bindParam(':foreign_id', $lieuId);
    $res->execute();

    $log[] = 'OK Ajout de '.$abonneId.' et '.$lieuId.' dans abonne_by_eventlieu' ;

    $log[] = attachPaysRegionLocalityQuartierToAbonne($lieuId, $abonneId);

    return implode("\n", $log );
}
function addAbonneQuartier($rowLieu, $abonneId)
{
    $log = [];
    $log[] = addAbonneForeign($rowLieu['quartier_id'],$abonneId,  'abonne_by_quartier');
    return implode("\n", $log );
}
function addAbonneRegion($rowLieu, $abonneId)
{
    $log = [];
    $log[] = addAbonneForeign($rowLieu['region_id'],$abonneId ,'abonne_by_region');
    return implode("\n", $log );
}
function addAbonnePays($rowLieu, $abonneId)
{
    $log = [];
    $log[] = addAbonneForeign($rowLieu['country_id'], $abonneId,  'abonne_by_countries');
    return implode("\n", $log );
}
function addAbonneLocality($rowLieu, $abonneId)
{
    $log = [];
    $log[] = addAbonneForeign($rowLieu['locality_id'],$abonneId,  'abonne_by_locality');
    return implode("\n", $log );
}
function addAbonneForeign($foreign_id, $localId, $tableName)
{
    $log = [];
    global $connection;

    $localId = filter_var($localId,FILTER_VALIDATE_INT);
    $foreign_id = filter_var($foreign_id,FILTER_VALIDATE_INT);

    if (!$localId || !$foreign_id) {
        $log[] = 'localId: '.$localId.' ou foreign_id: '.$foreign_id.' non entier' ;
        return implode("\n", $log );
    }


    if (rowExists($localId,$foreign_id,$tableName)) {
        $log[] = 'Relation '.$localId.' - '.$foreign_id.' existe dans '.$tableName;
        return implode("\n", $log );
    }

    $sql = "INSERT INTO `".$tableName."` SET `abonne_id` = :abonne_id, `foreign_id` = :foreign_id";
    $res = $connection->prepare( $sql );
    $res->bindParam(':abonne_id', $localId);
    $res->bindParam(':foreign_id', $foreign_id);
    $res->execute();

    $log[] = 'OK Ajout de '.$localId.' et '.$foreign_id.' dans '.$tableName;
//    $log[] = 'SQL '.$sql;

    return implode("\n", $log );
}
function rowExists($abonneId,$artisteId,$table)
{
    global $connection;

    $sql = "SELECT COUNT(*) nb FROM `$table` WHERE `abonne_id` = :abonne_id AND `foreign_id` = :foreign_id";

    $res = $connection->prepare($sql);
    $res->bindParam(':abonne_id', $abonneId);
    $res->bindParam(':foreign_id', $artisteId);
    $res->execute();

    $row = $res->fetch(PDO::FETCH_ASSOC);
    return $row['nb']>0;

}
function addAbonneArtistes($artisteId, $abonneId)
{
    $log = [];
    global $connection;

    $sql = "INSERT INTO `abonne_by_artistes` SET `abonne_id` = :abonne_id, `foreign_id` = :foreign_id";

    if (rowExists($abonneId,$artisteId,'abonne_by_artistes')) {
        $log[] = 'Relation '.$abonneId.' - '.$artisteId.' existe dans abonne_by_artistes';
        return implode("\n", $log );
    }

    $res = $connection->prepare($sql);
    $res->bindParam(':abonne_id', $abonneId);
    $res->bindParam(':foreign_id', $artisteId);
    $res->execute();

    $log[] = 'OK Ajout de '.$abonneId.' et '.$artisteId.' dans abonne_by_artistes';

    return implode("\n", $log);
}
function attachPaysRegionLocalityQuartierToAbonne($lieuId, $abonneId)
{
    $log = [];

    global $connection;

    $res = $connection->prepare("SELECT country_id, region_id, locality_id, quartier_id FROM `event_lieu` WHERE `id` = :id");
    $res->bindParam(':id', $lieuId);
    $res->execute();
    $rowLieu = $res->fetch(PDO::FETCH_ASSOC);

    // Un pays
    $log[] = addAbonnePays($rowLieu, $abonneId);

    // Une commune
    $log[] = addAbonneLocality($rowLieu, $abonneId);

    // Une region
    $log[] = addAbonneRegion($rowLieu, $abonneId);

    // Un quartier
    $log[] = addAbonneQuartier($rowLieu, $abonneId);

    return implode("\n", $log);
}
function attachLieuxToAbonneNewsletter($arrArtists,$abonneId)
{
    $log = [];

    if (count($arrArtists)<=0)
        return false;

    // Pour chaque artiste
    foreach ($arrArtists as $lieuId) {

        $lieuId = filter_var($lieuId, FILTER_VALIDATE_INT);
        if ($lieuId>0) {
            $log[] = addAbonneLieux($lieuId,$abonneId);
        }
    }

    return implode("\n", $log);
}
function attachArtistsToAbonneNewsletter($arrArtists,$abonneId)
{
    $log = [];

    if (count($arrArtists)<=0)
        return false;

    // Pour chaque artiste
    foreach ($arrArtists as $artisteId) {

        $artisteId = filter_var($artisteId, FILTER_VALIDATE_INT);
        if ($artisteId>0) {
            $log[] = addAbonneArtistes($artisteId,$abonneId);
        }
    }

    return implode("\n", $log);
}
function getEventTypeOfReferer($referer)
{
    $referer = getCleanedReferer($referer);

    if ($referer=='')
        return false;

    // Url du type "evenement_[slug}"
    $prefixe = substr($referer,0,10);
    $suffixe = substr($referer,10   );

    // $this->logger->info(__LINE__.'---'.$prefixe );
    // $this->logger->info(__LINE__.'---'.$suffixe );

    if ($prefixe!='evenement_')
        return false;

    // Chercher l'identifiant relatif au suffixe URL dans entity event
    $eventEntity = findOneByOrigSlug($suffixe);
    if (!$eventEntity)
        return false;

    return $eventEntity;

}

function findOneByOrigSlug($str)
{
    global $connection;

    $res = $connection->prepare("SELECT * FROM `event` WHERE `orig_slug` = :orig_slug LIMIT 1");
    $res->bindParam(':orig_slug', $str);
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);
    return $row;
}
function attachEmailAbonneByReferer($abonneId, $referer)
{
    $log = [];

    $eventEntity = getEventTypeOfReferer($referer);
    if (!$eventEntity)
        return 'Erreur referer '.$referer;

    $eventId = $eventEntity['id'];
    $eventlieuId = $eventEntity['eventlieu_id'];

    $log[]  = 'Eventid : '.$eventId;
        // $this->logger->info(__LINE__.'---'.$eventId);

    // Artistes de l'event
    $arrArtists = getAllArtistsIDOfEvent($eventId);
    $log[]  = count($arrArtists).' artiste(s)';
    $log[]  = attachArtistsToAbonneNewsletter($arrArtists,$abonneId);

    // Lieux de l'event
    $log[]  = addAbonneLieux($eventlieuId,$abonneId);

    $arrLieux = getAllPlacesIDOfEvent($eventId);
    $log[]  = count($arrLieux).' multi-lieux';
    $log[]  = attachLieuxToAbonneNewsletter($arrLieux,$abonneId);

    return implode("\n", $log);

}
function getCleanedReferer($referer)
{
    // Supprimer URL
    $host = ['http://www.madatsara.com/', 'http://madatsara.com/', 'http://m.madatsara.com/','https://www.madatsara.com/', 'https://madatsara.com/', 'https://m.madatsara.com/'];

    $referer = str_replace($host, '', $referer);

    // Supprimer .html
    $referer = str_replace('.html', '', $referer);

    $referer = trim($referer);

    return $referer;
}


$period = array();

$res = $connection->prepare("SELECT COUNT(id) as nb FROM newsletter_abonne");
$res->execute();
$row = $res->fetch(PDO::FETCH_ASSOC);
$nb = $row['nb'];
echo $nb." lignes \n\n";

$res = $connection->prepare("SELECT id, email, infos FROM newsletter_abonne");
$res->execute();
$rows = $res->fetchAll(PDO::FETCH_ASSOC);
 
$i = 1;
foreach( $rows as $row ) {
    $id = $row['id'];
    $email = $row['email'];
    $infos = $row['infos'];
    $infos = json_decode($infos,true);
    $referer = $infos['referer'];

    echo 'Ligne '.$i."\n".'AbonneID: '.$id ."\n";

    // Decoder infos
    $retour = attachEmailAbonneByReferer($id, $referer);
    echo $retour."\n";

    
     echo "--------------\n";

     $i++;
     unset($infos);

}