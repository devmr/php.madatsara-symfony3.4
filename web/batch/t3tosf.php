<?php
function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
} 
//$period = date_range('2016-01-01','2016-01-10','+1 day','Y-m-d');
//print_r(($period));exit();

error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');
/*use Cocur\Slugify\Slugify;
require 'vendor/autoload.php';
$slugify = new Slugify();*/
/*echo $slugify->slugify('Sorties / Musique')."\n";
echo $slugify->slugify('Arts / Culture')."\n";
echo $slugify->slugify('Festival')."\n";
echo $slugify->slugify('Sports / Danse')."\n";
echo $slugify->slugify('Conférence')."\n";
echo $slugify->slugify('Expositions / Salons')."\n";
exit();*/
/**************************/


Header('Content-Type: text/html; charset=utf-8');
$dns = 'mysql:host=localhost;charset=utf8';
$user = 'madatsara';
$mdp = '!4xntmj60!';
try {
    $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}

    
 
  
/**
 * Convert a comma separated file into an associated array.
 * The first row should contain the array keys.
 * 
 * Example:
 * 
 * @param string $filename Path to the CSV file
 * @param string $delimiter The separator used in the file
 * @return array
 * @link http://gist.github.com/385876
 * @author Jay Williams <http://myd3.com/>
 * @copyright Copyright (c) 2010, Jay Williams
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */
function csv_to_array($filename='', $delimiter=',')
{
	if(!file_exists($filename) || !is_readable($filename))
		return FALSE;
	
	$header = NULL;
	$data = array();
	if (($handle = fopen($filename, 'r')) !== FALSE)
	{
		while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
		{
			if(!$header)
				$header = $row;
			else
				$data[] = array_combine($header, $row);
		}
		fclose($handle);
	}
	return $data;
}
function convertToPositiveInteger($theInt) {
        $theInt = intval($theInt);
        if ($theInt < 0) {
                $theInt = 0;
        }
        return $theInt;
}

function writefile($file,$content,$mode='wb')      {
    if (!@is_file($file))   $changePermissions = true;
    if ($fd = fopen($file,$mode))    {
        $res = fwrite($fd,$content);
        fclose($fd);
    if ($res===false)       return false;

    return true;
    }
    return false;
}
function get_event_date_id($date){
    global $connection;
    if( $date != '' ) {
        $date = trim($date);

        if ( event_date_id_exists($date) ){
           
            $res = $connection->prepare("SELECT id FROM `mts_w4`.`event_date` WHERE `date` = :dates LIMIT 1");
            $res->bindParam(':dates', $date, PDO::PARAM_STR );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return $row['id'];  
        }  else {

            $insertsql = $connection->prepare( "INSERT INTO `mts_w4`.`event_date` SET `date` = :dates" );
            $insertsql->bindParam(':dates', $date, PDO::PARAM_STR ); 
            
            try {
                $insertsql->execute();
            } catch ( Exception $e ) {
              echo "Requete  : ", $insertsql->errorCode();
            }
            return $connection->lastInsertId();
        }   
    }  
    return 0;
}

function addOrFailEventByDate( $eventid, $eventdateid)
{
    global $connection;
    $sql = "";
    $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mts_w4`.`event_by_date` WHERE `event_id` = :event_id AND `event_date_id` = :event_date_id");
    $res->bindParam(':event_date_id', $eventdateid, PDO::PARAM_INT );
    $res->bindParam(':event_id', $eventid, PDO::PARAM_INT );
    $res->execute();
     
    $row = $res->fetch(PDO::FETCH_ASSOC);
    $nb = $row['nb'];

    if ( $nb <= 0 ){
        $sql = "INSERT INTO `mts_w4`.`event_by_date` SET `event_id` = :event_id, `event_date_id` = :event_date_id";
        $insertsql = $connection->prepare( $sql );
        $insertsql->bindParam(':event_date_id', $eventdateid, PDO::PARAM_INT );
        $insertsql->bindParam(':event_id', $eventid, PDO::PARAM_INT );
        try {
            $insertsql->execute();
        } catch ( Exception $e ) {
          echo "Requete  : ", $insertsql->errorCode();
        }
    }

    return $sql;
}
function splitUpperSpace($str)
{
    $str = trim( $str );
    for( $i=0; $i<strlen($str); $i++) {

        if ( ctype_upper( $str[$i] ) && ctype_lower($str[$i+1]) && !preg_match('/\s/', $str[$i-1] )  ) {
            $str[$i-1] = ' ';
        }

    }
}
function get_region_id($libelle ){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        
            $res = $connection->prepare("SELECT id FROM `mg_region` WHERE `libelle` LIKE :libelle LIMIT 1");
            $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return $row['id'];
         
    }
    return 0;
}
function get_district_id($libelle, $region_id){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        if ( district_id_exists($libelle) ) {

            $res = $connection->prepare("SELECT id FROM `mg_district` WHERE `libelle` LIKE :libelle LIMIT 1");
            $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return $row['id'];
        } else {
            $insertsql = $connection->prepare( "INSERT INTO `mg_district` SET `libelle` = :libelle,  `regionid`= :regionid" );
            $insertsql->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            $insertsql->bindParam(':regionid', $region_id, PDO::PARAM_INT );
             
            
            try {
                $insertsql->execute();
            } catch ( Exception $e ) {
              echo "Requete  : ", $insertsql->errorCode();
            }
            return $connection->lastInsertId();
        }
    }
    return 0;
}

function get_parti_id($libelle, $NOMPARTINDEPENDANT ){
    global $connection;
    if( $libelle != '' && mb_strtolower( $libelle ) == 'independant' && $NOMPARTINDEPENDANT!=''){
        $libelle = trim($NOMPARTINDEPENDANT);
    } 

    if( $libelle != '' ){
        $libelle = trim($libelle);
        if( parti_id_exists($libelle) ){
            $res = $connection->prepare("SELECT id FROM `mg_parti` WHERE `name` LIKE :libelle LIMIT 1");
            $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return $row['id'];
        }
        else{
             
            $insertsql = $connection->prepare( "INSERT INTO `mg_parti` SET `name` = :libelle" );
            $insertsql->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            
            try {
                $insertsql->execute();
            } catch ( Exception $e ) {
              echo "Requete  : ", $insertsql->errorCode();
            }
            return $connection->lastInsertId();
        }
    }
    
    return 0;
}

function add_or_update_candidat($libelle, $parti_id=0, $region_id=0, $district_id=0, $commune_id=0, $slugifyname='',$independant=0){
    global $connection;
    $candidat_id = 0;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        if( candidat_id_exists($libelle) ){
            $res = $connection->prepare("SELECT `id` FROM `mg_candidat_communales` WHERE `name` LIKE :libelle AND `parti_id` = :parti_id AND `region_id` = :region_id AND `district_id` = :district_id AND `commune_id` = :commune_id");
            $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
            $res->bindParam(':parti_id', $parti_id, PDO::PARAM_INT );
            $res->bindParam(':region_id', $region_id, PDO::PARAM_INT );
            $res->bindParam(':district_id', $district_id, PDO::PARAM_INT );
            $res->bindParam(':commune_id', $commune_id, PDO::PARAM_INT );            
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            $candidat_id = $row['id'];
            
            $sql = "UPDATE `mg_candidat_communales` ";
        }
        else{
            $sql = "INSERT INTO `mg_candidat_communales` ";
        }
        
        $sql .= "SET `name` = :libelle ";
        $sql .= ", `slugifyname` =  :slugifyname" ;
        
        if( $parti_id >  0 ) {
            $sql .= ", `parti_id` = :parti_id " ;
        }
        if( $commune_id >  0 ) {
            $sql .= ", `commune_id` = :commune_id " ;
        }
        if( $region_id >  0 ) {
            $sql .= ", `region_id` = :region_id " ;
        }
        if( $district_id >  0 ) {
            $sql .= ", `district_id` = :district_id " ;
        }
        if( $independant >  0 ) {
            $sql .= ", `independant` = 1 " ;
        }
        if( candidat_id_exists($libelle) ){
            $sql .= " WHERE `id` = :id ";
        }
        
        $insertsql = $connection->prepare( $sql );
        $insertsql->bindParam(':libelle', $libelle, PDO::PARAM_STR );
        $insertsql->bindParam(':slugifyname', $slugifyname, PDO::PARAM_STR );
        
        if( candidat_id_exists($libelle) && $candidat_id > 0 ){
            $insertsql->bindParam(':id', $candidat_id, PDO::PARAM_INT );
        }
        if( $parti_id >  0 ) {
            $insertsql->bindParam(':parti_id', $parti_id, PDO::PARAM_INT );
        }
        if( $commune_id >  0 ) {
            $insertsql->bindParam(':commune_id', $commune_id, PDO::PARAM_INT );
        }
        if( $region_id >  0 ) {
            $insertsql->bindParam(':region_id', $region_id, PDO::PARAM_INT );
        }
        if( $district_id >  0 ) {
            $insertsql->bindParam(':district_id', $district_id, PDO::PARAM_INT );
        }
        
        try {
            $insertsql->execute();
            //print_r( $insertsql->errorInfo() );
        } catch ( Exception $e ) {
          echo "Requete  : ", $insertsql->errorCode();
        }
    }
    return 0;
}

function candidat_id_exists($libelle){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mg_candidat_communales` WHERE `name` LIKE :libelle");
        $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
        $res->execute();
         
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];
         
         return ($nb>0?true:false);
         
    }
    return false;
}
function parti_id_exists($libelle){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mg_parti` WHERE `name` LIKE :libelle");
        $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
        $res->execute();
         
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];
         
         return ($nb>0?true:false);
         
    }
    return false;
}
function event_date_id_exists($date){
    global $connection;
    if( $date != '' ){
        $date = trim($date);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mts_w4`.`event_date` WHERE `date` = :dates");
        $res->bindParam(':dates', $date, PDO::PARAM_STR );
        $res->execute();
         
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];
         
         return ($nb>0?true:false);
         
    }
    return false;
}
function region_id_exists($libelle){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mg_region` WHERE `libelle` LIKE :libelle");
        $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
        $res->execute();
         
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];
         
         return ($nb>0?true:false);
         
    }
    return false;
}
function district_id_exists($libelle){
    global $connection;
    if( $libelle != '' ){
        $libelle = trim($libelle);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `mg_district` WHERE `libelle` LIKE :libelle");
        $res->bindParam(':libelle', $libelle, PDO::PARAM_STR );
        $res->execute();
         
        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];
         
         return ($nb>0?true:false);
         
    }
    return false;
}
function normalizeChars($s) {
    $replace = array(
        'À'=>'A', '??'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'Ae', 'Å'=>'A', 'Æ'=>'A', 'Ă'=>'A',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'ae', 'å'=>'a', 'ă'=>'a', 'æ'=>'ae',
        'þ'=>'b', 'Þ'=>'B',
        'Ç'=>'C', 'ç'=>'c',
        'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E',
        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 
        'Ğ'=>'G', 'ğ'=>'g',
        'Ì'=>'I', '??'=>'I', 'Î'=>'I', '??'=>'I', 'İ'=>'I', 'ı'=>'i', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i',
        'Ñ'=>'N',
        'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe', 'Ø'=>'O', 'ö'=>'oe', 'ø'=>'o',
        'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
        'Š'=>'S', 'š'=>'s', 'Ş'=>'S', 'ș'=>'s', 'Ș'=>'S', 'ş'=>'s', 'ß'=>'ss',
        'ț'=>'t', 'Ț'=>'T',
        'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'Ue',
        'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'ue', 
        '??'=>'Y',
        'ý'=>'y', 'ý'=>'y', 'ÿ'=>'y',
        'Ž'=>'Z', 'ž'=>'z'
    );
    $s = strtr($s, $replace);
    $s = mb_strtolower($s);
    $s = preg_replace('/[^a-z]/','',$s);
    return $s;
}
function getEventID($t3id){
    global $connection;
    //echo "getEventID: ".$t3id."\n";
    if( $t3id > 0 ) {            
            $res = $connection->prepare("SELECT id FROM `mts_w4`.`event_lieu` WHERE `t3id` = :t3id ");
            $res->bindParam(':t3id', $t3id, PDO::PARAM_INT );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return isset($row['id']) && $row['id']>0?$row['id']:getFirstEventLieuID();           
    } elseif (getFirstEventLieuID()>0) {
        return getFirstEventLieuID();
    } 
    return 0;
}
function getEventT3()
{
    //
    global $connection;
    $sql = "SELECT FROM_UNIXTIME(a.`crdate`) as created_at, FROM_UNIXTIME(a.`tstamp`) as updated_at, a.`uid`, a.`title`, a.`image`, a.`bodytext`, FROM_UNIXTIME(a.`tx_tsara_event_date`) as `tx_tsara_event_date`, FROM_UNIXTIME(a.`tx_tsara_event_date_end`) as `tx_tsara_event_date_end`, FROM_UNIXTIME(a.`tx_tsara_event_date`, '%Y-%m-%d' ) as `tx_tsara_event_date_noh`, FROM_UNIXTIME(a.`tx_tsara_event_date_end`, '%Y-%m-%d') as `tx_tsara_event_date_end_noh`, FROM_UNIXTIME(a.`tx_tsara_event_date`, '%H:%i:%s' ) as `tx_tsara_event_heure`,  a.`tx_tsara_event_reservation`, a.`tx_tsara_event_prix`, a.`tx_tsara_event_contacts`, a.`tx_tsara_event_endroit`, c.`value_alias` as `slug` 
FROM `mts_www`.`tt_news` a
LEFT JOIN `mts_w4`.`event` b ON b.t3id = a.uid
LEFT JOIN `mts_www`.`tx_realurl_uniqalias` c ON c.value_id = a.uid
WHERE a.`pid` = '55' AND a.`hidden` = '0' AND a.`deleted` = '0' AND b.t3id IS NULL AND c.tablename = 'tt_news' AND c.field_alias = 'title'
ORDER BY a.`uid` ASC";
echo $sql."\n"; 
    $res = $connection->prepare($sql);            
    $res->execute();
    return $res->fetchAll(PDO::FETCH_ASSOC);
     
}
function getLieuxT3() 
{
    global $connection;
    //Check row and insert if no row
    $res = $connection->prepare("SELECT COUNT(*) nb FROM `mts_w4`.`event_lieu`");            
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);
    
    if ( $row['nb']==0) {
        createEvent("name","address","","" );
    }
    
    $res = $connection->prepare("SELECT a.`uid`, a.`name`, a.`phone`, a.`address`, a.`tx_tsara_event_lieugps` FROM `mts_www`.`tt_address` a LEFT JOIN `mts_w4`.`event_lieu` b ON b.t3id = a.uid WHERE a.`pid` = '55' AND a.`hidden` = '0' AND a.`deleted` = '0' AND b.t3id IS NULL
ORDER BY `uid` ASC");            
    $res->execute();
    return $res->fetchAll(PDO::FETCH_ASSOC);

}
function getCatT3($t3id) 
{
    global $connection;
    $sql = "SELECT a.uid_foreign 
FROM `mts_www`.`tt_news_cat_mm` a
INNER JOIN `mts_www`.`tt_news_cat` b ON b.uid = a.uid_foreign
WHERE a.`uid_local` = ".$t3id." AND b.pid = 55 AND b.parent_category = 188";
    $res = $connection->prepare($sql);            
    $res->execute();
    $row = $res->fetchAll(PDO::FETCH_ASSOC);     
    return $row;
}
function getFirstEventLieuID()
{
    global $connection;
    $sql = "SELECT min(id) as id FROM `mts_w4`.`event_lieu` order by id asc";
    $res = $connection->prepare($sql);            
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);     
    return (isset($row['id']) && $row['id']>0?$row['id']:1);
}
function getLastIdEvent()
{
    global $connection;
    $sql = "SELECT max(id) as id FROM `mts_w4`.`event` order by id desc";
    $res = $connection->prepare($sql);            
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);     
    return (isset($row['id']) && $row['id']>0?$row['id']+1:1);
}

function mappingCat($uid)
{    $tab = array(
        189 => 1
        ,16830 => 1
        ,16872 => 1
        ,16662 => 1
        ,191 => 1
        ,16810 => 1

        ,190 => 6
        ,17091 => 6
        ,192 => 4
        ,193 => 4
        ,16829 => 5
        ,17055 => 2
        ,17054 => 2
        ,17053 => 6
        ,17047 => 3
        ,16979 => 1
        ,16978 => 2
        ,16871 => 1
        ,16898 => 6
        ,16709 => 1
        ,16708 => 3
        );

    return $tab[$uid];
}

function truncall()
{
    global $connection;
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_lieu`");
    $res->execute();
    $res = $connection->prepare("ALTER TABLE `mts_w4`.`event_lieu` AUTO_INCREMENT = 1");
    $res->execute();
     
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_event_flyers`");
    $res->execute(); 
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_event_type`");
    $res->execute(); 
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_event_local`");
    $res->execute(); 
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_flyers`");
    $res->execute(); 
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_videos`");
    $res->execute(); 
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event_event_artistes_dj_organisateurs`");
    $res->execute(); 
    
    $res = $connection->prepare("DELETE FROM `mts_w4`.`event`");
    $res->execute();   
    $res = $connection->prepare("ALTER TABLE `mts_w4`.`event` AUTO_INCREMENT = 1");
    $res->execute();
    
      
    $res = $connection->prepare("ALTER TABLE `mts_w4`.`event_flyers` AUTO_INCREMENT = 1");
    $res->execute();
    
     
    $res = $connection->prepare("ALTER TABLE `mts_w4`.`event_videos` AUTO_INCREMENT = 1");
    $res->execute();
    
    
    
    rmdir_recursive(dirname(__FILE__).'/../media/cache/*');

    foreach (new DirectoryIterator(dirname(__FILE__).'/../assets/flyers') as $fileInfo) {
        if(!$fileInfo->isDot()) {
            unlink($fileInfo->getPathname());
        }
    }

    return 'OK TRUNCATE  `mts_w4`.`event``, TRUNCATE  `mts_w4`.`event_flyers`, TRUNCATE `mts_w4`.`event_lieu`, TRUNCATE `mts_w4`.`event_videos` et Delete all files in '.dirname(__FILE__).'/../assets/flyers';
     
}
function rmdir_recursive($dir) { 
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? rmdir_recursive("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
} 
function slugify ($string) {
    $string = utf8_encode($string);
    $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);   
    $string = preg_replace('/[^a-z0-9- ]/i', '', $string);
    $string = str_replace(' ', '-', $string);
    $string = trim($string, '-');
    $string = strtolower($string);

    if (empty($string)) {
        return 'n-a';
    }

    return $string;
}
function createEvent($name,$address,$gps,$tel,$t3id=0)
{
    global $connection;
    $sql = "INSERT IGNORE INTO `mts_w4`.`event_lieu` SET ";
    $sql .= "`name` = :name, `slug` = :slug, `address` = :address, `gps` = :gps, `tel` = :tel, `t3id` = :t3id";

    $insertsql = $connection->prepare( $sql );

    $slug = slugify($name);


   $insertsql->bindParam(':name', $name, PDO::PARAM_STR );
   $insertsql->bindParam(':slug', $slug, PDO::PARAM_STR );
   $insertsql->bindParam(':address', $address, PDO::PARAM_STR );
   $insertsql->bindParam(':gps', $gps, PDO::PARAM_STR );
   $insertsql->bindParam(':tel', $tel, PDO::PARAM_STR );
   $insertsql->bindParam(':t3id', $t3id, PDO::PARAM_INT );


   $sql = str_replace(':name', $connection->quote($name), $sql );
   $sql = str_replace(':slug', $connection->quote($slug), $sql );
   $sql = str_replace(':address', $connection->quote($address), $sql );
   $sql = str_replace(':gps', $connection->quote($gps), $sql );
   $sql = str_replace(':tel', $connection->quote($tel), $sql );
   $sql = str_replace(':t3id', $t3id, $sql );
   echo $sql."\n";

   try {
       $insertsql->execute();
       //$id = $connection->lastInsertId();
       echo $i." - EventLieu name:«".$name."» adress:«".$address."», gps:«".$gps."», tel:«".$tel."», t3id: «".$t3id."» - enregistré en BDD \n";
   } catch ( Exception $e ) {
     echo "Requete  : ", $insertsql->errorCode();
   }
}
/*if( php_sapi_name() == 'cli' ) {
    print_r($argv);
    exit;
}*/
if( php_sapi_name() != 'cli' && $_GET['trunc']==1 ){
    echo truncall();exit();      
}
if( php_sapi_name() == 'cli' && $argv[1]=='trunc' ){
    echo truncall();exit();      
}

//Lieux
    $eventlieu = array();
    $eventlieu = getLieuxT3();

    echo count($eventlieu)." events Lieux \n";
    $i = 1; 
    if ( count($eventlieu) > 0 ) {
        foreach( $eventlieu as $row ){
            $t3id = $row['uid'];
            $name = $row['name'];
            echo $i." - Traitement de ".$name." ( ".$i." / ".count($eventlieu).")\n";
            $address = $row['address'];
            $gps = $row['tx_tsara_event_lieugps'];
            $tel = $row['phone'];

            createEvent($name,$address,$gps,$tel,$t3id);

            unset($insertsql);

            $i++;
        }
    }

 
//Event
$eventrow = array();
$eventrow = getEventT3();
echo count($eventrow)." events \n";

$eventid = getLastIdEvent();
$i = 1;
if ( count($eventrow)>0 ) {
    foreach( $eventrow as $row ){

         $t3id = $row['uid'];
         $name = $row['title'];
         $slug = $row['slug'];
         $created_at = $row['created_at'];
         $created_at = $row['updated_at'];

         $description = $row['bodytext'];

         if ( trim($name)=='' && $description!=''){
             $name = trim( substr( strip_tags($description), 0, 100 ) );
         }
         $image = $row['image'];
         $flyer = $row['image'];

         $date_debut = $row['tx_tsara_event_date_noh'];
         $date_fin = $row['tx_tsara_event_date_end_noh'];

         $tx_tsara_event_heure = $row['tx_tsara_event_heure'];

         $resa_prevente = $row['tx_tsara_event_reservation'];
         $contact_event = $row['tx_tsara_event_contacts'];

         $prixenclair = $row['tx_tsara_event_prix'] ;

         $eventlieu_id = getEventID($row['tx_tsara_event_endroit']);

         echo $i." - Traitement de ".$name." - eventlieu_id : ".$eventlieu_id." - ( ".$i." / ".count($eventrow).")\n";

         $date_unique = '';
         $heure_debut = '';
         if ( $date_debut == $date_fin ) {
            $date_unique = $date_debut;
            $heure_debut = $tx_tsara_event_heure;
            $date_debut = '';
            $date_fin = '';
         }
         if ( $prixunique > 0 ) {        
            $entree_id = 2;
         }
         $id = 0;
         $entree_id = 1;

         if ( intval( trim($prixenclair[0]) ) > 0 ) {
            $entree_id = 2;
         }

         $sql = "INSERT INTO `mts_w4`.`event` SET ";
         $sql .= "`flyer` = :flyer, `prixenclair` = :prixenclair, `slug` = :slug, `created_at`= :created_at, `updated_at`= :updated_at, `id` = ".$eventid.",`name` = :name, `description` = :description, `date_unique` = :date_unique, `date_debut` = :date_debut, `date_fin` = :date_fin, `contact_event` = :contact_event, `resa_prevente` = :resa_prevente, prixunique = :prixunique, heure_debut = :heure_debut, entreetype_id = :entree_id, t3id = :t3id, eventlieu_id = :eventlieu_id";



        //
        $sql = str_replace(':created_at', $connection->quote( $created_at ),$sql  );
        $sql = str_replace(':updated_at', $connection->quote( $updated_at ),$sql  );
        $sql = str_replace(':name', $connection->quote( $name ),$sql  );
        $sql = str_replace(':description', $connection->quote( $description ),$sql  );
        $sql = str_replace(':date_unique', $connection->quote( $date_unique ),$sql );
        $sql = str_replace(':date_debut', $connection->quote( $date_debut ),$sql );
        $sql = str_replace(':date_fin', $connection->quote( $date_fin ),$sql );
        $sql = str_replace(':contact_event', $connection->quote( $contact_event ),$sql );
        $sql = str_replace(':resa_prevente', $connection->quote( $resa_prevente ),$sql );
        $sql = str_replace(':prixunique', $connection->quote( $prixunique ),$sql );
        if ( $eventlieu_id > 0 )
            $sql = str_replace(':eventlieu_id', $connection->quote( $eventlieu_id ),$sql);

        $sql = str_replace(':entree_id', $connection->quote( $entree_id ),$sql );
        $sql = str_replace(':heure_debut', $connection->quote( $heure_debut ),$sql );
        $sql = str_replace(':prixenclair', $connection->quote( $prixenclair ),$sql );
        $sql = str_replace(':t3id', $connection->quote( $t3id ),$sql );
        $sql = str_replace(':slug', $connection->quote( $slug ),$sql );
        $sql = str_replace(':flyer', $connection->quote( $flyer ),$sql );

        if ( count($eventrow) < 4 )
            echo $sql."\n";


        try {
            //$insertsql->execute();
            //print_r( $insertsql->errorInfo() );
            $count = $connection->exec($sql);

            //$id = $connection->lastInsertId();
            //echo $count." row affected - Event enregistré en BDD avec ID : ".$id."\n";
        } catch ( Exception $e ) {
          echo "Requete  : ", $insertsql->errorCode();
        }
        unset($insertsql);
        if ( $eventid > 0 ) {

            if ( file_exists('/var/www/madatsara/www/uploads/pics/'.$image) && copy('/var/www/madatsara/www/uploads/pics/'.$image, dirname(__FILE__).'/../assets/flyers/'.$image ) ) {

                echo  $image.' copié de uploads/pics vers assets/flyers'."\n";

                //Ajout dans la BDD event_flyers
                $sql = "INSERT INTO `mts_w4`.`event_flyers` SET ";
                $sql .= "`image` = :image, `crdate` = NOW(), `ismain` = 1";
                $insertsql = $connection->prepare( $sql );    
                 
                $insertsql->bindParam(':image', $image, PDO::PARAM_STR );
                try {
                    $insertsql->execute();
                     $fid = $connection->lastInsertId();
                     echo "Flyer ".$image." enregistré en BDD pour event_id : ".$eventid." avec ID : ".$fid."\n";

                     $connection->exec("INSERT INTO `mts_w4`.`event_event_flyers` SET event_id = ".$eventid.", event_flyers_id = ".$fid);


                } catch ( Exception $e ) {
                  echo "Requete  : ", $insertsql->errorCode();
                } 
                 unset($insertsql);

            } else {
                echo 'ID : '.$eventid.' - '.$image.' Erreur copie '."\n";
            }

            //Type
            $tabcat = array();
            echo "t3id : ".$t3id."\n";
            $tabcat = getCatT3($t3id);

            foreach( $tabcat as $rowc ){
                $idcat = mappingCat($rowc['uid_foreign']);   
                if ( $idcat>0 ) {
                    echo "Enregistrement dans event_event_type de eventid: ".$eventid." - idcat: ".$idcat."\n";
                    $connection->exec("INSERT INTO `mts_w4`.`event_event_type` SET `event_id` = ".$eventid.", `event_type_id` = ".$idcat); 
                }
                unset($idcat);

            }



            


        }

        echo "-----------------------------\n";

        $eventid++;
        $i++;
    }

    //Reveillon
    $connection->exec("INSERT INTO `mts_w4`.`event_event_local` SELECT a.`id`, '1' FROM `mts_w4`.`event` a LEFT JOIN `mts_w4`.`event_event_local` b ON b.`event_id` = a.`id` WHERE b.`event_id` IS NULL AND a.`date_unique` = '2015-12-31'");

    //Noel
    $connection->exec("INSERT INTO `mts_w4`.`event_event_local` SELECT a.`id`, '2' FROM `mts_w4`.`event` a LEFT JOIN `mts_w4`.`event_event_local` b ON b.`event_id` = a.`id` WHERE b.`event_id` IS NULL AND a.`date_unique` = '2015-12-25'");

    //Saint-valentin
    $connection->exec("INSERT INTO `mts_w4`.`event_event_local` SELECT a.`id`, '3' FROM `mts_w4`.`event` a LEFT JOIN `mts_w4`.`event_event_local` b ON b.`event_id` = a.`id` WHERE b.`event_id` IS NULL AND a.`date_unique` = '2016-02-14'");
    
    //Nouvel An
    $connection->exec("INSERT INTO `mts_w4`.`event_event_local` SELECT a.`id`, '20' FROM `mts_w4`.`event` a LEFT JOIN `mts_w4`.`event_event_local` b ON b.`event_id` = a.`id` WHERE b.`event_id` IS NULL AND a.`date_unique` = '2016-01-01'");
    
    //Insert Date
    //$connection->exec("INSERT IGNORE INTO `mts_w4`.`event_date` (date) SELECT DISTINCT date_unique FROM `mts_w4`.event WHERE date_unique != '0000-00-00'");
    //$connection->exec("INSERT IGNORE INTO `mts_w4`.`event_by_date` SELECT a.id, b.id FROM event a LEFT JOIN `mts_w4`.event_date b ON b.date = a.date_unique WHERE b.id IS NOT NULL");

    $period = array();
    $res = $connection->prepare("SELECT id, date_debut,date_fin FROM `mts_w4`.event WHERE date_unique = '0000-00-00'");            
    $res->execute();
    foreach( $res->fetchAll(PDO::FETCH_ASSOC) as $row ) {
        $id = $row['id'];
        $period = date_range($row['date_debut'],$row['date_fin'],'+1 day','Y-m-d');

        foreach( $period as $temps ) {
            //echo $id.' - '.$temps.' - (entre '.$row['date_debut'].' et '.$row['date_fin'].')'."\n";
            
             
            $event_date_id = get_event_date_id($temps);
            $sql = addOrFailEventByDate($id, $event_date_id );
             
            //echo $sql."\n";
            unset($event_date_id);

        }
        unset($period);
        //echo "--------------\n"; 

    }

}

 