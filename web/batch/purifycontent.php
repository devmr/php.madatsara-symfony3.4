<?php
error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');

require 'vendor/autoload.php';
//use HTMLPurifier;

$bdd = 'mts_w4';

if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
    $bdd = 'mts_devw4'; 
}

$dns = 'mysql:host=localhost;charset=utf8';
$user = 'madatsara';
$mdp = '!4xntmj60!';

try {
     $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}

$sql = "SELECT id,title,content FROM `".$bdd."`.articles_event WHERE content IS NOT NULL AND content_purify IS NULL";
echo $sql."\n";
$res = $connection->prepare($sql);
$res->execute();
$row = $res->fetchAll(PDO::FETCH_ASSOC);
echo count($row)." lignes \n";
if ( count($row) <= 0 )
        break;

foreach($row as $ligne ) {


	echo "Purification de ".$ligne['title']." (".$ligne['id'].")\n";
	purifyAndUpdate($ligne);
}
function purifyAndUpdate($row)
{
	global $connection; 
	global $bdd;

	$content_purify = purifier($row['content']);

    $sql = "UPDATE `".$bdd."`.articles_event SET content_purify = :content_purify WHERE id = :id";
    $res = $connection->prepare( $sql );
    $res->bindParam(':content_purify', $content_purify, PDO::PARAM_STR );
    $res->bindParam(':id', $row['id'], PDO::PARAM_INT );
    try {
        $res->execute();
    } catch ( Exception $e ) {
      echo "Requete  : ", $res->errorCode();
    }
}
function purifier($html) 
{
        require dirname(__FILE__).'/vendor/ezyang/htmlpurifier/library/HTMLPurifier.auto.php';
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'em,strong,i,b,p,h1,h2,h3,h4,h5,h6,font[style|size],ol,ul,li,br,div');
        $config->set('AutoFormat.RemoveEmpty',  true);
        $config->set('Cache.SerializerPath', dirname(__FILE__) . '/../tmp/' ); // TODO: remove this later!
        $filter = new HTMLPurifier($config);
        $html = $filter->purify($html);    
        return $html;
}
