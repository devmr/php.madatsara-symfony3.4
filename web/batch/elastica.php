<?php
error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');

require 'vendor/autoload.php';
use Elasticsearch\ClientBuilder;
use Cocur\Slugify\Slugify;
// Load index
$hosts = [
    'localhost:9200'   // SSL to IP + Port
];
$client = ClientBuilder::create()->setHosts($hosts)->build();

Header('Content-Type: text/html; charset=utf-8');

    $bdd = 'madatsara';
    $elastic_index = 'madatsara';
    $elastic_type = 'event';

    if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
        $bdd = 'mts_devw4';
        $elastic_index = 'madatsara_dev';
        $elastic_type = 'event';
    }



$deleteindex = true;

if ( $_GET['index'] == 1 ) {

    if ( $_GET['nodelete'] == 1 )
        $deleteindex = false;
     
    $dns = 'mysql:host=localhost;charset=utf8';
    $user = 'madatsara';
    $mdp = '!4xntmj60!';
    try {
         $connection = new PDO( $dns, $user, $mdp );
        //echo 'Connexion OK';
    } catch ( Exception $e ) {
      echo "Connection à MySQL impossible : ", $e->getMessage();
      die();
    }
    
    
    $sql = "SELECT id,heure_fin_aube,slug,flyer,name,description,contact_event,CASE WHEN date_unique = '0000-00-00' THEN date_debut ELSE date_unique END as date_unique,heure_debut,heure_fin,CASE WHEN date_debut = '0000-00-00' THEN date_unique ELSE date_debut END as date_debut,CASE WHEN date_fin = '0000-00-00' THEN date_unique ELSE date_fin END as date_fin 
    FROM `".$bdd."`.`event`  
WHERE hidden = 0 AND deletedAt IS NULL";
    
    //if ( empty($_GET['remove']) || $_GET['remove'] != 1 )
    //    $sql .= " AND indexed IS NULL";
       
    
    $sql .= " ORDER BY  `id` DESC";
    echo $sql;
    
    $res = $connection->prepare($sql);
    $res->execute();
    $row = $res->fetchAll(PDO::FETCH_ASSOC);
    echo count($row)." lignes \n<br >";

    if ( count($row) <= 0 )
        exit;



    //Delete an index
    $deleteParams = [
        'index' => $elastic_index
    ];
    
    //Si l'index existe
    if ($deleteindex  && $client->indices()->exists($deleteParams) ) {
        $response = $client->indices()->delete($deleteParams);
        echo  "- Suppression de l'index ".$elastic_index." \n<br >";
    }

    //Si l'index existe pas
    if (!$client->indices()->exists($deleteParams) ) {
    	$client->indices()->create($deleteParams);
	echo  "- Creation de l'index ".$elastic_index." \n<br >";
    }      

    $params = ['body' => []];
    $i = 0;

    //$slugify = new Slugify();
    $date_array = array();
    foreach($row as $ligne ) {

        //Slugify
        /*$ligne['name'] = $slugify->slugify($ligne['name'], ' ');
        $ligne['description'] = $slugify->slugify($ligne['description'], ' ');*/

        $params['body'][] = [
            'index' => [
                '_index' => $elastic_index,
                '_type' => $elastic_type,
                '_id' => $ligne['id']
            ]
        ];

        //Enregistrer les dates
        $ligne['dateliste'] = getDates($ligne['id']);



        $params['body'][] = $ligne;


        
        echo "- Indexation de ".$ligne['name']." (".$ligne['id'].")<br />";




        // Every 1000 documents stop and send the bulk request
        if ($i > 0 && $i % 1000 == 0) {
            $responses = $client->bulk($params);
            // erase the old bulk request
            $params = ['body' => []];
            // unset the bulk response when you are done to save memory
            //echo print_r($responses, true);
            unset($responses);
        }




        $i++;
        
        //Update
        /*$res = $connection->prepare("UPDATE `mts_w4`.`event` SET `indexed`= 1 WHERE `id` = :id ");
        $res->bindParam(':id',$ligne['id'],PDO::PARAM_INT);
        $res->execute();*/
        
    }

    // Send the last batch if it exists
    if (!empty($params['body'])) {
        $responses = $client->bulk($params);
    }
    echo "<hr>".$i." elements indéxés \n";
    // exit();
}
  

//Creer un index
$params = array();
/*$params['body']  = array(
  'name' => 'Ash Ketchum',
  'age' => 10,
  'badges' => 8 
);

$params['index'] = 'madatsara';
$params['type']  = 'event';

$result = $client->index($params);*/
//print_r($result); 

//Recup d'un doc
/*$params = [
    'index' => 'madatsara',
    'type' => 'event',
     'id' => 10
];

$response = $client->get($params);
print_r($response);
exit;*/
function array_value_recursive($key, array $arr){
    $val = array();
    array_walk_recursive($arr, function($v, $k) use($key, &$val){
        if($k == $key) array_push($val, $v);
    });
    return count($val) > 1 ? $val : array_pop($val);
}
function getDates($id)
{

    global $connection;
    global $bdd;

    $sql = "SELECT a.date FROM ".$bdd.".event_date a LEFT JOIN ".$bdd.".event_by_date b ON b.event_date_id = a.id WHERE b.event_id = ".$id;
    $res = $connection->prepare($sql);
    $res->execute();
    return array_value_recursive('date',$res->fetchAll(PDO::FETCH_ASSOC));

}
function convertDate($date)
{
    if (($timestamp = strtotime($date)) === false) {
        return false;
    }

    $string = date('Y-m-d\TH:i:s\Z', $timestamp);
    return $string;
}

$q = '';
$opr = '';
$date1 = '';
$date2 = '';

echo "Param = q=&opr=gt|gte|lt|lte&date1=&date2=<br>";
echo "Exemple <a href='?q=leonard&opr=gte&date1=now'>?q=leonard&opr=gte&date1=now</a><br>";
echo "Exemple <a href='?date1=last Monday&date2=now'>?date1=last Monday&date2=now</a><br>";
if ( $_GET['opr'] != '')
    $opr = htmlspecialchars ($_GET['opr']);

if ( $_GET['q'] != '')
    $q = htmlspecialchars ($_GET['q']);

if ( $_GET['date1'] != '' && convertDate(htmlspecialchars ($_GET['date1'])) )
    $date1 = convertDate(htmlspecialchars ($_GET['date1']));

if ( $_GET['date2'] != '' && convertDate(htmlspecialchars ($_GET['date2'])) )
    $date2 = convertDate(htmlspecialchars ($_GET['date2'])); 


$range = $filter =array();


if ( $date1!='' && $date2!='' ) {
    $range = [
                        'date_unique' =>  [
                            'gte' => $date1
                            ,'lte' => $date2
                        ]
                    ];
    $filter = [
            'bool' => [
                'must' => [
                    'range' => $range
                ]
            ]
        ];
} elseif($date1!='' && $date2=='') {
    $range = [
                        'date_unique' =>  [
                            'gte' => $date1 
                        ]
                    ];

    $filter = [
            'bool' => [
                'must' => [
                    'range' => $range
                ]
            ]
        ];
} elseif($date1='' && $date2!='') {
    $range = [
                        'date_unique' =>  [
                            'lte' => $date2 
                        ]
                    ];
    $filter = [
            'bool' => [
                'must' => [
                    'range' => $range
                ]
            ]
        ];
}   

$match =  array(
            'match' => array( 
                'name' => $q
            )
            );
 $qs = array('query_string' => [
                //'default_field' => 'name',
                'fields' => [
                    'name', 
                    'description'
                    ],
                'query' => '*'.$q.'*'
            ] ); 

 $qso = array('query_string' => [
                 'default_field' => 'name',                 
                'query' => '*'.$q.'*'
            ] );

$st = $match;
 if ( $_GET['st'] != '' ) {
     $stype = htmlspecialchars($_GET['st']);
     switch($stype) {
        case 'm' :
            $st = $match;
        break;
        case 'q' :
            $st = $qs;
        break;
        case 'qo' :
            $st = $qso;
        break;
     }
 }

$query = array();
if ( $q != '' ) {
    $query =  [
            'bool' => [
                'should' => [
                    'query_string' => [
                        'query' => '*'.$q.'*',
                        'fields' => [
                            'name', 
                            'description'
                        ]
                    ]
                ]
            ]
        ] ;
}
   
//filtra avec date

$st = array(
    'filtered' => [
        'query' => $query,
        'filter' => $filter
    ]
);


//Recherche d'une chaine
$params = [
    'index' => $elastic_index,
    'type' => $elastic_type,
    'body' => [
        'query' =>  $st,
        'sort' => [
            'date_unique' => [
                'order' => 'asc'
            ]
        ]
            
        
    ]
];

echo 'Recherche de '.$q.' - opr: '.$opr.' - date1: '.$date1.' - date2: '.$date2.'<br>';
echo '<pre>'.print_r($params, true).'</pre>';
echo '<hr>';

$response = $client->search($params);

echo '<pre>'.print_r($response, true).'</pre>'; 
echo '<hr>';
echo '<pre>'.print_r(getAllValuesByFields('slug', $response), true).'</pre>'; 
echo '<hr>';
echo '<pre>'.print_r(getAllValuesByFields('query', $client->search(['index' => 'custom_elastic-2016.08.13' , 'type' => 'custom_elastic' ])), true).'</pre>'; 
//echo '<pre>'.print_r( getAllIndexCustomElastic() , true).'</pre>'; 
echo '<pre>'.json_encode( getAllQuery() ).'</pre>'; 

function getAllQuery(){
    global $client;

    $indices = getAllIndexCustomElastic();
    $data = array();
    if ( count($indices)<=0)
        return false;

    foreach($indices as $index ) {
        $response = $client->search(['index' => $index , 'type' => 'custom_elastic' ]);
        $data[] =  getAllValuesByFields('query', $response) ;
    }
    $data_ = array();
    foreach($data as $k => $v) {
        foreach($v as $k=>$vv)
            $data_[] = $vv;
    }
    $data_ = array_unique($data_);
    sort($data_);
    return array_values( $data_ );
}
function getAllIndexCustomElastic()
{
    global $client;
    $data = array();
    $keys_data = array_keys( $client->indices()->stats()['indices'] );
    if( count($keys_data)<=0)
        return false;
    foreach( $keys_data as $v ) {
        if (substr($v,0,15) == 'custom_elastic-')
            $data[] = $v;
    }
    return array_reverse( $data );
}

function getAllValuesByFields($field, $response = array()) {
    if ( count($response )<=0 )
        return false;

    $hits = $response['hits'];
    if ( !is_array($hits) || count( $hits )<=0 )
        return false;

    $total = $hits['total'];
    $data = $hits['hits'];

    if ( $total <=0 || count($data)<=0)
        return false;
     
    $data_val = array();
    foreach($data as $row ) {
        foreach($row['_source'] as $k => $v ) {
            if ($k == $field) {
                $data_val[] =  $v ;
            }
        }
    }

    return array_unique(array_values($data_val));
}
     

