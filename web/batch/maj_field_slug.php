<?php
require __DIR__ . '/vendor/autoload.php';
use Cocur\Slugify\Slugify;

error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');

$slugify = new Slugify();

Header('Content-Type: text/html; charset=utf-8');
$dns = 'mysql:host=localhost;charset=utf8;dbname=mts_w4';
$dnsdev = 'mysql:host=localhost;charset=utf8;dbname=mts_w4';

$user = 'madatsara';
$mdp = '!4xntmj60!';

if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
    $dns = $dnsdev;
}
if ('WINNT' == PHP_OS) {
    $dns = $dnsdev;
}

try {
    $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}

$table = 'locality';
if (''!=filter_input(INPUT_GET, 'table')) {
    $table = filter_input(INPUT_GET, 'table');
}

$period = $other = $current = $all = $data = $other_all = $allid = array();

$res = $connection->prepare("SELECT COUNT(a.id) as nb FROM $table a");
$res->execute();
$row = $res->fetch(PDO::FETCH_ASSOC);
$nb = $row['nb'];
echo $nb." lignes \n";

$res = $connection->prepare("SELECT a.slug,a.name,a.id  FROM $table a ORDER BY a.`name` ASC");
$res->execute();
echo '<pre>';
$i = 1;
$sqlU = "UPDATE $table SET slug = :slug WHERE id=:id";
foreach( $res->fetchAll(PDO::FETCH_ASSOC) as $row ) {
    $name = $row['name'];
    $slug = $row['slug'];
    $id = $row['id'];

    if (''==$slug && ''!=$name) {
        $slug = $slugify->slugify($name);
        $slug = getUniqueSlug($slug);
    }



    if (''!=$slug) {
        $slug = update($slug,$id);
    }

    echo $name . ' - ' . $slug . "\n";
}

echo '</pre>';

function getUniqueSlug($slug)
{
    $nb = slugExiste($slug);
    if ($nb>0) {

        return getUniqueSlug($slug . '-' . $nb );
    }
    return $slug;
}
function slugExiste($slug)
{
    global $connection;
    global $table;

    $sql = "SELECT COUNT(id) NB FROM $table WHERE slug = :slug";

    $res = $connection->prepare($sql);
    $res->bindParam(':slug', $slug );
    $res->execute();

    $row = $res->fetch(PDO::FETCH_ASSOC);
    return $row[NB];
}

function update($slug, $id)
{
    global $connection;
    global $sqlU;


    $res = $connection->prepare($sqlU);
    $res->bindParam(':slug', $slug );
    $res->bindParam(':id', $id );
    $res->execute();

    return $slug;
}