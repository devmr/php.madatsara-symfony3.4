<?php

error_reporting(1);
setlocale(LC_ALL, 'en_US.UTF8');

Header('Content-Type: text/html; charset=utf-8');
$bdd = 'mts_w4';

/*print_r(SQLite3::version());
echo "<br>";exit;*/

if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
    $bdd = 'mts_devw4';
}

$dns = 'mysql:host=localhost;charset=utf8;dbname='.$bdd;
$user = 'madatsara';
$mdp = '!4xntmj60!';

if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
    $dns = 'mysql:host=localhost;charset=utf8;dbname=mts_devw4';
}

try {
    $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}

/**
 * Functions
 */
function periodDate($fromdate, $todate) {
    $fromdate = DateTime::createFromFormat('Y-m-d', $fromdate);
    $todate = DateTime::createFromFormat('Y-m-d', $todate);
    return new DatePeriod(
        $fromdate,
        new DateInterval('P1D'),
        $todate->modify('+1 day')
    );
}
function dateRange($fromdate, $todate)
{
    $tab_dates = array();
    $tabrange = periodDate($fromdate, $todate);
    foreach($tabrange    as $date) {
        $tab_dates[] = $date->format('Y-m-d');
    }
    return $tab_dates;
}
function getEvents($id, $selectfield = '*', $returnfield = '*')
{
    global $connection;

    $sql = "FROM `event` a LEFT JOIN event_event_local b ON b.event_id = a.id  WHERE b.event_local_id = :eventlocalid AND a.hidden = 0 AND a.deletedAt IS NULL";

    $res = $connection->prepare("SELECT COUNT(*) AS nb ".$sql);
    $res->bindParam(':eventlocalid', $id, PDO::PARAM_INT );
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);
    $nb = $row['nb'];

    if ( $nb > 0 ) {


        $res = $connection->prepare("SELECT " . $selectfield . " " . $sql);
        $res->bindParam(':eventlocalid', $id, PDO::PARAM_INT);
        $res->execute();
        if ($returnfield == '*' || preg_match('/,/', $returnfield))
            return $res->fetchAll(PDO::FETCH_ASSOC);

        $data = array();
        foreach ($res->fetchAll(PDO::FETCH_ASSOC) as $k => $v) {
            $data[] = $v[$returnfield];
        }
        return $data;
    }

}
function deleteAllEvent()
{
    global $connection;

    $sql = "DELETE FROM `event_date_home`";

    $res = $connection->prepare($sql);
    $res->execute();


}
function deleteEvent( $id )
{
    global $connection;

    $sql = "DELETE FROM `event_date_home` WHERE event_id IN(:event_id)";

    $res = $connection->prepare($sql);
    $res->bindParam(':event_id', $id, PDO::PARAM_STR );
    $res->execute();


}

function getDateId( $date )
{
    global $connection;
    $sql = "SELECT id FROM `event_date` WHERE `date` = :date";

    $res = $connection->prepare($sql);
    $res->bindParam(':date', $date, PDO::PARAM_STR );
    $res->execute();

    $row = $res->fetch(PDO::FETCH_ASSOC);
    return $row['id'];
}

function addEvent($id, $date )
{
    global $connection;
    $sql = "INSERT INTO `event_date_home` SET `event_id` = :event_id, event_date_id = :event_date_id";

    $res = $connection->prepare($sql);
    $res->bindParam(':event_id', $id, PDO::PARAM_INT );
    $res->bindParam(':event_date_id', getDateId($date), PDO::PARAM_INT );
    $res->execute();
}
/**
 * END
 */

//Mois
$lastDateinCurrentmonth = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
$date_tmp = new DateTime('now');
$date_tmp->modify('last day of this month');
$d2 = $date_tmp->format('Y-m-d');



$date1 = new DateTime();
$date2 = new DateTime( $d2 );
$diff = $date2->diff($date1)->format("%a");

$ids = array();
if ( $diff <= 4 ) {

    $date_tmp = new DateTime('now');
    $date_tmp->modify('next month');

    $mois = $date_tmp->format('m');
    $date = $date_tmp->format('d');

    $ids[] = $mois;

    $date_tmp = new DateTime('now');
    $date_tmp->modify('this month');
    $mois = $date_tmp->format('m');
    $date = $date_tmp->format('d');

    $ids[] = $mois;

} else {
    //Sinon afficher la prochaine semaine
    $date_tmp = new DateTime('now');
    $date_tmp->modify('this month');

    $mois = $date_tmp->format('m');
    $date = $date_tmp->format('d');
}
if ( intval($mois)>0) {

    $sql = "FROM `event_local` WHERE MONTH(startdate) = :mois AND startdate >= :aujourdhui AND YEAR(startdate) = :annee";

    $res = $connection->prepare("SELECT COUNT(*) AS nb ".$sql);
    $res->bindParam(':mois', $mois, PDO::PARAM_STR );
    $res->bindParam(':annee', date('Y'), PDO::PARAM_STR );
    $res->bindParam(':aujourdhui', date('Y-m-d'), PDO::PARAM_STR );
    $res->execute();
    $row = $res->fetch(PDO::FETCH_ASSOC);
    $nb = $row['nb'];
    /*var_dump($nb);
    var_dump($bdd);*/
    echo '<pre>';
    if ( $nb > 0 ) {
        //Supprimer avant insert
        deleteAllEvent();

        $res = $connection->prepare("SELECT * ".$sql." ORDER BY startdate ASC");
        $res->bindParam(':mois', $mois, PDO::PARAM_STR );
        $res->bindParam(':annee', date('Y'), PDO::PARAM_STR );
        $res->bindParam(':aujourdhui', date('Y-m-d'), PDO::PARAM_STR );
        $res->execute();
        $row = $res->fetchAll(PDO::FETCH_ASSOC);
        //echo "SELECT * ".$sql." ORDER BY startdate ASC";

        echo $nb.' event local '."\n ---------- \n";

        $allid_arr  = $allocalname_arr = array();
        //Pour chaque eventlocal
        foreach( $row as $local ) {
            $date = $local['startdate'];
            $eventlocal = $local['id'];
            $eventlocal_name = $local['name'];

             echo "eventlocal_name: ".$eventlocal_name.' - '.$date."\n";

            $date_array = dateRange(date('Y') . '-' . $mois . '-01', $date);
            //print_r($date_array);

            //Les events dans cet localevent
            $events_name = getEvents($eventlocal, 'a.name', 'name');
            $events_id = getEvents($eventlocal, 'a.id', 'id');
            print_r($events_id);


            if (is_array($events_id)) {

                $arrayit = new ArrayIterator($events_id);
                $infinite = new InfiniteIterator($arrayit);
                $limit = new LimitIterator($infinite, 0, count($date_array));

                $event_iterator = array();
                foreach ($limit as $value) {
                    $event_iterator[] = $value;
                }
                //print_r($event_iterator);
                $i = 0;
                foreach ($event_iterator as $eventid) {
                    //deleteEvent($eventid);
                    //addEvent($eventid, $date_array[$i]);
                    $allid_arr[] = $eventid;
                    $alldates_arr[] = $date_array[$i];
                    $allocalname_arr[] = $eventlocal_name;
                     // echo "OK pour " . $eventid . " - " . $date_array[$i] . " - (ID date : ".getDateId($date_array[$i]). ")\n";
                    $i++;
                }
            }

            //echo "\n ---------- \n";
        }


        // print_r($allid_arr);
        print_r($alldates_arr);
        $i = 0;
        foreach ($allid_arr as $eventid) {
            addEvent($eventid, $alldates_arr[$i]);
            echo $allocalname_arr[$i]." - OK pour " . $eventid . " - " . $alldates_arr[$i] . " - (ID date : ".getDateId($alldates_arr[$i]). ")\n";
            $i++;
        }

    }
    echo '</pre>';
}