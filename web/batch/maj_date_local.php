<?php
function date_range($fromdate, $todate) {
    $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
    $todate = \DateTime::createFromFormat('Y-m-d', $todate);
    $dates = new \DatePeriod(
        $fromdate,
        new \DateInterval('P1D'),
        $todate->modify('+1 day')
    );

    /*foreach($dates as $date) {
        echo $date->format('Y-m-d').PHP_EOL;
    }
    exit;*/
    return $dates;
}

error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');
//Var bdd
$bdd = 'mts_w4';
if ( 'dev.madatsara.com' == $_SERVER['HTTP_HOST'] ) {
    $bdd = 'mts_devw4';
}
$dns = 'mysql:host=localhost;charset=utf8;dbname='.$bdd;
$user = 'madatsara';
$mdp = '!4xntmj60!';

Header('Content-Type: text/html; charset=utf-8');

try {
    $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
    echo "Connection à MySQL impossible : ", $e->getMessage();
    die();
}

function addOrFailEventByDate( $event_local_id, $eventdateid)
{
    global $connection;
    $sql = "";
    $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `local_by_date` WHERE `event_local_id` = :event_local_id AND `event_date_id` = :event_date_id");
    $res->bindParam(':event_date_id', $eventdateid, PDO::PARAM_INT );
    $res->bindParam(':event_local_id', $event_local_id, PDO::PARAM_INT );
    $res->execute();

    $row = $res->fetch(PDO::FETCH_ASSOC);
    $nb = $row['nb'];

    if ( $nb <= 0 ){
        $sql = "INSERT INTO `local_by_date` SET `event_local_id` = :event_local_id, `event_date_id` = :event_date_id";
        $insertsql = $connection->prepare( $sql );
        $insertsql->bindParam(':event_date_id', $eventdateid, PDO::PARAM_INT );
        $insertsql->bindParam(':event_local_id', $event_local_id, PDO::PARAM_INT );
        try {
            $insertsql->execute();
        } catch ( Exception $e ) {
            echo "Requete  : ", $insertsql->errorCode();
        }

        $sql = str_replace(':event_date_id', $eventdateid, $sql);
        $sql = str_replace(':event_local_id', $event_local_id, $sql);
    } else {
        $sql = "Combinaison ".$eventdateid." - ".$event_local_id." existe";
    }

    return $sql;
}
function event_date_id_exists($date){
    global $connection;
    if( $date != '' ){
        $date = trim($date);
        $res = $connection->prepare("SELECT COUNT(*) AS nb FROM `event_date` WHERE `date` = :dates");
        $res->bindParam(':dates', $date, PDO::PARAM_STR );
        $res->execute();

        $row = $res->fetch(PDO::FETCH_ASSOC);
        $nb = $row['nb'];

        return ($nb>0?true:false);

    }
    return false;
}
function get_event_date_id($date){
    global $connection;
    if( $date != '' ) {
        $date = trim($date);

        if ( event_date_id_exists($date) ){

            $res = $connection->prepare("SELECT id FROM `event_date` WHERE `date` = :dates LIMIT 1");
            $res->bindParam(':dates', $date, PDO::PARAM_STR );
            $res->execute();
            $row = $res->fetch(PDO::FETCH_ASSOC);
            return $row['id'];
        }  else {

            $insertsql = $connection->prepare( "INSERT INTO `event_date` SET `date` = :dates" );
            $insertsql->bindParam(':dates', $date, PDO::PARAM_STR );

            try {
                $insertsql->execute();
            } catch ( Exception $e ) {
                echo "Requete  : ", $insertsql->errorCode();
            }
            return $connection->lastInsertId();
        }
    }
    return 0;
}


$period = array();

$res = $connection->prepare("SELECT COUNT(a.id) as nb FROM event_local a LEFT JOIN `local_by_date` b ON b.event_local_id = a.id WHERE b.event_local_id IS NULL AND a.startdate IS NOT NULL AND a.startdate != '0000-00-00' AND a.enddate IS NOT NULL AND a.enddate != '0000-00-00' ");
$res->execute();
$row = $res->fetch(PDO::FETCH_ASSOC);
$nb = $row['nb'];
echo $nb." lignes \n";

$res = $connection->prepare("SELECT a.id, a.startdate as date_debut, a.enddate as date_fin FROM event_local a LEFT JOIN `local_by_date` b ON b.event_local_id = a.id WHERE b.event_local_id IS NULL AND a.startdate IS NOT NULL AND a.startdate != '0000-00-00' AND a.enddate IS NOT NULL AND a.enddate != '0000-00-00'");
$res->execute();

$i = 1;
foreach( $res->fetchAll(PDO::FETCH_ASSOC) as $row ) {
    $id = $row['id'];
    $date_debut = $row['date_debut'];
    $date_fin = $row['date_fin'];

    echo 'Ligne '.$i.' - ';




    $period = date_range($date_debut,$date_fin );
    //print_r($period);exit;

    foreach( $period as $temps ) {

        //echo $id.' - '.$temps->format('Y-m-d').' - (entre '.$row['date_debut'].' et '.$row['date_fin'].')'."\n";


        $event_date_id = get_event_date_id($temps->format('Y-m-d'));
        $sql = addOrFailEventByDate($id, $event_date_id );

        echo $sql."\n";
        unset($event_date_id);

    }
    unset($period);


    echo "--------------\n";
    $i++;

}