/**
 * ----------------- Variables -----------------
 */
var selector_lieu = $(".select2_lieux");
var selector_Fete = $(".select2_fete");
var selector_Artiste = $(".select2_artiste");
var selector_RelatedEvent = $(".select2_related_event");
var selector_Event = $(".select2_event");
var selector_Event_Inarticle = $(".select2_event_inarticle");
var selector_ParentEventLocal = $(".s2-parent-event-local");
var selector_Event_Ineventbymember = $(".select2_event_ineventbymember");
var selector_parentArtistes = $(".select2_parentartiste");
var selector_enfantArtistes = $(".select2_enfantartiste");

var selector_Date = 'input[name="daterange"]';
var selector_Date2 = 'input[name="daterange2"]';
var selector_DateRange = 'input[name="daterange_range"]';
var selector_DateMultiple = 'input[name="daterange_multiple"]';



var flatpickerConfigReset = {
    enableTime: true,
    altInput: true,
    altFormat: "d/m/Y-H:i",
    locale: "fr",
    onClose: function (selectedDates, dateStr, instance)
    {
        fillDateclairField() ;
    },
    time_24hr: true
};
var flatpickerConfigReset2 = {
    enableTime: true,
    altInput: true,
    defaultDate: "23:59",
    altFormat: "H:i",
    locale: "fr",
    onClose: function (selectedDates, dateStr, instance)
    {
        fillDateclairField() ;
    },
    time_24hr: true,
    noCalendar: true
};

var flatpickerConfigRange = {
    enableTime: true,
    altInput: true,
    altFormat: "d/m/Y-H:i",
    locale: "fr",
    mode: "range",
    onClose: function (selectedDates, dateStr, instance)
    {
        fillDateclairField() ;
    },
    time_24hr: true
};

var flatpickerConfigMultiples = {
    enableTime: true,
    altInput: true,
    altFormat: "d/m/Y-H:i",
    locale: "fr",
    mode: "multiple",
    onClose: function (selectedDates, dateStr, instance)
    {
        fillDateclairField() ;
    },
    time_24hr: true
};

Flatpickr.l10ns.fr.rangeSeparator = ' au ';
if (null!=document.querySelector(selector_Date))
    var calReset = new Flatpickr( document.querySelector(selector_Date), flatpickerConfigReset);
if (null!=document.querySelector(selector_Date2))
    var calReset2 = new Flatpickr( document.querySelector(selector_Date2), flatpickerConfigReset2);
if (null!=document.querySelector(selector_DateRange))
    var calRange = new Flatpickr( document.querySelector(selector_DateRange), flatpickerConfigRange);
if (null!=document.querySelector(selector_DateMultiple))
    var calMultiple = new Flatpickr( document.querySelector(selector_DateMultiple), flatpickerConfigMultiples);


var selector_Typeahead = $('#remoteevent .typeahead');
var selector_Telinput = $(".telinput");
var selector_Typeahead_PRCQ = $('#remote_search_paysregioncommunequartier .typeahead');

var selector_fieldPrice = document.querySelectorAll('.blocclone');
var selector_fieldVideo = document.querySelectorAll('.blocclone_video');
var selector_fieldTel = document.querySelectorAll('.blocclone_tel');
var selector_fieldArticles = document.querySelectorAll('.blocclone_articles');
var selector_fieldArticlesArtists = document.querySelectorAll('.blocclone_articles_artiste');
var selector_field_Eventlieu_Tel = $('#mdts_homebundle_eventlieu_tel');

var selector_mainFlyer = document.querySelectorAll('.setmainflyer');
var selector_deleteFlyer = document.querySelectorAll('.deleteflyer');
var selector_btndeleteitem = document.querySelectorAll(".btndeleteitem");
var selector_loadedimge = document.querySelectorAll(".loadedimge");

var dom_formArticles = 'form[name=mdts_homebundle_articlesevent]' ;
var dom_formArtistes = 'form[name=mdts_homebundle_eventartistesdjorganisateurs]' ;
var dom_formEvent = 'form[name=mdts_homebundle_event]' ;
var dom_formLocal = 'form[name=mdts_homebundle_eventlocal]' ;
var dom_formEvent_member = 'form[name=mdts_frontendbundle_eventbymember]' ;
var dom_form_ids = 'form[name=mdts_homebundle_all]' ;


var pathQuery_BtnCarteGoogle = "#btncartegoogle" ;
var pathQuery_CarteGoogle = "#cartegoogle" ;
var field_GpsID = "mdts_homebundle_eventlieu_gps" ;
var field_mapID = "map" ;


var selector_ckeditor = document.getElementById('name_ck');

var divContainer_Price = document.getElementById('collectionContainer');
var divContainer_Tel = document.getElementById('collectiontelContainer');
var divContainer_Video = document.getElementById('collectionvideoContainer');
var divContainer_Articles = document.getElementById('collectionarticlesContainer');
var divContainer_Image = document.getElementById('imagecontainer');
var divContainer_Artistes = document.getElementById('collectionarticlesartisteContainer');
var divContainer_Resultpic = document.getElementById('resultpic');
var select_action = document.getElementById('select_action');
var selector_formEventMultiple = document.getElementById('editmultiple') ;
var selector_resultArtistes = document.getElementById('resultArtistes') ;
var selector_resultEvents = document.getElementById('resultEvents') ;
var selector_iframeArtiste = document.getElementById('blociframeartiste') ;
var selector_iframeEvents = document.getElementById('blociframeevent') ;

var field_Country = document.getElementById('mdts_homebundle_eventlieu_country') ;
var field_Region = document.getElementById('mdts_homebundle_eventlieu_region') ;
var field_Locality = document.getElementById('mdts_homebundle_eventlieu_locality') ;
var field_Quartier = document.getElementById('mdts_homebundle_eventlieu_quartier') ;
var field_Event_Entree = document.getElementById('mdts_homebundle_event_entreetype') ;
var field_Map_Input = ( document.getElementById('pac-input') );

var checkbox_Enddate = document.getElementById('chk_enddate');
var checkbox_All = document.getElementById("chk_all");
var selector_td_classStyled = document.querySelectorAll("td .styled");
var selector_button_All = document.querySelectorAll(".btnall");
var selector_button_Add = document.querySelectorAll(".btnadd");
var selector_option_addNew = document.querySelectorAll("*[data-addnew]");

var urlprefixe = ('string' === typeof urlpath ? urlpath : '')+"/rest/";

var ajax_url_lieu = urlprefixe+"lieux";
var ajax_url_Fete = urlprefixe+"local";
var ajax_url_Artiste = urlprefixe+"artistesdj";
var ajax_url_RelatedEvent = urlprefixe+"relatedevent";
var ajax_url_Event = urlprefixe+"event";
var ajax_url_Regions = urlprefixe+"regions";
var ajax_url_Localities = urlprefixe+"localities";
var ajax_url_Quartiers = urlprefixe+"quartiers";

var ajax_url_setFlyer = urlprefixe+"setflyer";
var ajax_url_deleteFlyer = urlprefixe+"deleteflyer";

var ajax_url_saveCountries = urlprefixe+"savecountries";
var ajax_url_saveRegions = urlprefixe+"saveregions";
var ajax_url_saveLocalities = urlprefixe+"savelocalities";
var ajax_url_saveQuartiers = urlprefixe+"savequartiers";
var ajax_url_saveLieux = urlprefixe+"savelieus";
var ajax_url_saveLocal = urlprefixe+"savelocals";
var ajax_url_saveArtists = urlprefixe+"saveartists";
var ajax_url_saveEntreetypes = urlprefixe+"saveentreetypes";
var ajax_url_uploadFlyer = urlprefixe+"uploadflyers";


var dateUnique = document.getElementById('mdts_homebundle_event_dateUnique');
var heureDebut = document.getElementById('mdts_homebundle_event_heureDebut');
var heureFin = document.getElementById('mdts_homebundle_event_heureFin');
var dateDebut = document.getElementById('mdts_homebundle_event_dateDebut');
var dateFin = document.getElementById('mdts_homebundle_event_dateFin');

// Date mdts_homebundle_event_opt_dateclair
var opt_dateclair = document.querySelector('input[id$=_optdateclair]');
var dateclair = document.querySelector('input[id$=_dateclair]');
var selector_artistesdj = document.querySelector('input[id$=_artistesdj]');
var selector_eventmultilieu = document.querySelector('input[id$=_eventmultilieu]');
var selector_eventlocal = document.querySelector('input[id$=_eventlocal]');
var selector_related_event = document.querySelector('input[id$=_related_event]');

var selector_dataShowrangedate = document.querySelectorAll('*[data-showrangedate]');
var selector_dataShowmultiplesdate = document.querySelectorAll('*[data-showmultiplesdate]');
var selector_dataResetdate = document.querySelectorAll('*[data-resetdate]');
var selector_dataSelectpagination = document.querySelectorAll('*[data-selectpagination]');
var selector_btnUpload = document.querySelectorAll('*[data-btnupload]');

var selector_divDataResetdate = document.querySelectorAll('*[data-datereset]');
var selector_divDataRangedate = document.querySelectorAll('*[data-daterange]');
var selector_divDataMultiplesdate = document.querySelectorAll('*[data-datemultiples]');



var retour = '';

var artistsIDinOptions = new Array();
var lieuxIDinOptions = new Array();

var cropper;
var tab_hidden = new Array();
var flyerexiste = false;
var divimage = 'image';

var field_price = document.getElementById('fieldprice');
var field_optionEntreetype = document.getElementById('mdts_homebundle_event_entreetype');
var field_mainflyerId = document.getElementById('mainflyer');
var field_Event_File = document.getElementById('mdts_homebundle_event_flyerFile_file');
var field_Artiste_File = document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_photoFile_file');
var field_filedrag = document.getElementById('filedrag');
var field_Event_filenameajax = document.getElementById('mdts_homebundle_event_filenameajax');
var btn_closeparentiframe = document.getElementById('btn_closeparentiframe');

var at_config = {
    at: "@",
    limit:100,
    //data: issues,
    displayTpl: "<li data-value='@${name}'><div class=\"row\"><div class=\"col-sm-3\"><img src=\"/assets/artistes/${photo_png}\" width=\"20\" /></div><div class=\"col-sm-9\">${name}</div></div></li>",
    insertTpl: '<span class="highlight dataeval" data-photo="${photo}" data-id="${id}" data-type="artiste">${name}</span>',
    callbacks: {
        /*
         It function is given, At.js will invoke it if local filter can not find any data
         @param query [String] matched query
         @param callback [Function] callback to render page.
         */
        remoteFilter: function(query, callback) {
            cbAtRemoteArtists(query, callback);
        }
    }
};

var at_conf_lieux = {
    at: "#",
    limit:100,
    //data: issues,
    displayTpl: "<li data-value='@${name}'><div class=\"row\"><div class=\"col-sm-3\"></div><div class=\"col-sm-9\">${name}</div></div></li>",
    insertTpl: '<span class="highlight_gris dataeval" data-country="${country}" data-region="${region}" data-region="${region}" data-locality="${locality}" data-quartier="${quartier}" data-id="${id}" data-type="lieux">${name}</span>',
    callbacks: {
        /*
         It function is given, At.js will invoke it if local filter can not find any data
         @param query [String] matched query
         @param callback [Function] callback to render page.
         */
        remoteFilter: function(query, callback) {
            cbAtRemoteLieu(query, callback);
        }


    }
}

if ( selector_loadedimge.length > 0 )
    divimage = 'imageplus';
if ( divContainer_Image )
    divimage = 'imagecontainer'; 
